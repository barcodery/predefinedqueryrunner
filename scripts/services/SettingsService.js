'use strict';

angular.module('BarcoderyApp')
.factory('SettingsService',  [ '$q', 'DatabaseService', function ($q, DatabaseService)
{
	var fs = require('fs');
	var fileName= __dirname +'/data/data.json';


	var service = {};
	service.connections = [];
	service.active_connection = null;


	LoadSettings();

	function LoadSettings()
	{
		if (fs.existsSync(fileName))
		{
			var fileData = fs.readFileSync(fileName, 'utf8');
			service.connections = JSON.parse(fileData);

			if (service.connections.length > 0)
			{
				service.active_connection = service.connections[0];
				DatabaseService.Connect(service.active_connection);
			}
		}
		else
		{
			CreateDemoData();
		}
	}

	function CreateDemoData()
	{
		var connection = {};

		service.connections.push(connection);
		service.active_connection = connection;

		connection.name = "PSAS_TEST";
		connection.description = "Testovacie prostredie";
		connection.type = "mysql";
		connection.type = "mssql";
		connection.type = "postgresql";
		connection.color = "red";


		connection.queries = [];

		for (var i = 0; i < 10; i++)
		{
			var q = {};

			q.name = "Name" + i;
			q.description = "DESC" + i;
			q.id = i;
			q.sql = "SELECT * FROM Nieco";
			q.parameters = [];

			q.parameters.push({name:'text'	  , type:'Text', default_value:'poopioip'});
			q.parameters.push({name:'Checkbox', type:'Checkbox', default_value:true});
			q.parameters.push({name:'Interval', type:'DateTime', default_value:true});

			q.parameters.push({name:'Combobox', type:'Combobox', data_source:'[{value:1, text:"text 1"}, {value:2, text:"text 2"}]'});

			connection.queries.push(q);
		}
	}

	service.SaveSettings = function()
	{
		var data = JSON.stringify(service.connections);
		fs.writeFile(fileName, data, function(err)
		{
		});

		var deferred = $q.defer();
		deferred.resolve({});

		return deferred.promise;
	}



	// Queries

	service.DeleteQueryByName = function(name)
	{
  		var filtered = service.active_connection.queries.filter(function (item){ return item.name != name});
        service.active_connection.queries = filtered;

        service.SaveSettings();
	}

	service.GetQueriesAsync = function()
	{
		var deferred = $q.defer();
		deferred.resolve(service.active_connection.queries);

		return deferred.promise;
	}

	service.GetQueriesSync = function()
	{
		return service.active_connection.queries;
	}

	service.GetQueryByName = function(name)
	{
	  	var result = service.active_connection.queries.find(function (item) { return item.name == name; })

		var deferred = $q.defer();
		deferred.resolve(result);

		return deferred.promise;
	}

	service.AddQuery = function (query)
	{
		service.active_connection.queries.push(query);
		service.SaveSettings();

		var deferred = $q.defer();
		deferred.resolve({});
		return deferred.promise;
	}

	service.EditQuery = function(old_query, new_query)
	{
		for (var i = 0; i < service.active_connection.queries.length; i++) {
			var q = service.active_connection.queries[i];

			if (q.name == old_query.name)
			{
				service.active_connection.queries[i] = new_query;
			}
		}

		service.SaveSettings();

		var deferred = $q.defer();
		deferred.resolve({});

		return deferred.promise;
	}


	// Connections

	service.GetConnectionsAsync = function()
	{
		var deferred = $q.defer();
		deferred.resolve(service.connections);

		return deferred.promise;
	}

	service.GetConnectionsSync = function()
	{
		return service.connections;
	}

	service.GetActiveConnectionSync = function()
	{
		return service.active_connection;
	}

	service.AddConnection = function(connection)
	{
		service.connections.push(connection);
		service.SaveSettings();

		var deferred = $q.defer();
		deferred.resolve({});
		return deferred.promise;
	}

	service.EditConnection = function(old_connection, new_connection)
	{
		for (var i = 0; i < service.connections.length; i++)
		{
			var connection = service.connections[i];

			if (connection.name == old_connection.name)
			{
				service.connections[i] = new_connection;
			}
		}

		service.SaveSettings();

		var deferred = $q.defer();
		deferred.resolve({});

		return deferred.promise;
	}

	service.DeleteConnectionByName = function(name)
	{
  		var filtered = service.connections.filter(function (item){ return item.name != name});
        service.connections = filtered;

        service.SaveSettings();
	}

	service.SetActiveConnection = function(connection)
	{
		service.active_connection = connection;
		DatabaseService.Connect(connection);
	}

	return service;
}]);

