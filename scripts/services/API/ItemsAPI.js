'use strict';

angular.module('BarcoderyApp')
.factory('ItemsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		// legacy delete addEdit, when ready
		addEdit:
		{
			method: 'POST',
			//params: { name: '@name'},
			url: API_BASE_URL + '/add_edit_item',
			isArray:true,
		},

		createMultipleItems:
		{
			method: 'POST',
			url: API_BASE_URL + '/create_multiple_items',
		},

		get:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_items',
			isArray:true,
		},

		delete:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_items',
			isArray:true,
		},

		getItem:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_item',
		},

		getItemInOutEntries:
		{
			method: 'POST',
			// params: { item_id: '@item_id'},
			url: API_BASE_URL + '/get_item_in_out_entries',
			isArray:true,
		},

		getItemLocations:
		{
			method: 'POST',
			// params: { item_id: '@item_id'},
			url: API_BASE_URL + '/get_item_locations',
			isArray:true,
		},


		addInOutEntry:
		{
			method: 'POST',
			// params: { item_id: '@item_id', location_id: '@location_id', direction: '@direction', note: '@note', },
			url: API_BASE_URL + '/add_in_out_entry',
		},

		addTransfer:
		{
			method: 'POST',
			// params: { item_id: '@item_id', location_from_id: '@location_from_id', location_to_id: '@location_to_id', note: '@note', },
			url: API_BASE_URL + '/add_transfer',
		},

		deleteInOutEntries:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_in_out_entries',
			isArray:true,
		},

		get_items_simple:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_items_simple',
			isArray:true,
		},

		generate_unique_values:
		{
			method: 'POST',
			url: API_BASE_URL + '/generate_item_unique_values',
			isArray:false,
		},

		get_item_ids_with_minimum_count_alert:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_item_ids_with_minimum_count_alert',
			isArray:true,
		},


	})
}])