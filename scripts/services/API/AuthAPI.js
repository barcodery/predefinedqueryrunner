'use strict';

angular.module('BarcoderyApp')
.factory('AuthAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		login: {
			method: 'POST',
			// params: { username: '@email', password: '@password' },
			url: API_BASE_URL + '/login',
		},

		register: {
			method: 'POST',
			// params: {name: '@name', email: '@email', password: '@password' },
			url: API_BASE_URL + '/register',
		},

		getUser: {
			method: 'POST',
			url: API_BASE_URL + '/get_user',
		},

		logout: {
			method: 'POST',
			url: API_BASE_URL + '/logout',
			},

		password_recovery: {
			method: 'POST',
			url: API_BASE_URL + '/password_recovery',
			},

		use_promo_code: {
			method: 'POST',
			url: API_BASE_URL + '/use_promo_code',
			},

	})
}])