'use strict';

angular.module('BarcoderyApp')
.factory('AccountsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		getAccountInfo:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_account_info',
		},

		setActiveAccount:
		{
			method: 'POST',
			url: API_BASE_URL + '/set_active_account',
		},

		generate_demo_data:
		{
			method: 'POST',
			url: API_BASE_URL + '/generate_demo_data'
		},

		get_invoices:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_invoices',
			isArray: true,
		},

		get_account_address:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_account_address',
		},

		set_account_address:
		{
			method: 'POST',
			url: API_BASE_URL + '/set_account_address',
		},

		generate_invoice:
		{
			method: 'POST',
			url: API_BASE_URL + '/generate_invoice',
		},


	})
}])