'use strict';

angular.module('BarcoderyApp')
.factory('RequestsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		add_edit_request:
		{
			method: 'POST',
			// params: { name: '@name'},
			url: API_BASE_URL + '/add_edit_request',
			isArray:false,
		},

		add_request_items:
		{
			method: 'POST',
			// params: { name: '@name'},
			url: API_BASE_URL + '/add_request_items',
			isArray:true,
		},


		get:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_requests',
			isArray:true,
		},

		get_request_items:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_request_items',
			isArray:true,
		},

		delete_requests:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_requests',
			isArray:true,
		},

		delete_request_items:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_request_items',
			isArray:true,
		},

		cover_request_items:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/cover_request_items',
			isArray:true,
		},

		uncover_request_items:
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/uncover_request_items',
			isArray:true,
		},




	})
}])