'use strict';

angular.module('BarcoderyApp')
.factory('LocationsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null, 
	{
		add: 
		{
			method: 'POST',
			// params: { name: '@name'},
			url: API_BASE_URL + '/add_location',
		},
		
		get: 
		{
			method: 'POST',
			url: API_BASE_URL + '/get_locations',
			isArray:true,
		},
		
		delete: 
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_locations',
			isArray:true,
		},		
		
		edit: 
		{
			method: 'POST',
			// params: { location_id: '@location_id', name:'@name'},
			url: API_BASE_URL + '/edit_location',
		},		
		
		import: 
		{
			method: 'POST',
			// params: { items_json: '@items_json'},
			url: API_BASE_URL + '/import_locations',
		},		
		
	})
}])