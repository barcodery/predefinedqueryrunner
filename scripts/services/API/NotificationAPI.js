'use strict';

angular.module('BarcoderyApp')
.factory('NotificationAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null, 
	{
		get: 
		{
			method: 'POST',
			url: API_BASE_URL + '/get_notification',
		},
		
		setSeen: 
		{
			method: 'POST',
			url: API_BASE_URL + '/set_notification_seen',			
		},				
		
		set: 
		{
			method: 'POST',
			url: API_BASE_URL + '/set_notification',			
		},
		
		setForUserEmail: 
		{
			method: 'POST',
			url: API_BASE_URL + '/set_notification_for_user_email',			
		},		
		
		setForUserId: 
		{
			method: 'POST',
			url: API_BASE_URL + '/set_notification_for_user_id',			
		},				
	})
}])