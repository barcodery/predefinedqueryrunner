'use strict';

angular.module('BarcoderyApp')
.factory('CategoriesAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null, 
	{
		add: 
		{
			method: 'POST',
			// params: { name: '@name'},
			url: API_BASE_URL + '/add_category',
		},
		
		get: 
		{
			method: 'POST',
			url: API_BASE_URL + '/get_categories',
			isArray:true,
		},
		
		delete: 
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_categories',
			isArray:true,
		},	
		
		edit: 
		{
			method: 'POST',
			// params: { category_id: '@category_id', name:'@name'},
			url: API_BASE_URL + '/edit_category',
		},		
		
		import: 
		{
			
			// params: { items_json: '@items_json'},
			url: API_BASE_URL + '/import_categories',
			method: 'POST',
		},		
		
		
	})
}])