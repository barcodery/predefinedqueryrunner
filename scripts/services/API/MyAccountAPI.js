'use strict';

angular.module('BarcoderyApp')
.factory('MyAccountAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		getAccounts:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_my_accounts',
			isArray: true,
		},

		getAccountStatistics:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_my_account_statistics',
		},

		getInvetedUsers:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_invited_users_to_my_account',
			isArray: true,
		},

		inviteUser:
		{
			method: 'POST',
			url: API_BASE_URL + '/invite_user_to_my_account',
		},

		removeInvite:
		{
			method: 'POST',
			url: API_BASE_URL + '/remove_invite_from_my_account',
		},

		get_time_zones:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_time_zones',
			isArray: true,
		},

		get_invoices:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_invoices',
		},

		save_account_settings:
		{
			method: 'POST',
			url: API_BASE_URL + '/save_account_settings',
		},
	})
}])