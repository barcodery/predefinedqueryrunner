'use strict';

angular.module('BarcoderyApp')
.factory('CustomFieldNamesAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		loadCustomFieldNames:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_custom_field_names',
			isArray:true,
		},

		getAvailableCustomFieldIds:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_available_custom_field_ids',
			isArray: true,
		},

		loadBarcodeFieldNames:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_custom_barcode_names',
		},

		add:
		{
			method: 'POST',
			url: API_BASE_URL + '/add_edit_custom_field_name',
		},

		delete:
		{
			method: 'POST',
			url: API_BASE_URL + '/delete_custom_field_names',
			isArray: true,
		},
	})
}])