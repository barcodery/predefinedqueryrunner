'use strict';

angular.module('BarcoderyApp')
.factory('UserSettingsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		saveGirdState: {
			method: 'POST',
			// params: { grid_state: '@grid_state'},
			url: API_BASE_URL + '/set_user_settings',
		},

		get: {
			method: 'POST',
			url: API_BASE_URL + '/get_user_settings',
		},

		get_app_settings:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_app_settings',
		},

	})
}])