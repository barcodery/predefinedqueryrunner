'use strict';

angular.module('BarcoderyApp')
.factory('TagsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null, 
	{
		add: 
		{
			method: 'POST',
			// params: { name: '@name'},
			url: API_BASE_URL + '/add_tag',
		},
		
		get: 
		{
			method: 'POST',
			url: API_BASE_URL + '/get_tags',
			isArray:true,
		},
		
		delete: 
		{
			method: 'POST',
			// params: { ids: '@ids'},
			url: API_BASE_URL + '/delete_tags',
			isArray:true,
		},		
		
		edit: 
		{
			method: 'POST',
			// params: { category_id: '@tag_id', name:'@name'},
			url: API_BASE_URL + '/edit_tag',
		},		
		
		import: 
		{
			method: 'POST',
			// params: { items_json: '@items_json'},
			url: API_BASE_URL + '/import_tags',
		},				
		
	})
}])