'use strict';

angular.module('BarcoderyApp')
.factory('ReportsAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{

		get_available_reports:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_available_reports',
			isArray:true,
		},

		get_users_items_activity:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_report__users_items_activity',
			isArray:true,
		},



	})
}])