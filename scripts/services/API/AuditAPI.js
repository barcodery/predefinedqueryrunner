'use strict';

angular.module('BarcoderyApp')
.factory('AuditAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null,
	{
		create:
		{
			method: 'POST',
			url: API_BASE_URL + '/create_edit_audit',
		},

		get_audits:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_audits',
			isArray:true,
		},

		delete:
		{
			method: 'POST',
			url: API_BASE_URL + '/delete_audits',
			isArray:true,
		},

		close_audit:
		{
			method: 'POST',
			url: API_BASE_URL + '/close_audit',
		},

		// audit items

		get_audit_items:
		{
			method: 'POST',
			url: API_BASE_URL + '/get_audit_items',
			isArray:true,
		},

		increment_audit_items:
		{
			method: 'POST',
			url: API_BASE_URL + '/increment_audit_items',
			isArray:true,
		},

		decrement_audit_items:
		{
			method: 'POST',
			url: API_BASE_URL + '/decrement_audit_items',
			isArray:true,
		},

		set_audit_items_count:
		{
			method: 'POST',
			url: API_BASE_URL + '/set_audit_items_count',
			isArray:true,
		},

		add_extra_audit_items:
		{
			method: 'POST',
			url: API_BASE_URL + '/add_extra_audit_items',
		},

	})
}])