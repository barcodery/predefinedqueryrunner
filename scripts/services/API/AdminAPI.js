'use strict';

angular.module('BarcoderyApp')
.factory('AdminAPI', ['$resource',  'API_BASE_URL', function ($resource,  API_BASE_URL) {
	return $resource(API_BASE_URL, null, 
	{
		get_user_groups: 
		{
			method: 'POST',
			url: API_BASE_URL + '/get_user_groups',
			isArray:true,
		},
		
		add_user_group: 
		{
			method: 'POST',
			url: API_BASE_URL + '/add_user_group',
		},
		
		delete_user_groups: 
		{
			method: 'POST',
			url: API_BASE_URL + '/delete_user_groups',
			isArray:true,
		},	
		
		edit_user_group: 
		{
			method: 'POST',
			url: API_BASE_URL + '/edit_user_group',
		},		
		
		get_users: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_get_users',
			isArray:true,
		},		
	
		edit_user: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_edit_user',
			isArray:true,
		},	
		
		add_user: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_add_user',
			isArray:true,
		},		
		
		admin_change_password: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_change_password',
			isArray:false,
		},			
		
		admin_delete_user: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_delete_user',
			isArray:true,
		},			
		
		admin_undelete_user: 
		{
			method: 'POST',
			url: API_BASE_URL + '/admin_undelete_user',
			isArray:true,
		},	

		save_user_group_permissions: 
		{
			method: 'POST',
			url: API_BASE_URL + '/save_user_group_permissions',			
		},	
		
		
		
		
	})
}])