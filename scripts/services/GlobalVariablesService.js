'use strict';

angular.module('BarcoderyApp')
.factory('GlobalVariablesService',  [ function ($)
{
	var serviceInstance = {};

	serviceInstance.CustomFieldNamesCount = 18;
	serviceInstance.ItemsCategoriesFilter = null;
	serviceInstance.ItemsTagsFilter = null;
	serviceInstance.ItemsLocationsFilter = null;
	serviceInstance.AdminUserGroupFilter = null;

	serviceInstance.Scope_ItemTab_Locations = null;
	serviceInstance.Scope_ItemTab_Details = null;
	serviceInstance.Scope_ItemTab_InOutEntries = null;
	serviceInstance.Scope_ItemTab_Images = null;

	serviceInstance.scope_audits_tabset = null;
	serviceInstance.scope_audits_items_tabset = null;
	serviceInstance.scope_scanned_audits_items_tabset = null;

	serviceInstance.scope_requests_tabset = null;
	serviceInstance.scope_requests = null;
	serviceInstance.scope_request_details = null;

	serviceInstance.ItemsStatesFilter = null;

	return serviceInstance;
}]);
