'use strict';

angular.module('BarcoderyApp')
.factory('HelperService',  [ function ($)
{
	var serviceInstance = {};

	serviceInstance.date_format ='yyyy/MM/dd';

	serviceInstance.set_date_format = function(date_format)
	{
		serviceInstance.date_format = date_format;
	}

	serviceInstance.showHelpDialog = function(viewName, ev)
	{
		// $mdDialog.show({
			// controller: 'HelpDialogCtrl',
			// templateUrl: viewName,
			// targetEvent: ev,
		// })
		// .then(function(answer)
		// {
			// if (answer)
			// {
				// loadData();
			// }
		// },
		// function()
		// {
		// });
	};

	serviceInstance.CsvToJson = function(csv)
	{
		var lines=csv.split("\n");
		var result = [];
		var headers=lines[0].split(",");
		for (var i in headers)
		{
			if (headers[i][0] =='"') headers[i] = headers[i].slice( 1 );
			if (headers[i][headers[i].length - 1] =='"') headers[i] = headers[i].slice(0,-1);
		}

		for(var i=1;i<lines.length;i++){

			var obj = {};
			var currentline=lines[i].split(",");

			for(var j=0;j<headers.length;j++)
			{
				var value = currentline[j];

				if (!value || value.length == 0) continue;

				if (value[0] =='"') value = value.slice( 1 );
				if (value[value.length - 1] =='"') value = value.slice(0,-1);

				obj[headers[j]] = value;
			}

			result.push(obj);

		}

		//return result; //JavaScript object
		return JSON.stringify(result); //JSON
	}

	serviceInstance.getFormattedDate = function(date_string_from_server)
	{
		if (date_string_from_server)
		{
			var date = Helpers.Date.Parse(date_string_from_server);

			var formatted = date.format(serviceInstance.date_format +" hh:mm");

			return formatted;
		}
		else
		{
			return '';
		}
	}

	serviceInstance.getBoolFaSymbolClass = function(tick)
	{
		if (tick == true || tick == "1" || tick == 1)
		{
			return 'fa fa-check';
		}
		else
		{
			return 'fa fa-times';
		}
	}

	return serviceInstance;
}]);
