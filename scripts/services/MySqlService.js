'use strict';

angular.module('BarcoderyApp')
.factory('MySqlService',  [ '$q', function ($q)
{
    var mysql = require('mysql');

	var service = {};


    service.CheckConnection = function(connection_data)
    {
        var db_connection = mysql.createConnection({
          host     : connection_data.host,
          user     : connection_data.user,
          password : connection_data.password,
          port     : connection_data.port
        });

        var deferred = $q.defer();

        db_connection.query('SELECT 1', function(err, rows)
        {
            console.log(rows);
          // connected! (unless `err` is set)
        // });

        // db_connection.connect(function(err)
        // {
            if(!err)
            {
                deferred.resolve({});
                console.log("Database is connected ... nn");
            } else
            {
                deferred.reject({});
                console.log("Error connecting database ... nn");
                console.log(err)
            }

            db_connection.end(function(err)
            {
              // The connection is terminated now
            });
        });

        return deferred.promise;
    }

    service.CreateConnection = function(connection_data)
    {
        var db_connection = mysql.createConnection({
          host     : connection_data.host,
          user     : connection_data.user,
          password : connection_data.password,
          port     : connection_data.port
        });

        var deferred = $q.defer();

        db_connection.query('SELECT 1', function(err, rows)
        {
            console.log(rows);

            if(!err)
            {
                deferred.resolve(db_connection);
                console.log("Database is connected ... nn");
            } else
            {
                deferred.reject(err);
                console.log("Error connecting database ... nn");
                console.log(err)
            }
        });

        return deferred.promise;
    }

    service.RunQuery = function(db_connection, sql)
    {
        // var deferred = $q.defer();

        // db_connection.query(sql, function(err, rows, fields)
        // {
        //     console.log("Fields:");

        //     console.log(fields);
        //     if(!err)
        //     {
        //         deferred.resolve(db_connection);
        //         console.log("SQL success result:");
        //         console.log(rows);
        //     }
        //     else
        //     {
        //         deferred.reject(err);
        //         console.log("Error running sql:");
        //         console.log(err)
        //     }
        // });

        // return deferred.promise;

        var deferred = $q.defer();

        const mysqlErrors =
        {
            ER_EMPTY_QUERY: 'ER_EMPTY_QUERY',
        };

        db_connection.query(sql, (err, data, fields) =>
        {
            if (err && err.code === mysqlErrors.ER_EMPTY_QUERY)
            {
                deferred.resolve([])
                return;
            }

            if (err)
            {
                deferred.reject(_getRealError(db_connection, err));
                return;
            }

            if (!isMultipleQuery(fields))
            {
                deferred.resolve([parseRowQueryResult(data, fields)]);
                return;
            }

            deferred.resolve(data.map((_, idx) => parseRowQueryResult(data[idx], fields[idx])));
        });


        return deferred.promise;

    }

    function _getRealError(client, err)
    {
        if (client && client._protocol && client._protocol._fatalError)
        {
            return client._protocol._fatalError;
        }

        return err;
    }

    function isMultipleQuery(fields)
    {
        if (!fields) { return false; }
        if (!fields.length) { return false; }
        return (Array.isArray(fields[0]) || fields[0] === undefined);
    }

    function parseRowQueryResult(data, fields)
    {
      const isSelect = Array.isArray(data);

      var rows = (isSelect == true ? data : []);

      var result =
      {
        isSelect:isSelect,
        rows: rows,
        fields: fields || [],
        rowCount: isSelect ? (data).length : undefined,
        affectedRows: !isSelect ? data.affectedRows : undefined,
      };

      console.log(result);

      return result;
    }

	return service;
}]);

