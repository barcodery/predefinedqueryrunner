'use strict';

angular.module('BarcoderyApp')
.factory('ApiService', ['API_BASE_URL', 'AuthAPI','CustomFieldNamesAPI','CategoriesAPI' ,'LocationsAPI', 'TagsAPI', 'AccountsAPI', 'MyAccountAPI', 'NotificationAPI', 'ItemsAPI', 'UserSettingsAPI', 'AdminAPI', 'RequestsAPI', 'AuditAPI', 'ReportsAPI', 'StatesAPI',
			   function (API_BASE_URL, AuthAPI, CustomFieldNamesAPI, CategoriesAPI, LocationsAPI,TagsAPI, AccountsAPI, MyAccountAPI, NotificationAPI, ItemsAPI, UserSettingsAPI,AdminAPI ,RequestsAPI, AuditAPI, ReportsAPI, StatesAPI )
{
	var service={};

	service.AuthAPI = AuthAPI;
	service.CustomFieldNamesAPI = CustomFieldNamesAPI;
	service.CategoriesAPI = CategoriesAPI;
	service.LocationsAPI = LocationsAPI;
	service.TagsAPI = TagsAPI;
	service.AccountsAPI = AccountsAPI;
	service.MyAccountAPI = MyAccountAPI;
	service.NotificationAPI = NotificationAPI;
	service.ItemsAPI = ItemsAPI;
	service.UserSettingsAPI = UserSettingsAPI;
	service.AdminAPI = AdminAPI;
	service.RequestsAPI = RequestsAPI;
	service.AuditAPI = AuditAPI;
	service.ReportsAPI = ReportsAPI;
	service.StatesAPI = StatesAPI;

	service.get_api_base_url = function()
	{
		return API_BASE_URL;
	}

	return service;
}]);
