'use strict';

angular.module('BarcoderyApp')
.factory('CacheService',  ["ApiService",  "$q",
function (ApiService, $q)
{
	var serviceInstance = {};
	serviceInstance.barcode_names = null;
	serviceInstance.items = null;

	serviceInstance.always_reload = true;


	// --------------------------------------- Categories ---------------------------------------
	serviceInstance.categories = null;
	serviceInstance.reload_categories = false;

	serviceInstance.SetCategoriesDirty = function ()
	{
		serviceInstance.reload_categories = true;
	}

	serviceInstance.GetCategories = function(reloadFromServer)
	{
		if (serviceInstance.categories == null || reloadFromServer || serviceInstance.reload_categories == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_categories = false;

			var promise = ApiService.CategoriesAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.categories = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.categories);

			return deferred.promise;
		}
	};


	// --------------------------------------- Tags ---------------------------------------
	serviceInstance.tags = null;
	serviceInstance.reload_tags = false;

	serviceInstance.SetTagsDirty = function ()
	{
		serviceInstance.reload_tags = true;
	}

	serviceInstance.GetTags = function(reloadFromServer)
	{
		if (serviceInstance.tags == null || reloadFromServer || serviceInstance.reload_tags == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_tags = false;

			var promise = ApiService.TagsAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.tags = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.tags);

			return deferred.promise;
		}
	};


	// --------------------------------------- States ---------------------------------------
	serviceInstance.states = null;
	serviceInstance.reload_states = false;

	serviceInstance.SetStatesDirty = function ()
	{
		serviceInstance.reload_states = true;
	}

	serviceInstance.GetStates = function(reloadFromServer)
	{
		if (serviceInstance.states == null || reloadFromServer || serviceInstance.reload_states == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_states = false;

			var promise = ApiService.StatesAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.states = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.states);

			return deferred.promise;
		}
	};


	// --------------------------------------- Locations ---------------------------------------
	serviceInstance.locations = null;
	serviceInstance.reload_locations = false;

	serviceInstance.SetLocationsDirty = function ()
	{
		serviceInstance.reload_locations = true;
	}

	serviceInstance.GetLocations = function(reloadFromServer)
	{
		if (serviceInstance.locations == null || reloadFromServer || serviceInstance.reload_locations == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_locations = false;

			var promise = ApiService.LocationsAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.locations = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.locations);

			return deferred.promise;
		}
	};


	// --------------------------------------- Custom Field Names ---------------------------------------
	serviceInstance.custom_field_names = null;
	serviceInstance.reload_custom_field_names = false;

	serviceInstance.SetCustomFieldNamesDirty = function ()
	{
		serviceInstance.reload_custom_field_names = true;
	}

	serviceInstance.GetCustomFieldNames = function(reloadFromServer)
	{
		if (serviceInstance.custom_field_names == null || reloadFromServer || serviceInstance.reload_custom_field_names == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_custom_field_names = false;

			var promise = ApiService.CustomFieldNamesAPI.loadCustomFieldNames().$promise;
			promise.then(function(result)
			{
				serviceInstance.custom_field_names = result;

				for (var i = 0; i< serviceInstance.custom_field_names.length; i++)
				{
					var item = serviceInstance.custom_field_names[i];
					item.is_unique = item.is_unique == 1 ? true : false;
					item.is_identifier = item.is_identifier == 1 ? true : false;
				}
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.custom_field_names);

			return deferred.promise;
		}
	};


	// --------------------------------------- Barcode names ---------------------------------------
	serviceInstance.barcode_names = null;
	serviceInstance.reload_barcode_names = false;

	serviceInstance.SetBarcodeNamesDirty = function ()
	{
		serviceInstance.reload_barcode_names = true;
	}

	serviceInstance.GetBarcodeNames = function(reloadFromServer)
	{
		if (serviceInstance.barcode_names == null || reloadFromServer || serviceInstance.reload_barcode_names == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_barcode_names = false;

			var promise = ApiService.CustomFieldNamesAPI.loadBarcodeFieldNames().$promise;
			promise.then(function(result)
			{
				serviceInstance.barcode_names = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.barcode_names);

			return deferred.promise;
		}
	};


	// --------------------------------------- Items ---------------------------------------
	serviceInstance.items = null;
	serviceInstance.reload_items = false;

	serviceInstance.SetItemsDirty = function ()
	{
		serviceInstance.reload_locations = true;
		serviceInstance.reload_items = true;
	}

	serviceInstance.GetItems = function(reloadFromServer)
	{
		if (serviceInstance.items == null || reloadFromServer || serviceInstance.reload_items == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_items = false;

			var promise = ApiService.ItemsAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.items = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.items);

			return deferred.promise;
		}
	};



	// --------------------------------------- User settings ---------------------------------------
	serviceInstance.user_settings = null;
	serviceInstance.reload_user_settings = false;

	serviceInstance.SetUserSettingsDirty = function ()
	{
		serviceInstance.reload_user_settings = true;
	}

	serviceInstance.GetUserSettings = function(reloadFromServer)
	{
		if (serviceInstance.user_settings == null || reloadFromServer || serviceInstance.reload_user_settings == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_user_settings = false;

			var promise = ApiService.UserSettingsAPI.get().$promise;
			promise.then(function(result)
			{
				serviceInstance.user_settings = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.user_settings);

			return deferred.promise;
		}
	};


	// --------------------------------------- Available custom field types ---------------------------------------
	serviceInstance.available_custom_field_types = null;
	serviceInstance.reload_available_custom_field_types = false;

	serviceInstance.SetAvailableCustomFieldTypesDirty = function ()
	{
		serviceInstance.reload_available_custom_field_types = true;
	}

	serviceInstance.GetAvailableCustomFieldTypes = function(reloadFromServer)
	{
		if (serviceInstance.available_custom_field_types == null || reloadFromServer || serviceInstance.reload_available_custom_field_types == true || serviceInstance.always_reload)
		{
			// get from server
			serviceInstance.reload_available_custom_field_types = false;

			var promise = ApiService.CustomFieldNamesAPI.getAvailableCustomFieldIds().$promise;
			promise.then(function(result)
			{
				serviceInstance.available_custom_field_types = result;
			},
			function(err)
			{
			});

			return promise;
		}
		else
		{
			// return cached
			var deferred = $q.defer();
			deferred.resolve(serviceInstance.available_custom_field_types);

			return deferred.promise;
		}
	};







	serviceInstance.SetEverythingToDirty = function()
	{
		serviceInstance.reload_items = true;
		serviceInstance.reload_barcode_names = true;
		serviceInstance.reload_custom_field_names = true;
		serviceInstance.reload_locations = true;
		serviceInstance.reload_tags = true;
		serviceInstance.reload_categories = true;
		serviceInstance.reload_user_settings = true;
	}

	return serviceInstance;
}]);
