'use strict';

angular.module('BarcoderyApp')
.factory('UserService',  ["ApiService",  "$q", "$rootScope",
function (  ApiService,   $q, $rootScope )
{
	var serviceInstance = {};

	serviceInstance.user=null;
	serviceInstance._reloaduser = false;
	serviceInstance.is_logged_in=false;
	serviceInstance._scope = null;


	serviceInstance.login = function(username, password)
	{
		var promise = ApiService.AuthAPI.login({ email: username, password: password }).$promise;

		promise.then(function(result)
		{

		},
		function(err)
		{
		});

		return promise;
	};

	serviceInstance.register = function(name, email, password, promo_code)
	{
		var data = { name:name, email: email, password: password };

		if (promo_code && promo_code != null)
		{
			data.promo_code = promo_code;
		}

		var promise = ApiService.AuthAPI.register(data).$promise;

		promise.then(function(result)
		{
			$rootScope.$broadcast('USER_LOGGED_IN_LOGGED_OUT_EVENT', {});
		},
		function(err)
		{
		});

		return promise;
	};

	serviceInstance.logout = function()
	{
		var promise =ApiService.AuthAPI.logout().$promise;

		promise.then(function(result)
		{
			serviceInstance.user=null;
			serviceInstance.is_logged_in = false;

			$rootScope.$broadcast('USER_LOGGED_IN_LOGGED_OUT_EVENT', {});
		},
		function(err)
		{
		});

		return promise;
	};

	// serviceInstance.userPromise = null;
	serviceInstance.getUser = function(reloadFromServer)
	{
		if (serviceInstance.user == null || reloadFromServer ||  serviceInstance._reloaduser)
		{
			serviceInstance._reloaduser = false;
			var promise = ApiService.AuthAPI.getUser().$promise;

			promise.then(function(result)
			{
				serviceInstance.is_logged_in = result.user_id ? true: false;
				if (result.user_id)
				{
					serviceInstance.user = result;

					$rootScope.$broadcast('USER_LOGGED_IN_LOGGED_OUT_EVENT', {});
				}
			},
			function(err)
			{
			});

			// serviceInstance.userPromise = promise;

			return promise;
		}
		else
		{
			// return serviceInstance.userPromise;
			var deferred = $q.defer();
			deferred.resolve({user:serviceInstance.user});

			return deferred.promise;
		}
	};

	return serviceInstance;
}]);
