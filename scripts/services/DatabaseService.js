'use strict';

angular.module('BarcoderyApp')
.factory('DatabaseService',  [ '$q', 'MySqlService', 'toastr',  function ($q, MySqlService, toastr)
{
    var mysql = require('mysql');

	var service = {};
    service.database = {};
    service.database.db_connection = null;

    service.CheckConnection = function(connection_data)
    {
        switch (connection_data.type)
        {
            case "mysql":
            {
                return MySqlService.CheckConnection(connection_data);
                break;
            }

            case "mssql":
            {

            }

            case "postgresql":
            {

            }
        }
    }

    service.Connect = function(connection_data)
    {
        var promise = null;

        service.database.state = 'connecting';
        service.database.data = connection_data;
        service.database.error = "";

        toastr.info(connection_data.name, 'Connecting');

        switch (connection_data.type)
        {
            case "mysql":
            {
                promise = MySqlService.CreateConnection(connection_data);
                break;
            }

            case "mssql":
            {

            }

            case "postgresql":
            {

            }
        }

        promise.then(function(result)
        {
            service.database.db_connection = result;
            service.database.state = 'connected';
            toastr.success(connection_data.name, 'Connected');

        },
        function(err)
        {
            service.database.state = 'connection_failed';
            service.database.error = err;
            service.database.db_connection = null;
            toastr.error(connection_data.name, 'Connection failed');
        });

    }

    service.RunQuery = function(sql)
    {
        var promise = null;

        if (service.database.db_connection != null)
        {
            if (service.database.data.type == 'mysql')
            {
                promise = MySqlService.RunQuery(service.database.db_connection, sql);
            }
        }

        promise.then(x =>
        {

        },
        err =>
        {
            // toastr.error('Query error', "Error");
        });

        return promise;
    }

	return service;
}]);
