'use strict';

angular.module('BarcoderyApp')
.factory('NotificationService',  ["ApiService", function (ApiService) 
{
	var service = {};
	
	service.setNotificationForUserByEmail = function(userEmail, text)
	{
		var promise = ApiService.NotificationAPI.setForUserEmail({email:userEmail, text:text}).$promise;
		
		promise.then(function(result) 
		{
			
		}, 
		function(err) 
		{
		});		
		
		return promise;
	};	
	
	service.setNotificationForUserById = function(userId, text)
	{
		var promise = ApiService.NotificationAPI.setForUserId({user_id:userId, text:text}).$promise;
		
		promise.then(function(result) 
		{
			
		}, 
		function(err) 
		{
		});		
		
		return promise;
	};	
	
	return service;
}]);

