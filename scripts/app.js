'use strict';

var app = angular.module('BarcoderyApp', [
'ngAnimate'
,'ngRoute'
,'ngResource'
//,'ngMaterial'
,'ui.grid'
,'ui.grid.selection'
,'ui.grid.pinning'
,'ui.grid.resizeColumns'
,'ui.grid.autoResize'
,'ui.grid.saveState'
,'ui.grid.moveColumns'
,'ui.grid.exporter'
,'ui.grid.importer'
,'ui.grid.grouping'
,'ui.grid.edit'
,'ui.grid.autoResize'
,'ui.bootstrap'
,'multi-select'
,'linkify'
,'ui.select'
,'ngSanitize'
,'flow'
,'ngFileUpload'
,'gettext'
,'color.picker'
,'ui.ace'
,'daterangepicker'
,'toastr'
]);

app.config(function($routeProvider, $locationProvider, toastrConfig)
{
	$locationProvider.hashPrefix('!');

	$routeProvider
	// .when('/', {
	// templateUrl : 'views/LandingView.html',
	// // controller  : 'LandingCtrl'
	// })
	// .when('/accounts', {
	// templateUrl : 'views/Manage/AccountsView.html',
	// controller  : 'AccountsCtrl',
	// access_level: ACCESS_LEVELS.ONLY_REGISTERED
	// })
	.when('/queries', {
		templateUrl : 'views/QueriesView.html',
		controller  : 'QueriesCtrl',
	})
	.when('/connections', {
		templateUrl : 'views/ConnectionsView.html',
		controller  : 'ConnectionsCtrl',
	})

	.when('/', {
		templateUrl : 'views/DashBoardView.html',
		controller  : 'DashBoardCtrl',
	})

	.when('/dashboard', {
		templateUrl : 'views/DashBoardView.html',
		controller  : 'DashBoardCtrl',
	})

	.when('/query/:query_name', {
		templateUrl : 'views/QueryView.html',
		controller  : 'QueryCtrl',
	})

	// .when('/fieldnames', {
	// 	templateUrl : 'views/Manage/FieldNamesView.html',
	// 	controller  : 'FieldNamesCtrl',
	// })

	// .when('/item_detail/:item_id', {
	// 	templateUrl : 'views/Manage/ItemDetailView.html',
	// 	controller  : 'ItemDetailCtrl',
	// })
	;

	// $locationProvider.html5Mode(true);

	  // angular.extend(toastrConfig,
	  // {
	  //   autoDismiss: false,
	  //   containerId: 'toast-container',
	  //   maxOpened: 1,
	  //   newestOnTop: true,
	  //   positionClass: 'toast-top-right',
	  //   preventDuplicates: false,
	  //   preventOpenDuplicates: false,
	  //   target: 'body'
	  // });
});

app.run(function($rootScope,  $location, $route, gettextCatalog, i18nService)
{

	// i18nService.setCurrentLang("cz");
 // 	gettextCatalog.setCurrentLanguage("cs_CZ");
 // 	gettextCatalog.debug = false;

	// i18nService.setCurrentLang("cz");
 // 	gettextCatalog.setCurrentLanguage('sk_SK');
 // 	gettextCatalog.debug = true;

	i18nService.setCurrentLang("en");
 	gettextCatalog.setCurrentLanguage("en");
 	gettextCatalog.debug = false;



 	//http://stackoverflow.com/questions/16674279/how-to-nest-ng-view-inside-ng-include
	$route.reload();

	$rootScope.$on('$routeChangeStart', function(evt, next, current)
	{
		if ( next && typeof next.access_level  != 'undefined')
		{
			// if (next.access_level == ACCESS_LEVELS.ONLY_REGISTERED)
			// {
			// 	if (!UserService.is_logged_in)
			// 	{
			// 		UserService.getUser(true).then(function(result)
			// 		{
			// 			if (!UserService.is_logged_in)
			// 			{
			// 				$location.path('/');
			// 			}
			// 		}
			// 		,
			// 		function(err)
			// 		{
			// 			$location.path('/');
			// 		});
			// 	}

			// 	UserService.getUser().then(function(result)
			// 	{
			// 		var user = UserService.user;

			// 		if (next.originalPath == "/fieldnames" && user.perm_admin_fields == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/my_account"  && user.perm_admin_account == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/categories"  && user.perm_categories_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/locations"  && user.perm_locations_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/tags"  && user.perm_tags_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/tags"  && user.perm_tags_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/items"  && user.perm_items_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath == "/new_item"  && user.perm_items_edit == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/edit_item") > -1  && user.perm_items_edit == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/admin_user_groups") > -1  && user.perm_admin_user_groups == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/admin_users") > -1  && user.perm_admin_users == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/admin_permissions") > -1  && user.perm_admin_permissons == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/allrequests") > -1  && user.perm_all_requests_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}

			// 		if (next.originalPath.indexOf("/requests") > -1  && user.perm_my_requests_view == 0)
			// 		{
			// 			$location.path('/dashboard');
			// 		}
			// 	});
			// }
		}
		if (next) next.$$route.originalPath =="/dashboard"
	});

	$rootScope.$on('$viewContentLoaded', function(evt, next, current)
	{
		if ($.AdminLTE.layout) $.AdminLTE.layout.activate();
	});
});

/*
	* AngularJS default filter with the following expression:
	* "person in people | filter: {name: $select.search, age: $select.search}"
	* performs a AND between 'name: $select.search' and 'age: $select.search'.
	* We want to perform a OR.
*/
app.filter('propertiesFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			items.forEach(function(item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
			} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text)
                {
                    var transformedInput = text.match(/^[0-9]+(\.{1}[0-9]*){0,1}$/);

                    if (transformedInput) return transformedInput[0];

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
					}
                    return transformedInput;
				}
                return undefined;
			}
            ngModelCtrl.$parsers.push(fromUser);
		}
	};
});

app.directive('datepickerPopup', function (dateFilter, datepickerPopupConfig) {
	return {
		restrict: 'A',
		priority: 1,
		require: 'ngModel',
		link: function(scope, element, attr, ngModel) {
			var dateFormat = attr.datepickerPopup || datepickerPopupConfig.datepickerPopup;
			ngModel.$formatters.push(function (value) {
				return dateFilter(value, dateFormat);
			});
		}
	};
});

angular.module('BarcoderyApp')
.directive('fixSidebar', ['$timeout', function ($timeout) {
	return function($scope, element, attrs)
	{
		$timeout(function ()
		{

		}, 0, false);
	};
}]);