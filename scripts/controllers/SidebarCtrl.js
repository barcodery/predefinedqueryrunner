'use strict';

angular.module('BarcoderyApp')
.controller('SidebarCtrl', ['$scope', '$location', '$timeout', 'SettingsService', 'DatabaseService', '$modal',
	function ($scope, $location, $timeout, SettingsService, DatabaseService, $modal)
{
	init();

	// $scope.queries = SettingsService.setting.queries;
	$scope.SettingsService = SettingsService;
	$scope.DatabaseService = DatabaseService;


	$scope.redirect = function(path, ev)
	{
		ev.preventDefault();
		$location.path(path);
	}

	$scope.isActive = function(path)
	{
		var currentPath = $location.path();
		var result = currentPath.indexOf(path) > -1;
		return result;
	}

	$scope.isActiveExactly = function(path)
	{
		var currentPath = $location.path();
		var result = currentPath == path;
		return result;
	}

	$scope.ConnectionDetailsClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'ConnectionDetailDialogCtrl',
			templateUrl: 'views/dialogs/ConnectionDetailDialog.html',
			size: '50-percent',
			// resolve: {
			// items: function () {
			// return $scope.items;
			// }
			// }
		});

		modalInstance.result.then(function (answer)
		{
			// if (answer)
			// {
			// 	CacheService.SetCategoriesDirty();
			// 	loadData();
			// }
		});
	}

	$scope.$on('HIDE_SIDEBAR_EVENT', function (event, data)
	{
	});

	$scope.$on('SHOW_SIDEBAR_EVENT', function (event, data)
	{
	});

	$scope.$on('TOGGLE_SIDEBAR_EVENT', function (event, data)
	{
	});

	$scope.$on('USER_LOGGED_IN_LOGGED_OUT_EVENT', function (event, data)
	{
		loadUser();

		if (!$.AdminLTE.initialized) AdminLteInit();
	});


	function init()
	{
		// loadUser();
	}

	function userLoaded()
	{
	}

}]);
