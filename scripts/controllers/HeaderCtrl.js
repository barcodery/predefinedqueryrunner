'use strict';

angular.module('BarcoderyApp')
.controller('HeaderCtrl', ['$scope', '$location',    '$modal',
function ($scope, $location,   $modal)
{
	// $scope.isLoggedIn = false;
	$scope.showNotification = false;

	$scope.header = '';

	init();





	$scope.redirectToDashboard = function()
	{
		$location.path('/dashboard');
	}

	// $scope.hideNotification = function()
	// {
	// 	$scope.showNotification = false;
	// 	ApiService.NotificationAPI.setSeen().$promise.then(function(result)
	// 	{
	// 		$scope.showNotification = false;
	// 	},
	// 	function(err)
	// 	{
	// 	});
	// }

	$scope.showHideSidebar = function(ev)
	{
		ev.preventDefault();
	}

	$scope.$on('RELOAD_USER_EVENT', function (event, data)
	{
		getLoggedUser();
	});

	$scope.$on('$routeChangeSuccess', function (event, data)
	{
		// checkNotification();
	});

	// function checkNotification()
	// {
	// 	if (!$scope.isLoggedIn) return;

	// 	ApiService.NotificationAPI.get().$promise.then(function(result)
	// 	{
	// 		if (result.text)
	// 		{
	// 			$scope.notification = result;
	// 			$scope.showNotification = true;
	// 		}
	// 	},
	// 	function(err)
	// 	{
	// 	});
	// }

	function init()
	{
		// getLoggedUser();

		// getAppSettings();

		setTimeout((function()
		{
			//if (!$.AdminLTE.initialized)
				AdminLteInit();
		}), 0);
	}


}]);
