'use strict';

angular.module('BarcoderyApp')
.controller('AddEditQueryDialogCtrl', ['$scope',  '$location', '$modal', '$routeParams', 'SettingsService', '$modalInstance','query',
function($scope,  $location,  $modal, $routeParams,  SettingsService, $modalInstance, query)
{
    var old_query = _.clone(query);
    if (query == null)
    {
        $scope.query = {};
        $scope.query.name = '';
        $scope.query.description = '';
        $scope.query.parameters = [];
        $scope.query.sql = "SELECT * FROM table WHERE 1 = {{PARAM1}}";
    }
    else
    {
        $scope.query = _.clone(query);
    }

    $scope.query = _.clone($scope.query);


    $scope.parameterTypesEnum =
    [
         "Text"
        ,"DateTime"
        ,"Checkbox"
        ,"Combobox"
    ];

    $scope.generateIsIntervalEnum = function(_param)
    {
        $scope.isIntervalEnum =
        [
             {id:0, type:'no'   , text:'No interval'}
            ,{id:1, type:'start', text:'Interval start'}
            // ,{id:3, type:'start', text:'Interval end for'}
        ];

        for (var i = 0; i < $scope.query.parameters.length; i++)
        {
            var param = $scope.query.parameters[i];

            if (param.type == "DateTime" && param.interval && param.interval.type == 'start')  // is interval start
            {
                var hasEndInterval = $scope.query.parameters.some(function (paramEleme, index, array)
                    {
                        var result = paramEleme.type == "DateTime" && paramEleme.interval && paramEleme.interval.type == 'end' && paramEleme.interval.start_id == param.id;
                        return result;
                    });

                if (!hasEndInterval)
                {
                  $scope.isIntervalEnum.push({type:'end', text:'Interval end for ' + param.id, start_id:param.id});
                }
            }
        }
    }

	// $scope.item = {};
    // $scope.item.title = '';
    // $scope.item.note = '';
    // $scope.item.description = '';
    $scope.categories = [];
    $scope.tags = [];
    $scope.validation_error = '';
    $scope.working = false;
    $scope.alerts = [];
    $scope.editing_item = false;
    $scope.edit_field_enabled = {}
    $scope.images_object_url_and_id = [];

    $scope.onlyNumbers = /^\d+$/;

    $scope.aceLoaded = function (_editor)
    {
        _editor.setFontSize(16);
    }

    $scope.aceChanged = function ()
    {
        var params = $scope.query.sql.match(/{{.*?}}/g);
        var uniqueparams = _.uniq(params, function(item, key, a) { return item;  });

        parametersChanged(uniqueparams);
    }

    function parametersChanged(params)
    {
        var paramsToAdd    = params.filter(function (item1) { return ! $scope.query.parameters.some(function(item2) { return item1 == item2.id}); });
        var paramsToDelete = $scope.query.parameters.filter(function (item1) { return ! params.some(function(item2) { return item1.id == item2}); });

        $scope.query.parameters = $scope.query.parameters.filter(function (item1) { return !paramsToDelete.some(function (item2) { return item1.id == item2.id} ) });

        for (var i = 0; i < paramsToAdd.length; i++)
        {
            $scope.query.parameters.push(
                {
                    id: paramsToAdd[i],
                    interval: {id:0, type:'no'   , text:'No interval'}
                });
        }

        $scope.generateIsIntervalEnum();
    }



    // init();

    $scope.cancelClicked = function()
    {
        $modalInstance.dismiss('cancel');
    }

    $scope.redirect = function(path, ev)
    {
        // ev.preventDefault();
        $location.path(path);
	}

    $scope.saveClicked = function()
    {
    	var isValid = validate();

    	if (!isValid)
    	{
    		return;
		}

        $scope.working = true;

        var promise = null;

        if (query == null)
        {
            promise = SettingsService.AddQuery($scope.query);
        }
        else
        {
            query = $scope.query;
            promise = SettingsService.EditQuery(old_query, $scope.query);
        }

        promise.then(function(result)
        {
            $scope.working = false;
            $modalInstance.close(true);
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
	}

	$scope.createNewCategoryClicked = function()
	{
		var modalInstance = $modal.open(
		{
			controller: 'AddCategoryDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCategoryDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function(answer)
		{
			if (answer)
			{
				var selectedCategories = _.where($scope.categories,
				{
					ticked: true
				});

				CacheService.SetCategoriesDirty();
				var promise = getCategroies();

				promise.then(function(result)
				{
					var last_item = _.last($scope.categories);
					last_item.ticked = true;

					for (var i in selectedCategories)
					{
						var item = _.findWhere($scope.categories,
						{
							category_id: selectedCategories[i].category_id
						});
						item.ticked = true;
					}
				});
			}
		});
	}

	$scope.createNewTagClicked = function()
	{
		var modalInstance = $modal.open(
		{
			controller: 'AddTagDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddTagDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function(answer)
		{
			if (answer)
			{
				var selectedTags = _.where($scope.tags,
				{
					ticked: true
				});

				CacheService.SetTagsDirty();
				var promise = getTags(true);

				promise.then(function(result)
				{
					var last_item = _.last($scope.tags);
					last_item.ticked = true;

					for (var i in selectedTags)
					{
						var item = _.findWhere($scope.tags,
						{
							tag_id: selectedTags[i].tag_id
						});
						item.ticked = true;
					}
				});
			}
		});
	}

    $scope.createNewStateClicked = function()
    {
        var modalInstance = $modal.open(
        {
            controller: 'AddEditStateDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddEditStateDialog.html',
            size: 'sm',
            resolve:
            {
                item: function ()
                {
                    return null;
                }
            }
        });

        modalInstance.result.then(function(answer)
        {
            if (answer)
            {
                var selectedStates = _.where($scope.states,
                {
                    ticked: true
                });

                CacheService.SetStatesDirty();
                var promise = getStates(true);

                promise.then(function(result)
                {
                    var last_item = _.first($scope.states);
                    last_item.ticked = true;

                    for (var i in selectedStates)
                    {
                        var item = _.findWhere($scope.states,
                        {
                            tag_id: selectedStates[i].state_id
                        });
                        item.ticked = true;
                    }
                });
            }
        });
    }

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.get_custom_fields_by_type = function(type)
	{
		var result = [];

		if ($scope.custom_filed_names)
		{
			for (var i = 0; i < $scope.custom_filed_names.length; i++)
			{
				if ($scope.custom_filed_names[i].type == type)
				{
					result.push($scope.custom_filed_names[i]);
				}
			}
		}

		return result;
	}

    $scope.get_custom_fields_by_types = function(types_array)
    {
        var result = [];

        if ($scope.custom_filed_names)
        {
            for (var i = 0; i < $scope.custom_filed_names.length; i++)
            {
                if (_.contains(types_array, $scope.custom_filed_names[i].type))
                {
                    result.push($scope.custom_filed_names[i]);
                }
            }
        }

        return result;
    }

	$scope.on_controller_init = function()
	{
		GUI.InitNumberOnlyInputs();
	}

    $scope.remove_existing_image = function(cfn, image_id)
    {
        var cfn_id = cfn.custom_field_name_id;

        var field_url = "custom_field_id_" + cfn_id + "_image_url";
        var field_id  = "custom_field_id_" + cfn_id;

        var urls = $scope.item[field_url];
        var ids  = $scope.item[field_id];

        var splitted_urls = [];
        var splitted_ids  = [];

        if (urls) splitted_urls = urls.split(",");
        if (ids) splitted_ids   = ids.split(",");

        for (var i = 0; i < splitted_ids.length; i++)
        {
            var id  = splitted_ids[i];
            var url = splitted_urls[i];

            if (id == image_id)
            {
                splitted_ids = _.without(splitted_ids, image_id);
                splitted_urls = _.without(splitted_urls, url);
                break;
            }
        };

        $scope.item[field_id] = splitted_ids.join(',');
        $scope.item[field_url] = splitted_urls.join(',');

        load_existing_images_into_var(cfn);
    }

    $scope.flowImageAddedEvent = function($file, $event, $flow, totalFilesCount)
    {
        event.preventDefault();

        if (!($file.getExtension() == "png" || $file.getExtension() == "gif" || $file.getExtension() == "jpg" || $file.getExtension() == "jpeg"))
        {
        return false;
        }

        if (totalFilesCount + images_count_to_add >= $scope.AppSettings.AllowedImagesPerImageField )
        {
            return false;
        }

        images_count_to_add ++;

        return true;
    }

    $scope.flowImagesAddedEvent = function($file, $event, $flow, totalFilesCount)
    {
        images_count_to_add = 0;
        return true;
    }

	// private

	function getSelectedCategoriesId()
	{
		var result = [];

		for (var i in $scope.categories)
		{
			if ($scope.categories[i].ticked)
			{
				result.push($scope.categories[i].category_id);
			}
		}

		return result;
	}

	function getSelectedTagsId()
	{
		var result = [];

		for (var i in $scope.tags)
		{
			if ($scope.tags[i].ticked)
			{
				result.push($scope.tags[i].tag_id);
			}
		}

		return result;
	}

    function getSelectedStatesId()
    {
        var result = [];

        for (var i in $scope.states)
        {
            if ($scope.states[i].ticked)
            {
                result.push($scope.states[i].state_id);
            }
        }

        return result;
    }

    function loadCustomFieldNames()
    {
        // ApiService.CustomFieldNamesAPI.loadCustomFieldNames().$promise
        var promise = CacheService.GetCustomFieldNames();
        promise.then(function(result)
        {
        	$scope.custom_filed_names = result;
        	init_item_default_valuse();
		},
        function(err)
        {
        	$scope.custom_filed_names = [];
		});
	}

    function getCategroies()
    {
    	var promise = CacheService.GetCategories();
    	promise.then(function(result)
    	{
    		$scope.categories = result;

    		_.each($scope.categories, function(item)
    		{
    			item.ticked = false;
			});
		},
    	function(err) {});

    	return promise;
	}

    function getTags()
    {
    	var promise = CacheService.GetTags();
    	promise.then(function(result)
    	{
    		$scope.tags = result;

    		_.each($scope.tags, function(item)
    		{
    			item.ticked = false;
			});
		},
    	function(err) {});

    	return promise;
	}

    function getStates()
    {
        var promise = CacheService.GetStates();
        promise.then(function(result)
        {
            $scope.states = result;

            _.each($scope.states, function(item)
            {
                item.ticked = false;
            });
        },
        function(err) {});

        return promise;
    }

    function init()
    {
	}

    function init_item_default_valuse()
    {
    	$scope.item = {};

    	for (var i = 0; i < $scope.custom_filed_names.length; i++)
    	{
    		var cfn = $scope.custom_filed_names[i];
    		var value = '';

            if (cfn.type == 3) // bit value
            {
            	value = 0;
			}

            $scope.item['custom_field_id_' + cfn.custom_field_name_id] = value;
		};
	}

    function loadExistingItem()
    {
        if (!selected_items)
        {
            return;
        }

		// $scope.item = selected_items[0];


        // get intersected categories
        var array = [];
        _.each(selected_items, function (item) { if (item.categories_ids) array.push(item.categories_ids.split(',')); else array.push(0);  } );
        var categories_ids_to_load = _.intersection.apply(_, array);

        // load intersected categories
		for (var i in $scope.categories)
		{
			for (var j in categories_ids_to_load)
			{
				if ($scope.categories[i].category_id == categories_ids_to_load[j])
				{
					$scope.categories[i].ticked = true;
				}
			}
		}

        // load intersected tags
        var array = [];
        _.each(selected_items, function (item) {  if (item.tags_ids) array.push(item.tags_ids.split(',')); else array.push(0); } );
        var tags_ids_to_load = _.intersection.apply(_, array);

		// load tags from item
		for (var i in $scope.tags)
		{
			for (var j in tags_ids_to_load)
			{
				if ($scope.tags[i].tag_id == tags_ids_to_load[j])
				{
					$scope.tags[i].ticked = true;
				}
			}
		}

        // load states
        var array = [];
        _.each(selected_items, function (item) {  if (item.states_ids) array.push(item.states_ids.split(',')); else array.push(0); } );
        var states_ids_to_load = _.intersection.apply(_, array);

        for (var i in $scope.states)
        {
            for (var j in states_ids_to_load)
            {
                if ($scope.states[i].state_id == states_ids_to_load[j])
                {
                    $scope.states[i].ticked = true;
                }
            }
        }

		// format date values to date
		var date_cfn = $scope.get_custom_fields_by_type(4);
		for (var i = 0; i < date_cfn.length; i++)
		{
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + date_cfn[i].custom_field_name_id]] ); } );
            var intersected_dates = _.intersection.apply(_, array);

            if (!intersected_dates || intersected_dates.length <= 0) continue;

            if (intersected_dates[0] != null)
            {
                var t = intersected_dates[0].split(/[- :]/);
    			// var t = $scope.item['custom_field_id_' + date_cfn[i].custom_field_name_id].split(/[- :]/);

    			$scope.item['custom_field_id_' + date_cfn[i].custom_field_name_id] = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
            }
		}

        var bit_cfn = $scope.get_custom_fields_by_type(3);
        for (var i = 0; i < bit_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + bit_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            if (intersected_values[0] == "1")
            {
                $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 1;
            }
            else
            {
                $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 0;
            }

            // if ($scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] == "1")
            // {
            //     $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 1;
            // }
            // else
            // {
            //     $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 0;
            // }
        }


         // text, number/ barcode values
        var other_cfn = $scope.get_custom_fields_by_types(["1","2","5","6","7","9"]);
        for (var i = 0; i < other_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + other_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + other_cfn[i].custom_field_name_id] = intersected_values[0];
        }


        var images_cfn = $scope.get_custom_fields_by_type(8);
        for (var i = 0; i < images_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + images_cfn[i].custom_field_name_id +"_image_url" ] ] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + images_cfn[i].custom_field_name_id +"_image_url"] = intersected_values[0];

            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + images_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + images_cfn[i].custom_field_name_id] = intersected_values[0];

            load_existing_images_into_var(images_cfn[i]);
        }
	}

    function load_existing_images_into_var(cfn)
    {
        var cfn_id = cfn.custom_field_name_id;
        var field_url = "custom_field_id_" + cfn_id + "_image_url";
        var field_id = "custom_field_id_" + cfn_id;

        var urls = $scope.item[field_url];
        var ids = $scope.item[field_id];

        var splitted_urls = [];
        var splitted_ids = [];

        if (urls) splitted_urls = urls.split(",");
        if (ids) splitted_ids = ids.split(",");

        var result = [];
        for (var i = 0; i < splitted_urls.length; i++)
        {
            var obj = {url : splitted_urls[i], id : splitted_ids[i]};
            result.push(obj);
        };

        $scope.images_object_url_and_id[cfn_id] = result;
    }

	function load_account()
	{
		var promise = ApiService.MyAccountAPI.getAccounts()
		.$promise;

		promise.then(function(result)
		{
			$scope.account = result[0];
			$scope.calendar_format = $scope.account.date_format;
		},
		function(err) {}
		);

		return promise;
	}

	function validate()
	{
        $scope.alerts = [];

        if ($scope.query.name == '')
        {
            $scope.alerts.push({ type: 'danger', msg: 'Please fill query name.'});
            return false;
        }

        var queries = SettingsService.GetQueriesSync();
        var existName = queries.some(function(elem)
            {
                if (query == null)
                {
                    return elem.name == $scope.query.name ;
                }
                else
                {
                    return elem.name == $scope.query.name && query.name != elem.name;
                }
            } );

        if (existName)
        {
            $scope.alerts.push({ type: 'danger', msg: 'Name of query ' + $scope.query.name + ' already exists.'});
            return false;
        }


        for (var i = 0; i < $scope.query.parameters.length; i++)
        {
            var param = $scope.query.parameters[i];

            if (!param.name || param.name == '')
            {
                $scope.alerts.push({ type: 'danger', msg: 'Please fill parameter names.'});
                return false;
            }
        }

        // $scope.alerts = [];
        // var valid = false;

        // for (var i =0; i<  GlobalVariablesService.CustomFieldNamesCount; i++)
        // {
        // 	if ($scope.item['custom_field_'+(i+1)] && $scope.item['custom_field_'+(i+1)].length > 0)
        // 	{
        // 		valid = true;
        // 		break;
        // 	}
        // }

        // if (!valid)
        // {
        // 	$scope.alerts.push({ type: 'danger', msg: 'Please fill at least 1 text field.'});
        // 	return false;
        // }


        $scope.validation_error = '';
        return true;
	}

    function getAppSettings()
    {
        var appSettingsPromise = SettingsService.get_app_settings();

        appSettingsPromise.then(function(result)
        {
            $scope.AppSettings = result;
        },
        function(err)
        {
        });
    }



    // calendar

    $scope.calendar_open = function($event, scope_item_name)
    {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope[scope_item_name] = true;
	};

    $scope.calendar_dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1
	};

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 2);
    $scope.calendar_events =
    [
    {
    	date: tomorrow,
    	status: 'full'
	},
    {
    	date: afterTomorrow,
    	status: 'partially'
	}];

    $scope.calendar_getDayClass = function(date, mode)
    {
    	if (mode === 'day')
    	{
    		var dayToCheck = new Date(date)
    		.setHours(0, 0, 0, 0);

    		for (var i = 0; i < $scope.events.length; i++)
    		{
    			var currentDay = new Date($scope.events[i].date)
    			.setHours(0, 0, 0, 0);

    			if (dayToCheck === currentDay)
    			{
    				return $scope.events[i].status;
				}
			}
		}

    	return '';
	};


    // time

    $scope.time_hstep = 1;
    $scope.time_mstep = 15;

    $scope.time_changed = function()
    {
    	console.log('Time changed to: ' + $scope.mytime);
	};

}
]);