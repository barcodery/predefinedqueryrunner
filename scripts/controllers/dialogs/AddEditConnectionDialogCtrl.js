'use strict';

angular.module('BarcoderyApp')
.controller('AddEditConnectionDialogCtrl', ['$scope',  '$location', '$modal', '$routeParams', 'SettingsService', '$modalInstance', 'DatabaseService', 'MySqlService', 'connection', 'new_connection_type',
function($scope,  $location,  $modal, $routeParams,  SettingsService, $modalInstance, DatabaseService, MySqlService, connection, new_connection_type)
{
    var old_connection = connection;

    $scope.alerts = [];
    $scope.connection_testing = false;
    $scope.connection_test_passed = false;

    $scope.is_editing = connection != null;

    if (connection == null)
    {
        $scope.connection = create_connection_object(new_connection_type);

        if (new_connection_type == 'mysql')
        {
            $scope.connection.name        = "name";
            $scope.connection.description = "desc";
            $scope.connection.host        = "127.0.0.1";
            $scope.connection.port        = "3306";
            $scope.connection.user        = "root";
            $scope.connection.password    = "";
            $scope.connection.port        = '3306';
        }
    }
    else
    {
        $scope.connection = connection;
    }

    $scope.cancelClicked = function()
    {
        $modalInstance.dismiss('cancel');
    }

    $scope.saveClicked = function()
    {
        var isValid = validate();

        if (!isValid)
        {
            return;
        }

        $scope.working = true;

        var promise = null;

        if (connection == null)
        {
            promise = SettingsService.AddConnection($scope.connection);
        }
        else
        {
            // query = $scope.query;
            promise = SettingsService.EditConnection(old_connection, $scope.connection);
        }

        promise.then(function(result)
        {
            $scope.working = false;
            $modalInstance.close(true);
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
    }

    $scope.testClicked = function()
    {
        if (!validate())
        {
            return;
        }

        if ($scope.connection.type == 'mysql')
        {
            test_mysql_connection()
        }

        // $scope.alerts.push({ type: 'success', msg: 'Connection ok'});
        // $scope.alerts.push({ type: 'warning', msg: 'Connection ok'});
        // $scope.alerts.push({ type: 'danger', msg: 'Connection ok'});
    }




    $scope.closeAlert = function(index)
    {
        $scope.alerts.splice(index, 1);
    };

    function validate()
    {
        // $scope.alerts = [];
        // $scope.validation_error = '';

        if ($scope.connection.name == '')
        {
            $scope.alerts.push({ type: 'danger', msg: 'Please fill connection name.'});
            return false;
        }

        var connections = SettingsService.GetConnectionsSync();
        var existName = connections.some(function(elem)
            {
                if (connection == null)
                {
                    return elem.name == $scope.connection.name ;
                }
                else
                {
                    return elem.name == $scope.connection.name && connection.name != elem.name;
                }
            } );

        if (existName)
        {
            $scope.alerts.push({ type: 'danger', msg: 'Name "' + $scope.connection.name + '"" already exists.'});
            return false;
        }

        return true;
    }

    function test_mysql_connection()
    {
        $scope.alerts = [];

        if ($scope.connection.host == null || $scope.connection.host == "")
        {
            $scope.alerts.push({ type: 'warning', msg: 'Connection failed'});
            return;
        }

        $scope.connection_testing = true;
        var promise = DatabaseService.CheckConnection($scope.connection);

        promise.then(function(result)
        {
            $scope.alerts.push({ type: 'info', msg: 'Connection ok'});
            $scope.connection_testing = false;
            $scope.connection_test_passed = true;
        },
        function(err)
        {
            $scope.alerts.push({ type: 'warning', msg: 'Connection failed'});
            $scope.connection_testing = false;
            $scope.connection_test_passed = false;
        });

    }

    function create_connection_object(connection_type)
    {
        var connection = {};
        connection.description = '';
        connection.host        = '';
        connection.user        = '';
        connection.password    = '';
        connection.port        = '';
        connection.type        = connection_type;
        connection.queries     = [];
        connection.status      = '';

        return connection;
    }
}
]);