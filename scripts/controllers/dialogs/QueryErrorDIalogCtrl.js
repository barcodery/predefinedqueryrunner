'use strict';

angular.module('BarcoderyApp')
.controller('QueryErrorDIalogCtrl', ['$scope',  '$location', '$modal', '$routeParams',  '$modalInstance',  'err', 'title', 'sql',
function($scope,  $location,  $modal, $routeParams,   $modalInstance, err, title, sql)
{
    $scope.title = title;
    $scope.err = JSON.stringify(err, null, 4);
    $scope.sql = sql;
}
]);