'use strict';

angular.module('BarcoderyApp')
.controller('DashBoardCtrl', ['$scope', '$location',   function ($scope, $location)
{
	$scope.redirect = function(path)
	{
		$location.path(path);
	}

}]);
