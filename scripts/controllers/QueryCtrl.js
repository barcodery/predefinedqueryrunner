'use strict';

angular.module('BarcoderyApp')
.controller('QueryCtrl', ['$scope', '$location', '$routeParams', 'SettingsService', 'DatabaseService', 'uiGridConstants', '$modal',
                function ($scope, $location, $routeParams, SettingsService, DatabaseService, uiGridConstants, $modal)
{
    var query_name = $routeParams.query_name;
    init_grid();

    $scope.datePicker = {};
    $scope.datePicker.date = {startDate: null, endDate: null};
    $scope.datePicker.options = {timePicker:true, locale: {format: 'MM/DD/YYYY h:mm A' }};
    $scope.datePicker.options.opens = "center"; // 'left'/'right'/'center'
    $scope.datePicker.options.singleDatePicker = false;
    $scope.datePicker.options.linkedCalendars = false;
    $scope.datePicker.options.alwaysShowCalendars = true;
    $scope.datePicker.options.autoApply = true;

    $scope.datePicker.options.ranges= {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }

    $scope.single_options = {};
    $scope.single_options.date = {startDate: null, endDate: null};
    $scope.single_options.options = {timePicker:true, locale: {format: 'MM/DD/YYYY h:mm A' }};
    $scope.single_options.options.opens = "center"; // 'left'/'right'/'center'
    $scope.single_options.options.singleDatePicker = true;
    $scope.single_options.options.linkedCalendars = false;
    $scope.single_options.options.alwaysShowCalendars = true;
    $scope.single_options.options.autoApply = true;

    loadQuery();

    function loadQuery()
    {
        $scope.working = true;

        var promise = SettingsService.GetQueryByName(query_name);

        promise.then(function(result)
        {
            $scope.query =  _.clone(result); //SettingsService.setting.queries;

            set_default_values();
            set_data_sources();

            $scope.working = false;
            GUI.Grid.SetGridHeight();
            GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
    }


    function set_default_values()
    {
        for (var i = 0; i < $scope.query.parameters.length; i++)
        {
            var param = $scope.query.parameters[i];

            if (param.default_value)
            {
                param.value = param.default_value;
            }
        }
    }

    function set_data_sources()
    {
        for (var i = 0; i < $scope.query.parameters.length; i++)
        {
            var param = $scope.query.parameters[i];

            if (param.type == 'Combobox' && param.data_source)
            {
                param.data_source_array = eval(param.data_source);
            }
        }
    }

	$scope.redirect = function(path)
	{
		$location.path(path);
	}

    $scope.runQuery = function()
    {
        var sql = createSqlQuery(); //$scope.query.sql;

        var promise = DatabaseService.RunQuery(sql);

        promise.then(function(result)
        {
            setGridFields(result[0].fields);
            $scope.gridOptions.data = result[0].rows;
        },
        function(err)
        {
            showErrorDialog(err, sql, "Error running query");
            console.log(err);
        });
    }

    function showErrorDialog(err, sql, title)
    {
        var modalInstance = $modal.open({
            controller: 'QueryErrorDIalogCtrl',
            templateUrl: 'views/dialogs/QueryErrorDIalog.html',
            size: '50-percent',
            resolve:
            {
                err: function ()
                {
                    return err;
                },
                title: function ()
                {
                    return title;
                },
                sql: function ()
                {
                    return sql;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {

        });
    }

    function init_grid()
    {
        $scope.gridOptions =
        {

            showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: true,

            exporterMenuPdf: false,

            exporterCsvFilename: 'data.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
            },

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    enableDisableDeleteButton();
                    enableDisableEditButton();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    enableDisableDeleteButton();
                    enableDisableEditButton();
                });
            }
        };
    }

    function setGridFields(fields)
    {
        $scope.gridOptions.columnDefs = [];

        for (var i = 0; i < fields.length; i++)
        {
            var field = fields[i];
            var col_def =
            {
                field: field.name
                ,displayName:field.name
                , enableHiding:true
                // , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
                , aggregationType: uiGridConstants.aggregationTypes.count

                // ,filters:
                // [
                //     {
                //         condition: uiGridConstants.filter.CONTAINS,
                //         placeholder: "contains"
                //     }
                //     ,
                //     {
                //         type: uiGridConstants.filter.SELECT,
                //         selectOptions: [  { value: '1', label: "empty" } ],
                //         condition: function(term, value, row, column)
                //         {
                //             // true ak sa nema filtrovat
                //             if (term == 1 && value != null) return false;
                //             return true;
                //         },
                //     },
                // ]
            }

            $scope.gridOptions.columnDefs.push(col_def);
        }
    }

    function createSqlQuery()
    {
        var sql = $scope.query.sql;
        for (var i = 0; i < $scope.query.parameters.length; i++)
        {
            var param = $scope.query.parameters[i];

            if (param.type == "Text")
            {
                sql = sql.replace(param.id, param.value);
            }

            if (param.type == "Combobox")
            {
                sql = sql.replace(param.id, param.value.value);
            }

            if (param.type == "DateTime" && param.interval.type =='start')
            {
                sql = sql.replace(param.id, Helpers.Date.DateToMysqlDate(param.interval.date.startDate._d));

                // find interval end
                for (var j = 0; j < $scope.query.parameters.length; j++)
                {
                    var param2 = $scope.query.parameters[j];
                    if (param2.type == "DateTime" && param2.interval.type =='end' && param2.interval.start_id == param.id )
                    {
                        sql = sql.replace(param2.id, Helpers.Date.DateToMysqlDate(param.interval.date.endDate._d));
                    }
                }
            }

            if (param.type == "DateTime" && param.interval.type =='no')
            {
                sql = sql.replace(param.id, Helpers.Date.DateToMysqlDate(param.single_date.date._d));
            }

        }

        return sql;
    }



    // export function

    $scope.exportToXLSX = function()
    {
        $scope.filteredRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);

        var artistWorkbook = ExcelBuilder.createWorkbook();
        var albumWorksheet = artistWorkbook.createWorksheet({ name: "Items Activity Report"  });

        var stylesheet = artistWorkbook.getStyleSheet();

        var boldFormatter = stylesheet.createFormat({
            font: {
                bold: true,
                size: 11

            }
        });

        var headerFormatter = stylesheet.createFormat({
            font: {
                bold: true,
                size: 18,
                // color:'FF0A60A6',
            }
        });


        var data_to_save = [];

        // var formatted_date_from = $scope.date_from.format($scope.account.date_format);
        // var formatted_date_to = $scope.date_to.format($scope.account.date_format);

        // data_to_save.push([ {value:"Barcodery - Items Activity Report" , metadata: {style: headerFormatter.id}}]);
        // data_to_save.push([]);

        // // data input params
        // data_to_save.push([{value: "Date From"  , metadata: {}}, {value: formatted_date_from, metadata: {style: boldFormatter.id}} ]);
        // data_to_save.push([{value: "Date To"    , metadata: {}}, {value: formatted_date_to, metadata: {style: boldFormatter.id}} ]);

        for (var i = 0; i < $scope.gridApi.grid.columns.length; i++)
        {
            var col =$scope.gridApi.grid.columns[i];
            if (col.filters[0].term)
            {
                data_to_save.push([{value: col.displayName, metadata: {}}, {value: col.filters[0].term, metadata: {style: boldFormatter.id}} ]);
            }
        }

        // data header cells
        data_to_save.push([]);

        var header_rows_count = data_to_save.length;
        var header_display_names = [];
        var header_field_names = [];
        for (var i = 0; i < $scope.gridApi.grid.columns.length; i++)
        {
            var col =$scope.gridApi.grid.columns[i];
            if (col.visible)
            {
                header_display_names.push({value: col.displayName, metadata: {style: boldFormatter.id}});
                header_field_names.push(col.field);
            }
        }
        data_to_save.push(header_display_names);


        var grid_data = get_grid_visible_data_ready_to_export();
        for (var i = 0; i < grid_data.length; i++)
        {
            var grid_row = grid_data[i];
            var row_to_insert = [];
            for (var j = 0; j < header_field_names.length; j++)
            {
                var hd_field_name = header_field_names[j];
                row_to_insert.push(grid_row[hd_field_name]);
            }

            data_to_save.push(row_to_insert);
        }

        // var data_table_filtering = new Table();
        // data_table_filtering.styleInfo.themeStyle = "TableStyleLight2";
        // data_table_filtering.setReferenceRange([1, header_rows_count], [header_field_names.length, header_rows_count + grid_data.length]);
        // data_table_filtering.setTableColumns(_.map(header_display_names, function(row){ return row.displayName }));
        // albumWorksheet.addTable(data_table_filtering);
        // artistWorkbook.addTable(data_table_filtering);

        albumWorksheet.setData(data_to_save);


        var width_array = [];
        _.each(header_field_names, function(q) {width_array.push({width: 15}); });

        albumWorksheet.setColumns(width_array);

        albumWorksheet.setFooter([
        "Date of print: &D &T",
        "&A",
        "Page &P of &N"
        ]);

        artistWorkbook.addWorksheet(albumWorksheet);
        var documentData = ExcelBuilder.createFile(artistWorkbook);

        window.location = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+documentData;
    }

    $scope.exportToCSV = function()
    {
        var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
        $scope.gridApi.exporter.csvExport( 'visible', 'all' , myElement);
    }

    $scope.exportToPDF = function()
    {
        var doc = new jsPDF({unit: 'pt', lineHeight: 1.5, orientation: 'p'});

        doc.setFontSize(18);
        doc.text("Barcodery - Items Activity Report" , 40, 60);
        doc.setFontSize(11);
        doc.setTextColor(100);

        // create audit table

        // doc.setFontType("bold");
        // doc.text('Audit Name  :' , 40, 100);
        // doc.text('Description :' , 40, 120);
        // doc.text('Created     :' , 40, 120);
        // doc.text('Closed      :' , 40, 120);

        // var cloned_audit = _.clone($scope.audit);
        // cloned_audit.loc_created = $scope.format_date_time($scope.audit.created_date);
        // cloned_audit.loc_closed  = $scope.format_date_time($scope.audit.closed_date);

        var header_column_defs =
        [
            {title: "", dataKey: "header"},
            {title: "", dataKey: "value"},
        ];

        var header_data =
        [
            {  header: "Date From" ,value: $scope.date_from.format($scope.account.date_format) }
            ,{ header: "Date To"  ,value: $scope.date_to.format($scope.account.date_format) }
        ];

        var body_column_def = [];

        for (var i = 0; i < $scope.gridApi.grid.columns.length; i++)
        {
            var col =$scope.gridApi.grid.columns[i];
            if (col.filters[0].term)
            {
                header_data.push ({header: col.displayName, value: col.filters[0].term});
            }

            if (col.visible)
            {
                body_column_def.push({title: col.displayName, dataKey: col.field});
            }
        }

        doc.autoTable(header_column_defs, header_data,
        {
            startY: 90
            ,drawHeaderRow: function()
            {
                // Draw header row
                return false;
            }
            ,columnStyles:
            {
                header: {fillColor: [41, 128, 185], textColor: 255, fontStyle: 'bold', columnWidth: 150}
            }

        });


        // create audit items table
        // doc.setFontSize(15);
        // doc.text('Audit items', 40, doc.autoTableEndPosY() + 40);
        // doc.setFontSize(11);

        // var text = doc.splitTextToSize('shuffleSentence(faker.lorem.words(55))' + '.', doc.internal.pageSize.width - 80, {});
        // doc.text(text, 40, 80);

        var yPosition = doc.autoTableEndPosY() + 20;

        // yPosition = 250;

        var grid_data = get_grid_visible_data_ready_to_export();

        doc.autoTable(body_column_def, grid_data ,
        {
            startY: yPosition
          ,  createdCell: function (cell, data)
            {
                // if (data.column.dataKey === 'missing_count' || data.column.dataKey === 'extra_count' )
                // {
                //     cell.styles.halign = 'right';
                //     if (cell.raw != 0 || cell.raw != '0')
                //     {
                //         cell.styles.textColor = [255, 100, 100];
                //         cell.styles.fontStyle = 'bold';
                //     }
                // }
            }
        });

        var now = new Date();
        var generated_date_text = 'Generated: ' + now.toLocaleDateString()  + ' ' + now.toLocaleTimeString();
        doc.text(generated_date_text, 40, doc.internal.pageSize.height - 30);

        // set page numbers
        for (var i = 0; i < doc.internal.pages.length - 1; i++)
        {
            doc.setPage(i);
            doc.text(doc.internal.pageSize.width - 70, doc.internal.pageSize.height - 30 , 'Page ' + (i+1) );
        };

        doc.save('items_activity_report' +'.pdf' , 'right' );
    }

    function get_grid_visible_data_ready_to_export()
    {
        var gridVisibleData = $scope.filteredRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
        var gridDataToShow = _.map(gridVisibleData, function(row){ return _.clone(row.entity) });


        _.each(gridDataToShow, function(item)
        {
            // item.type = $scope.getTranslatedType(item.type);
            // item.timestamp = $scope.get_formatted_date(item.timestamp);
        });

        return gridDataToShow;
    }

}]);

