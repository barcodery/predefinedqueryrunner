'use strict';

angular.module('BarcoderyApp')
.controller('ConnectionsCtrl', ['$scope', '$location',  'SettingsService', '$modal',  'uiGridConstants', 'gettextCatalog', '$timeout', 'DatabaseService',
                   function ($scope, $location,SettingsService,$modal, uiGridConstants, gettextCatalog, $timeout, DatabaseService)
{
    $scope.isDeleteButtonDisabled = true;
    $scope.isEditButtonEnabled = false;
    $scope.deleteButtonTooltip = '';
    $scope.alerts = [];

    initGrid();
    loadData();

    $scope.DatabaseService = DatabaseService;


    $scope.redirect = function(path, ev)
    {
        if (ev) ev.preventDefault();
        $location.path(path);
    }

    $scope.addMysql_clicked = function()
    {
        $scope.showAddNewConnectionDialog('mysql');
    }

    $scope.addMssql_clicked = function()
    {
        $scope.showAddNewConnectionDialog('mssql');
    }

    $scope.addPostgresql_clicked = function()
    {
        $scope.showAddNewConnectionDialog('postgresql');
    }

    $scope.showAddNewConnectionDialog = function(new_connection_type)
    {
        var connection = null;

        var modalInstance = $modal.open({
            controller: 'AddEditConnectionDialogCtrl',
            templateUrl: 'views/dialogs/AddEditConnectionDialog.html',
            size: '70-percent',
            resolve:
            {
                connection: function ()
                {
                    return connection;
                },
                new_connection_type: function ()
                {
                    return new_connection_type;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                loadData();
            }
        });
    }

    $scope.editButtonClicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var connection = rows[0].entity;
        var connection_cloned = _.clone(connection);

        var modalInstance = $modal.open({
            controller: 'AddEditConnectionDialogCtrl',
            templateUrl: 'views/dialogs/AddEditConnectionDialog.html',
            size: '70-percent',
            resolve:
            {
                connection: function ()
                {
                    return connection;
                },
                new_connection_type: function ()
                {
                    return null //'mysql';
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                loadData();
            }
            else
            {
                rows[0].entity = connection_cloned;
            }
        });
    }

    $scope.deleteButtonClicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var ids = [];

        for(var i in rows)
        {
            var name = rows[i].entity.name;
            SettingsService.DeleteConnectionByName(name);
            // ids.push(rows[i].entity.category_id);
            // if (rows[i].entity.count > 0)
            // {
            //     $scope.alerts.push({ type: 'danger', msg: gettextCatalog.getString("Categories", null, "Only empty categories can be deleted.") });
            //     return;
            // }
        }

        loadData();

        // $scope.working = true;
        // ApiService.CategoriesAPI.delete({ids:JSON.stringify(ids)}).$promise
        // .then(function(result)
        // {

        //     CacheService.SetCategoriesDirty();
        //     loadData();
        //     $scope.isDeleteButtonDisabled = true;
        // },
        // function(err)
        // {
        //     $scope.working = false;
        // });
    }

    $scope.closeAlert = function(index)
    {
        $scope.alerts.splice(index, 1);
    };

    $scope.itemClicked = function(row)
    {
        var connection = row.entity;

        SettingsService.SetActiveConnection(connection);
          $location.path('/queries');
    }

    $scope.refreshClicked = function()
    {
        loadData();
    }

    function loadData()
    {
        $scope.working = true;

        var promise = SettingsService.GetConnectionsAsync();

        promise.then(function(result)
        {
            $scope.gridOptions.data =  result; //SettingsService.setting.queries;

            refreshGrid();

            $scope.working = false;
            GUI.Grid.SetGridHeight();
            GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
    }

    function initGrid()
    {
        $scope.gridOptions = {

            showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: true,

            exporterMenuPdf: false,

            exporterCsvFilename: 'categories.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
            },

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    enableDisableDeleteButton();
                    enableDisableEditButton();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    enableDisableDeleteButton();
                    enableDisableEditButton();
                });
            }
        };

        $scope.gridOptions.columnDefs =
        [
        {
            field: 'name'
            ,displayName:'Name'
            , enableHiding:true
            // , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'
            , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
        }
        ,
        {
            field: 'description'
            ,displayName:'Description'
            , enableHiding:true
            , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
        }
        ,{
            field: 'type'
            ,displayName:gettextCatalog.getString("Type", null, "Context Grid Columns")
            , enableHiding:true
            ,sortingAlgorithm: Helpers.Sorting.Numeric
            , aggregationType: uiGridConstants.aggregationTypes.sum
        }
        ,{
            field: 'color'
            ,displayName:gettextCatalog.getString("Color", null, "Context Grid Columns")
            , enableHiding:true
            , cellTemplate: ' <div style=" margin-left:5px; margin:2px; width:100%; max-width:25px; height:100%; background:{{COL_FIELD}}"></div> '
        }

        ];
    }

    function enableDisableDeleteButton()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();

        if (rows.length >0)
        {
            $scope.isDeleteButtonDisabled = false;
        }
        else
        {
            $scope.isDeleteButtonDisabled = true;
        }
    }

    function enableDisableEditButton()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();

        if (rows.length == 1)
        {
            $scope.isEditButtonEnabled = true;
        }
        else
        {
            $scope.isEditButtonEnabled = false;
        }
    }

    function refreshGrid()
    {
        $scope.refresh_grid = true;
        $timeout(function() {
          $scope.refresh_grid = false;
        }, 0);
    };

}]);
