'use strict';

angular.module('BarcoderyApp')
.controller('ConfirmDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'content',
							function ($scope, $location,  ApiService, $modalInstance, content)
{
	$scope.content = content;
		
	$scope.noClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}
	
	$scope.yesClicked = function()
	{	
		$modalInstance.close(true);
	};
}]);
