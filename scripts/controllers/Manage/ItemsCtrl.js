// 'use strict';

angular.module('BarcoderyApp')
.controller('ItemsCtrl', ['$scope', '$location', 'ApiService', 'HelperService', 'CacheService', 'uiGridConstants', 'GlobalVariablesService','$timeout', 'UserService','$modal', 'uiGridConstants', "SettingsService", "$sce", "gettextCatalog", "Upload",
function ($scope, $location, ApiService, HelperService, CacheService, uiGridConstants, GlobalVariablesService, $timeout, UserService, $modal, uiGridConstants, SettingsService, $sce, gettextCatalog, Upload)
{
	init();
	$scope.alerts = [];
	$scope.HelperService = HelperService;
	$scope.base_url = $location.absUrl().replace($location.path(), "");

	var setting_force_images_first = false;

	$scope.redirect = function(path, ev)
	{
		// ev.preventDefault();
		$location.path(path);
	}

	$scope.hideAllColumns = function()
	{
		for (var i in $scope.gridOptions.columnDefs)
		{
			$scope.gridOptions.columnDefs[i].visible = false;
		}

		$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN );
	}

	$scope.showAllColumns = function()
	{
		for (var i in $scope.gridOptions.columnDefs)
		{
			$scope.gridOptions.columnDefs[i].visible = true;
		}

		$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN );
	}

	$scope.export = function()
	{
		$scope.gridApi.exporter.pdfExport('all','all' );
	}

	$scope.itemClicked = function(row)
	{
		// var item_id = row.entity.item_id;
		// $scope.redirect('/item_detail/' + item_id);

		var url = $scope.getItemUrl(row);
		$scope.redirect(url);
	}

	$scope.getItemUrl = function(row)
	{
		var item_id = row.entity.item_id;
		var url = '/item_detail/' + item_id;
		return url;
	}

	$scope.saveGridState = function()
	{
		$scope.state = $scope.gridApi.saveState.save();

		var state = $scope.gridApi.saveState.save();
		var objToSave =
		{
			grid_state: state
			,account_id:$scope.user.account_id
		};
		var json = JSON.stringify(objToSave);

		var obj_to_send = {grid_state:json, grid_row_height:$scope.gridOptions.rowHeight};

		var promise = ApiService.UserSettingsAPI.saveGirdState(obj_to_send).$promise;
		promise.then(function(result)
		{
		},
		function(err)
		{
		});

		CacheService.SetUserSettingsDirty();
	}

	$scope.resetGridState = function()
	{
		$scope.state ="";

		var state = "";
		var objToSave =
		{
			grid_state: state
			,account_id:$scope.user.account_id
		};
		var json = JSON.stringify(objToSave);

		var promise = ApiService.UserSettingsAPI.saveGirdState({grid_state:json, grid_row_height:"null"}).$promise;
		promise.then(function(result)
		{
		},
		function(err)
		{
		});

		//tryLoadGridState();

		$scope.gridApi.saveState.restore( $scope, "");
		$scope.refreshClicked();
		CacheService.SetUserSettingsDirty();
	}

	$scope.restoreState = function()
	{
		tryLoadGridState();
	}

	$scope.deleteButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var content = {title:'Delete', text:'Do you really want to delete selected items in all locations, audit items and requests associated with this item ?'};

		var modalInstance = $modal.open({
			controller: 'ConfirmDialogCtrl',
			templateUrl: 'views/ConfirmDialog.html',
			size: 'sm',
			resolve: {
				content: function ()
				{
					return content;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{

				var rows = $scope.gridApi.selection.getSelectedGridRows();
				var ids = [];

				for(var i in rows)
				{
					ids.push(rows[i].entity.item_id);
				}

				ApiService.ItemsAPI.delete({ids:JSON.stringify(ids)}).$promise
				.then(function(result)
				{
					CacheService.SetItemsDirty();
					loadData();
				},
				function(err)
				{
				});
			}
		});
	}

	$scope.addNewItemClicked = function()
	{
		// $scope.redirect('/new_item');
		var modalInstance = $modal.open({
			controller: 'AddEditItemDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditItemDialog.html',
			size: '90-percent',
			resolve: {
			selected_items: function ()
			{
				return null;
			}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				loadData();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var selected_items = _.map(rows, function(row){ return row.entity; });

		var modalInstance = $modal.open({
			controller: 'AddEditItemDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditItemDialog.html',
			size: '90-percent',
			resolve: {
			selected_items: function ()
			{
				return selected_items;
			}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				loadData();
			}
		});
	}

	$scope.moveButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var items = _.map(rows, function(row) {return row.entity; });

		var modalInstance = $modal.open({
				controller: 'AddTransferDialogCtrl',
				templateUrl: 'views/Manage/dialogs/AddTransferDialog.html',
				size: 'lg',
				resolve: {
					item_id: function ()
					{
						return null;
					}
					,items: function ()
					{
						return items;
					}
				}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer) loadData();
			// loadData();

			// if (answer)
			// {
				// CacheService.SetCategoriesDirty();
				// GlobalVariablesService.Scope_ItemTab_Details.loadData();
				// GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				// GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
				// loadData();
			// }
		}, function ()
		{
  			// loadData();
		});
	}

	$scope.addCheckInClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var items = _.map(rows, function(row) {return row.entity; });

		var modalInstance = $modal.open({
				controller: 'AddCheckInDialogCtrl',
				templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
				size: 'lg',
				resolve: {
					item_id: function ()
					{
						return null;
					}
					,items: function ()
					{
						return items;
					}
				}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer) loadData();
			// loadData();

			// if (answer)
			// {
				// CacheService.SetCategoriesDirty();
				// GlobalVariablesService.Scope_ItemTab_Details.loadData();
				// GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				// GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
				// loadData();
			// }
		}, function ()
		{
  			// loadData();
		});
	}

	$scope.addCheckOutClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var items = _.map(rows, function(row) {return row.entity; });

		var modalInstance = $modal.open({
				controller: 'AddCheckOutDialogCtrl',
				templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
				size: 'lg',
				resolve: {
					item_id: function ()
					{
						return null;
					}
					,items: function ()
					{
						return items;
					}
				}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer) loadData();

			// if (answer)
			// {
				// CacheService.SetCategoriesDirty();
				// GlobalVariablesService.Scope_ItemTab_Details.loadData();
				// GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				// GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
				// loadData();
			// }
		}, function ()
		{
  			// loadData();
		});
	}

	$scope.exportToCsv = function(rowType)
	{
		var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
		$scope.gridApi.exporter.csvExport( rowType, 'all' , myElement);
	}

	$scope.refreshClicked = function()
	{
		loadData();
	}

	$scope.requestButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddRequestItemDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddRequestItemDialog.html',
			size: 'lg',
			resolve:
			{
				items_id: function ()
				{
					var rows = $scope.gridApi.selection.getSelectedGridRows();
					var ids = [];

					for(var i in rows)
					{
						ids.push(rows[i].entity.item_id);
					}

					return ids;
				},
				request_id: function()
				{
					return null;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				// CacheService.SetCategoriesDirty();
				// GlobalVariablesService.Scope_ItemTab_Details.loadData();
				// GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				// $scope.loadData();
			}
		});
	}

	$scope.generateQrButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'GenerateUniqueValuesDialogCtrl',
			templateUrl: 'views/Manage/dialogs/GenerateUniqueValuesDialog.html',
			size: 'lg',
			resolve:
			{
				items_id: function ()
				{
					var rows = $scope.gridApi.selection.getSelectedGridRows();
					var ids = [];

					for(var i in rows)
					{
						ids.push(rows[i].entity.item_id);
					}

					return ids;
				},
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				loadData();
			}
		});
	}

	$scope.generate_img_tag_from_urls = function(urls)
	{
		if (urls)
		{
			var splitted_urls = urls.split(",");

			if (splitted_urls.length > 0)
			{
				// var tag = "<img src='"+ splitted_urls[0]+"' width='100%'  height='100%'> </img>";

				// tag = "<div style=' width: 4rem; height: 4rem; background-image: url(http://localhost/scott/"+ splitted_urls[0]+")'></div>";
				// tag = "<div style='background-image: url(http://localhost/scott/server/index.php/dimg_handler/get/5)'></div>";

				// tag = $sce.trustAsHtml("<div style=' background-size: cover; background-position:50% 50%;width: 8rem; height: 8rem; background-image: url(http://localhost/scott/"+ splitted_urls[0]+")'></div>");

				// console.log(tag);

				var tag = "<img src='"+ splitted_urls[0]+"'   height='100%'> </img>";
				return tag;
			}
		}

		return "";
	}


	$scope.generate_states_colors = function(row)
	{
		var result = "";

		if (row.entity.state_colors)
		{
			var splitted_colors = row.entity.state_colors.split(",");

			for (var i = 0; i < splitted_colors.length; i++)
			{
				var color = splitted_colors[i];
				// var span =  '<div style=" margin-left:5px; margin:2px; width:100%; max-width:25px; height:100%; background:' + color +'"></div>';
				var span =  '<td> <div style=" margin-left:5px; margin:2px; width:25px; max-width:25px; height:25px; background:' + color +'"> </div></td>';
				result += " "  + span;
			};

			// result += "<span >" + row.entity.states +"</span>";

			result = "<table><tr>" + result +"<td>"+ row.entity.states +"</td></tr></table>";
			var result = $sce.trustAsHtml(result);
			return result;
		}

		return result;

	}

	$scope.gps_row_clicked = function(latlng)
	{
		var url = 'http://www.google.com/maps';

		latlng = latlng.replace(/\s/g,'');  	//remove spaces
		var semicolonIndex = latlng.indexOf(";");
		if (semicolonIndex > -1 )
		{
			var latitude  = latlng.substring(0, semicolonIndex);
			var longitide = latlng.substring(semicolonIndex + 1, latlng.length);

			url = 'http://www.google.com/maps/place/'+latitude+','+longitide+'/@'+latitude+','+longitide+',12z';
		}

		window.open(url, "_blank");
		// return url;
	}

	$scope.itemImageClicked = function(urls)
	{
		if (urls)
		{
			var splitted_urls = urls.split(",");

			if (splitted_urls.length > 0)
			{
				var modalInstance = $modal.open(
				{
					controller: 'ImagesGalleryDialogCtrl',
					templateUrl: 'views/Manage/dialogs/ImagesGalleryDialog.html',
					size: '75-percent',
					resolve:
					{
						imagesUrls: function ()
						{
							return splitted_urls;
						}
						, selectedIndex: function()
						{
							return 0;
						}
					}
				});

					// modalInstance.result.then(function (answer)
					// {
					// 	if (answer)
					// 	{
					// 		$scope.loadData();
					// 	}
					// });
			}
		}
	}

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.optionButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'GridOptionsDialogCtrl',
			templateUrl: 'views/Manage/dialogs/GridOptionsDialog.html',
			size: 'lg',
			resolve:
			{
				items_view_scope:function () { return $scope; }
			}
		});

		modalInstance.result.then(function (answer)
		{
			$scope.refreshClicked();

			// if (answer)
			// {
			// 	$scope.refreshClicked();
			// }
			// else
			// {
			// 	$scope.gridOptions.rowHeight = original_row_height;
			// }
		});
	}

	$scope.showSaveBarcodesPDFDialog = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var items = _.map(rows, function(row) {return row.entity; });

		var modalInstance = $modal.open({
			controller: 'SaveBarcodesPdfDialogCtrl',
			templateUrl: 'views/Manage/dialogs/SaveBarcodesPdfDialog.html',
			size: 'lg',
			resolve: {
				item: function ()
				{
					return null;
				},

				items: function ()
				{
					return items;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
			}
		});
	}

	$scope.createMultipleItemsClicked = function()
	{

		var modalInstance = $modal.open({
				controller: 'CreateMultipleItemsDialogCtrl',
				templateUrl: 'views/Manage/dialogs/CreateMultipleItemsDialog.html',
				size: 'sm',
				resolve: {
				}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer) loadData();
		}, function ()
		{
		});
	}


	function saveEditedItem(rowEntity, colDef, newValue, oldValue)
	{
		// console.log('edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue );
		if (newValue === oldValue) return;

		$scope.show_loading_data_in_grid = true;
		var url = ApiService.get_api_base_url() + "/add_edit_item_fd";
		var item_to_save = {};

		// {"categories_ids":"[]","tags_ids":"[]","item_ids":"[\"22616\"]","values":"{\"27822\":\"sdsad\",\"27823\":\"ddd\",\"27824\":\"\",\"27825\":\"0\",\"27826\":0,\"27827\":\"1899-11-30T00:00:00.000Z\",\"27828\":\"\",\"27829\":\"\"}"}""}
		// {"item_ids":"[\"22618\"]","values":"{\"27822\":\"eeeeeee\"}"}""

		var values = {};
		var custom_field_id = colDef.field.replace("custom_field_id_", "");
		values[custom_field_id] = newValue;

		item_to_save.item_ids = JSON.stringify([rowEntity.item_id]);
		item_to_save.values = JSON.stringify(values)
		if (rowEntity.categories_ids != null && rowEntity.categories_ids.length > 0)
		{
			item_to_save.categories_ids = JSON.stringify(rowEntity.categories_ids.split(", "));
		}

		if (rowEntity.tags_ids != null && rowEntity.tags_ids.length > 0)
		{
			item_to_save.tags_ids = JSON.stringify(rowEntity.tags_ids.split(", "));
		}

		if (rowEntity.states_ids != null && rowEntity.states_ids.length > 0)
		{
			item_to_save.states_ids = JSON.stringify(rowEntity.states_ids.split(", "));
		}

        Upload.upload(
        {
            url: url,
            data: { 'item_data': JSON.stringify(item_to_save)}
        })
        .then(function (resp)
        {
            CacheService.SetItemsDirty();

			for (var i = 0; i < $scope.gridOptions.data.length; i++)
			{
				if ($scope.gridOptions.data[i].item_id == rowEntity.item_id)
				{
					$scope.gridOptions.data[i] = resp.data[0];
					break;
				}
			};

            $scope.show_loading_data_in_grid = false;
        },
        function (err)
        {
            $scope.alerts.push(
            {
                type: 'danger',
                msg: err.data.error_message
            });

			rowEntity[colDef.field] = oldValue;
			//  		var promise = CacheService.GetItems();
			// promise.then(function(result)
			// {
			// 	$scope.gridOptions.data = result;
			// 	$scope.show_loading_data_in_grid = false;
			// },
			// function(err)
			// {
			// 	$scope.show_loading_data_in_grid = false;
			// });

            $scope.show_loading_data_in_grid = false;
        },
        function (evt)
        {
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

		// $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue ;
		$scope.$apply();
	}

	function init()
	{
		loadUser();
		initGrid();
		loadData();
	}

	function loadData()
	{
		$scope.alerts = [];
		$scope.loadingData = true;
		var promise = CacheService.GetItems();

		var userSettingsPromise = CacheService.GetUserSettings();

		var appSettingsPromise = getAppSettings();

		userSettingsPromise.then(function(result)
		{
			$scope.user_settings = result;
			return promise;
		},
		function(err)
		{
			$scope.loadingData = false;
		})

		.then(function(result)
		{
			//$scope.noItems = result.length == 0;
			$scope.items = result;

			var customFieldNamesPromise = CacheService.GetCustomFieldNames();
			loadCustomFieldNames(customFieldNamesPromise);
			return customFieldNamesPromise;
		},
		function(err)
		{
			$scope.loadingData = false;
		})

		.then(function(result)
		{
			var account_promise = ApiService.MyAccountAPI.getAccounts().$promise;

			account_promise.then(function(result)
			{
				$scope.account = result[0];
				HelperService.date_format = $scope.account.date_format;
			},
			function(err)
			{

			});

			return account_promise;

		},
		function(err)
		{
			$scope.loadingData = false;
		})
		.then(function(result)
		{
			return appSettingsPromise;
		},
		function(err)
		{
			$scope.loadingData = false;
		})
		.then(function(result)
		{
			// vsetko loadnute
			loadDataIntoGrid($scope.items);
		},
		function(err)
		{
			$scope.loadingData = false;
		})
		;
	}

	function loadCustomFieldNames(promise)
	{
		//ApiService.CustomFieldNamesAPI.loadCustomFieldNames().$promise
		promise.then(function(result)
		{
			$scope.custom_filed_names = result;
		},
		function(err)
		{
			$scope.custom_filed_names = [];
		});
	}

	function get_custom_fields_by_type(type)
	{
		var result = [];

		if ($scope.custom_filed_names)
		{
			for (var i = 0; i < $scope.custom_filed_names.length; i++)
			{
				if ($scope.custom_filed_names[i].type == type)
				{
					result.push($scope.custom_filed_names[i]);
				}
			}
		}

		return result;
	}

	function initGrid()
	{
		$scope.gridOptions = {

			// rowHeight:130,
			showColumnFooter: true,
			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			saveWidths : true,
			saveOrder : true,
			saveScroll : false,
			saveFocus : true,
			saveVisible : true,
			saveSort : true,
			saveFilter : false,
			savePinning : true,
			saveGrouping : true,
			saveGroupingExpandedStates : true,
			saveTreeView : true,
			saveSelection : false,



			enableCellEdit:false, //disable inline cell editing on all grid

			exporterMenuPdf: false,

			exporterCsvFilename: 'items.csv',
			exporterPdfDefaultStyle: {fontSize: 9},
			exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
			exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
			exporterPdfHeader: { text: "Items", style: 'headerStyle' },
			exporterPdfFooter: function ( currentPage, pageCount ) {
				return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			},
			exporterPdfCustomFormatter: function ( docDefinition ) {
				docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
				docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
				return docDefinition;
			},
			exporterPdfOrientation: 'portrait',
			exporterPdfPageSize: 'LETTER',
			exporterPdfMaxGridWidth: 500,
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

			exporterOlderExcelCompatibility:true,

			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;
				GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
				});

				// gridApi.colMovable.on.columnPositionChanged($scope,function(colDef, originalPosition, newPosition)
				// {
				// alert('column moved');
				// });

				// gridApi.colResizable.on.columnSizeChanged($scope,function(colDef, deltaChange)
				// {
				// alert('size changed');
				// });

				gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue)
				{
					saveEditedItem(rowEntity, colDef, newValue, oldValue);
				});

				enableDisableDeleteButton();
			}
		};
	}

	function saveState()
	{
		$scope.saveGridState();
	}

	function restoreState()
	{
		$timeout(function()
		{
			$scope.restoreState();
		});
	}


	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}

		$scope.isEditButtonDisabled = rows.length == 0;
	}

	function loadDataIntoGrid(items)
	{
		$scope.gridOptions.data = items;

		$scope.gridOptions.columnDefs = [];


		$scope.gridOptions.rowHeight  = 30;

		setGridCustomColumns();

		if($scope.user_settings.grid_row_height != null)
		{
			$scope.gridOptions.rowHeight = parseInt($scope.user_settings.grid_row_height);
		}

		$scope.gridOptions.columnDefs.push(
		{
			field: 'locations'
			,displayName:gettextCatalog.getString("Locations", null, "Context Grid Columns")
			,enableHiding:true
			// ,filter: {
			// 	condition: uiGridConstants.filter.CONTAINS
			// 	,placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
			// }
			,filters:
			[
				{
					condition: uiGridConstants.filter.CONTAINS,
					placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
				}
				,
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value != null) return false;
						return true;
					},
				},
			]
		});

		$scope.gridOptions.columnDefs.push(
		{
			field: 'categories'
			,displayName:gettextCatalog.getString("Categories", null, "Context Grid Columns")
			,enableHiding:true
			// ,filter: {
			// 	condition: uiGridConstants.filter.CONTAINS
			// 	,placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
			// }
			,filters:
			[
				{
					condition: uiGridConstants.filter.CONTAINS,
					placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
				}
				,
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value != null) return false;
						return true;
					},
				},
			]

		});

		$scope.gridOptions.columnDefs.push(
		{
			field: 'tags'
			,displayName:gettextCatalog.getString("Tags", null, "Context Grid Columns")
			,enableHiding:true
			// ,filter: {
			// 	condition: uiGridConstants.filter.CONTAINS,
			// 	placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
			// }
			,filters:
			[
				{
					condition: uiGridConstants.filter.CONTAINS,
					placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
				}
				,
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value != null) return false;
						return true;
					},
				},
			]
		});

		$scope.gridOptions.columnDefs.push(
		{
			field: 'states'
			,displayName:gettextCatalog.getString("States", null, "Context Grid Columns")
			,enableHiding:true
			, cellTemplate: ' <div ng-click="grid.appScope.itemClicked(row)" style="position:relative;cursor:pointer; margin:3px; height:150px;"  ng-bind-html="grid.appScope.generate_states_colors(row)"></div> '
			,filters:
			[
				{
					condition: uiGridConstants.filter.CONTAINS,
					placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
				}
				,
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value != null) return false;
						return true;
					},
				},
			]
		});

		$scope.gridOptions.columnDefs.push(
		{
			field: 'count'
			,displayName:gettextCatalog.getString("Count", null, "Context Grid Columns")
			,enableHiding:true
			,sortingAlgorithm: Helpers.Sorting.Numeric

			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-href="{{grid.appScope.base_url}}/item_detail/{{row.entity.item_id}}">{{COL_FIELD}} </a></div>'
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
			, aggregationType: uiGridConstants.aggregationTypes.sum
			, filters:
			[
				{
					// condition: uiGridConstants.filter.GREATER_THAN,
					condition: function(term, value, row, column)
					{
						if(!term) return true;
						return parseFloat(value) > parseFloat(term);
					},
					placeholder: gettextCatalog.getString("greater", null, "Grid Filters"),
				},

				{
					// condition: uiGridConstants.filter.LESS_THAN,
					condition: function(term, value, row, column)
					{
						if(!term) return true;
						return parseFloat(value) < parseFloat(term);
					},
					placeholder: gettextCatalog.getString("less", null, "Grid Filters"),
				}
  			]
		});

		$scope.gridOptions.columnDefs.push(
		{
			field: 'user_name'
			,displayName:gettextCatalog.getString("User", null, "Context Grid Columns")
			, enableHiding:true
			,filters:
			[
				{
					condition: uiGridConstants.filter.CONTAINS,
					placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
				}
				,
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value.length > 0) return false;
						return true;
					},
				},
			]
		});


		// setColumnFiltersTerms();
		tryLoadGridState();

		// if ($scope.gridApi) $scope.gridApi.core.notifyDataChange($scope.gridApi.grid, uiGridConstants.dataChange.ALL );

		$scope.loadingData = false;

		setAllGridColsCellClass();

		GUI.Grid.SetGridHeight();
		GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
	}

	function tryLoadGridState()
	{
		var promise = CacheService.GetUserSettings();
		promise.then(function(result)
		{
			$timeout((function()
			{
				if (result.grid_state)
				{
					$timeout((function()
					{
						var state = JSON.parse(result.grid_state);

						if (state && state.account_id == $scope.user.account_id && state.grid_state)
						{
							$scope.gridApi.saveState.restore( $scope, state.grid_state);
						}
						// setColumnFiltersTerms();


						$scope.gridApi.colMovable.on.columnPositionChanged($scope, saveState);
						$scope.gridApi.colResizable.on.columnSizeChanged($scope, saveState);
						//$scope.gridApi.grouping.on.aggregationChanged($scope, saveState);
						//$scope.gridApi.grouping.on.groupingChanged($scope, saveState);
						$scope.gridApi.core.on.columnVisibilityChanged($scope, saveState);
						// $scope.gridApi.core.on.filterChanged($scope, saveState);
						$scope.gridApi.core.on.sortChanged($scope, saveState);

					}), 0);
				}

				setColumnFiltersTerms();
			}), 0);
		},
		function(err)
		{
			$scope.loadingData = false;
			setColumnFiltersTerms();
		})
	}

	function setGridCustomColumns()
	{
		var gridOptions = $scope.gridOptions;

		for (var i = 0; i < $scope.custom_filed_names.length; i++)
		{
			var cfn = $scope.custom_filed_names[i]
			if (cfn.type == 8 && $scope.user_settings.grid_row_height == null)
			{
				$scope.gridOptions.rowHeight = 130;
				break;
			}
		};

		if (setting_force_images_first)
		{
			var images_cfn = get_custom_fields_by_type(8);

			for(var i = 0; i < images_cfn.length;i++)
			{
				var cfn = images_cfn[i];

				addImageFieldToGrid(gridOptions, cfn);
			}
		}

		for(var i = 0; i < $scope.custom_filed_names.length;i++)
		{
			var cfn = $scope.custom_filed_names[i];

			if (cfn)
			{
				switch (Number(cfn.type))
				{
					case 1: 	// text value
					case 5: 	// barcode value
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,enableCellEdit: $scope.user && $scope.user.perm_items_edit==1
							,displayName: cfn.name
							,headerCellClass:$scope.user && $scope.user.perm_items_edit==1 ? 'inline-edit-enabled-header-cell' : ''
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'

							,filters:
							[
								{
									condition: uiGridConstants.filter.CONTAINS,
									placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
								}
								,
								{

									type: uiGridConstants.filter.SELECT,
									selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
									condition: function(term, value, row, column)
									{
										// true ak sa nema filtrovat
										if (term == 1 && value.length > 0) return false;
										return true;
									},
								},
							]
						});
						break;
					}
					case 4: // date time
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,sortingAlgorithm: Helpers.Sorting.Date
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'

							,filters:
							[
								{
									condition: uiGridConstants.filter.CONTAINS,
									placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
								}
								,
								// {

								// 	type: uiGridConstants.filter.SELECT,
								// 	selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
								// 	condition: function(term, value, row, column)
								// 	{
								// 		// true ak sa nema filtrovat
								// 		if (term == 1 && value.length > 0) return false;
								// 		return true;
								// 	},
								// },
							]
						});
						break;
					}
					case 3: // bit
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
							,filter:
							{
								type: uiGridConstants.filter.SELECT
								, selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
							},

						});
						break;
					}

					case 2: // number
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							, enableHiding:true
							, sortingAlgorithm: Helpers.Sorting.Numeric
							, displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
							, aggregationType: uiGridConstants.aggregationTypes.sum
							, filters:
								[
									{
										// condition: uiGridConstants.filter.GREATER_THAN,
										condition: function(term, value, row, column)
										{
											if(!term) return true;
											return parseFloat(value) > parseFloat(term);
										},
										placeholder: gettextCatalog.getString("greater", null, "Grid Filters"),
									},

									{
										// condition: uiGridConstants.filter.LESS_THAN,
										condition: function(term, value, row, column)
										{
											if(!term) return true;
											return parseFloat(value) < parseFloat(term);
										},
										placeholder: gettextCatalog.getString("less", null, "Grid Filters"),
									}
									]
						});
						break;
					}

					case 7: // GPS
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.gps_row_clicked(COL_FIELD)">{{COL_FIELD}} </a></div>'

							,filters:
							[
								{
									condition: uiGridConstants.filter.CONTAINS,
									placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
								}
								,
								{

									type: uiGridConstants.filter.SELECT,
									selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
									condition: function(term, value, row, column)
									{
										// true ak sa nema filtrovat
										if (term == 1 && value.length > 0) return false;
										return true;
									},
								},
							]
						});
						break;
					}

					case 8: // images
					{
						if (!setting_force_images_first) addImageFieldToGrid(gridOptions, cfn);
						break;
					}

					case 9: // MIN_COUNT_ALERT_VALUE_TYPE
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							, enableHiding:true
							, sortingAlgorithm: Helpers.Sorting.Numeric
							, displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
							, aggregationType: uiGridConstants.aggregationTypes.sum
							, filters:
								[
									{
										// condition: uiGridConstants.filter.GREATER_THAN,
										condition: function(term, value, row, column)
										{
											if(!term) return true;
											return parseFloat(value) > parseFloat(term);
										},
										placeholder: gettextCatalog.getString("greater", null, "Grid Filters"),
									},

									{
										// condition: uiGridConstants.filter.LESS_THAN,
										condition: function(term, value, row, column)
										{
											if(!term) return true;
											return parseFloat(value) < parseFloat(term);
										},
										placeholder: gettextCatalog.getString("less", null, "Grid Filters"),
									},

									{
										type: uiGridConstants.filter.SELECT,
										selectOptions: [  { value: '1', label: gettextCatalog.getString("alert", null, "Grid Filters") } ],
										condition: function(term, value, row, column)
										{
											if (term != 1) return true;

											var minimum_item_alert_count_cfn = get_custom_fields_by_type(9);

											for(var i = 0; i < minimum_item_alert_count_cfn.length;i++)
											{
												var cfn = minimum_item_alert_count_cfn[i];

												if ( (row.entity["custom_field_id_" + cfn.custom_field_name_id]) && Number(row.entity.count) <= Number(row.entity["custom_field_id_" + cfn.custom_field_name_id]) )
												{
													return true;
												}
											}

											return false;
										},
									},
								]
						});
						break;
					}

					default:
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'

							,filters:
							[
								{
									condition: uiGridConstants.filter.CONTAINS,
									placeholder: gettextCatalog.getString("contains", null, "Grid Filters"), //'contains'
								}
								,
								{

									type: uiGridConstants.filter.SELECT,
									selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
									condition: function(term, value, row, column)
									{
										// true ak sa nema filtrovat
										if (term == 1 && value.length > 0) return false;
										return true;
									},
								},
							]
						});
						break;
					}
				}
			}
		}
	}

	function setAllGridColsCellClass()
	{
		var minimum_item_alert_count_cfn = get_custom_fields_by_type(9);

		var cellClassFunction = function(grid, row, col, rowRenderIndex, colRenderIndex)
		{
			for(var i = 0; i < minimum_item_alert_count_cfn.length;i++)
			{
				var cfn = minimum_item_alert_count_cfn[i];

				if (row.entity["custom_field_id_" + cfn.custom_field_name_id])
				{
					if (Number(row.entity.count) <= Number(row.entity["custom_field_id_" + cfn.custom_field_name_id]) )
					{
						return "cell-class-red-alert";
					}
				}
			}

			// if (grid.getCellValue(row,col) === 'Velity')
			// {
			// 	return 'blue';
			// }

			// return "cell-class-red-alert";
		}


		var gridOptions = $scope.gridOptions;
		for (var i = 0; i < gridOptions.columnDefs.length; i++)
		{
			var colDef = gridOptions.columnDefs[i];
			colDef.cellClass = cellClassFunction;
		};
	}

	function addImageFieldToGrid(gridOptions, cfn)
	{
		gridOptions.columnDefs.push(
		{
			field: 'custom_field_id_'+cfn.custom_field_name_id+"_image_url"
			,enableHiding:true
			,displayName: cfn.name

			, cellTemplate: ' <div ng-click="grid.appScope.itemImageClicked(COL_FIELD)" style="cursor:pointer; margin:3px; height:150px;"  ng-bind-html="grid.appScope.generate_img_tag_from_urls(COL_FIELD)"></div> '

			,filters:
			[
				{

					type: uiGridConstants.filter.SELECT,
					selectOptions: [  { value: '1', label: gettextCatalog.getString("empty", null, "Grid Filters") } ],
					condition: function(term, value, row, column)
					{
						// true ak sa nema filtrovat
						if (term == 1 && value != null) return false;
						return true;
					},
				},
			]

			// , cellTemplate: '<div class="ui-grid-cell-contents" > <div ng-bind-html="grid.appScope.generate_img_tag_from_urls(COL_FIELD)"></div>  </div>'
			// , cellTemplate: '{{ grid.appScope.generate_img_tag_from_urls(COL_FIELD) }}'
			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
		});
	}

	function setColumnFiltersTerms()
	{
		if (GlobalVariablesService.ItemsCategoriesFilter != null)
		{
			var col = _.findWhere($scope.gridApi.grid.columns, {field: "categories"});
			col.filters[0].term = GlobalVariablesService.ItemsCategoriesFilter;

			GlobalVariablesService.ItemsCategoriesFilter = null;
		}

		if (GlobalVariablesService.ItemsTagsFilter != null)
		{
			var col = _.findWhere($scope.gridApi.grid.columns, {field: "tags"});
			col.filters[0].term = GlobalVariablesService.ItemsTagsFilter;

			GlobalVariablesService.ItemsTagsFilter = null;
		}

		if (GlobalVariablesService.ItemsLocationsFilter != null)
		{
			var col = _.findWhere($scope.gridApi.grid.columns, {field: "locations"});
			col.filters[0].term = GlobalVariablesService.ItemsLocationsFilter;

			GlobalVariablesService.ItemsLocationsFilter = null;
		}

		if (GlobalVariablesService.ItemsStatesFilter != null)
		{
			var col = _.findWhere($scope.gridApi.grid.columns, {field: "states"});
			col.filters[0].term = GlobalVariablesService.ItemsStatesFilter;

			GlobalVariablesService.ItemsStatesFilter = null;
		}

		if ($scope.gridApi.core)
		{
			$scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL );
			// $scope.gridApi.saveState.restore( $scope, state.grid_state);
		}
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
			userLoaded();
		},
		function(err)
		{
		});
	}

	function userLoaded()
	{
	}

	function getAppSettings()
	{
		var appSettingsPromise = SettingsService.get_app_settings();

		appSettingsPromise.then(function(result)
		{
			if (result.ItemsGrid_ForceImagesFieldsFirst)
			{
				setting_force_images_first = result.ItemsGrid_ForceImagesFieldsFirst
			}
		},
		function(err)
		{
		});

		return appSettingsPromise;
	}




}]);
