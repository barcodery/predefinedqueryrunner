'use strict';

angular.module('BarcoderyApp')
.controller('StatesCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService','$modal', 'GlobalVariablesService', 'UserService', 'gettextCatalog',
function ($scope, $location, ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService, gettextCatalog)
{
	$scope.isDeleteButtonDisabled = true;
	$scope.isEditButtonEnabled = false;
	$scope.deleteButtonTooltip = '';
	$scope.alerts = [];

	loadUser();
	loadData();
	initGrid();

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.addButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddEditStateDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditStateDialog.html',
			size: 'sm',
			resolve: {
				item: function ()
				{
					return null;
				}
			}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetTagsDirty();
				loadData();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var item = rows[0].entity;

		var modalInstance = $modal.open({
			controller: 'AddEditStateDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditStateDialog.html',
			size: 'sm',
			resolve: {
				item: function ()
				{
					return item;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetTagsDirty();
				CacheService.SetItemsDirty();
				loadData();
			}
		});
	}

	$scope.deleteButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var ids = [];

		for(var i in rows)
		{
			ids.push(rows[i].entity.state_id);
			if (rows[i].entity.count > 0)
			{
				$scope.alerts.push({ type: 'danger', msg: gettextCatalog.getString("States", null, "Only empty statses can be deleted.")});
				return;
			}
		}

		$scope.working = true;
		ApiService.StatesAPI.delete({ids:JSON.stringify(ids)}).$promise
		.then(function(result)
		{
			CacheService.SetTagsDirty();
			loadData();
			$scope.isDeleteButtonDisabled = true;
		},
		function(err)
		{
			$scope.working =false;
		});
	}

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.itemClicked = function(row)
	{
		var name = row.entity.name;
		GlobalVariablesService.ItemsStatesFilter = name;
		$scope.redirect('/items');
	}

	$scope.exportToCsv = function(rowType)
	{
		var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
		$scope.gridApi.exporter.csvExport( rowType, 'all' , myElement);
	}

	$scope.refreshClicked = function()
	{
		loadData();
	}

	function loadData()
	{
		$scope.working = true;
		var promise = CacheService.GetStates();
		promise.then(function(result)
		{
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;
			$scope.working = false;
			GUI.Grid.SetGridHeight();
			GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

			$scope.isDeleteButtonDisabled = true;
			$scope.isEditButtonEnabled = false;
		},
		function(err)
		{
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		});
	}

	$scope.generate_color_rectangle = function(color)
	{
		var html = "<div style='width:25px; height:25px; background:"+ color+"; '> <b>" + color +"</b> </div>";
		var html = "<div style='width:25px; height:25px; background:red'> <b>" + color +"</b> </div>";
		return html;
	}

	function initGrid()
	{
		$scope.gridOptions = {

			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,


			exporterCsvFilename: 'states.csv',
			exporterOlderExcelCompatibility:true,

			exporterPdfDefaultStyle: {fontSize: 9},
			exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
			exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
			exporterPdfHeader: { text: "Tags", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
			exporterPdfFooter: function ( currentPage, pageCount )
			{
			  return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			},

			exporterPdfCustomFormatter: function ( docDefinition )
			{
			  docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
			  docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
			  return docDefinition;
			},

			exporterPdfOrientation: 'portrait',
			exporterPdfPageSize: 'LETTER',
			exporterPdfMaxGridWidth: 500,
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),



			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;
				GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
			}
		};

		$scope.gridOptions.columnDefs =
		[
		{
			field: 'name'
			,displayName:gettextCatalog.getString("Name", null, "Context Grid Columns")
			, enableHiding:true
			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
		}
		,
		{
			field: 'color'
			,displayName:gettextCatalog.getString("Color", null, "Context Grid Columns")
			, enableHiding:true
			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'
			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'

			// var html = "<div style='width:25px; height:25px; background:"+ color+"; '> <b>" + color +"</b> </div>";
			// , cellTemplate: ' <div style="cursor:pointer; margin:3px; height:150px;"  ng-bind-html="grid.appScope.generate_color_rectangle(COL_FIELD)"></div> '
			, cellTemplate: ' <div style=" margin-left:5px; margin:2px; width:100%; max-width:25px; height:100%; background:{{COL_FIELD}}"></div> '
		}

		,{
			field: 'count'
			,displayName:gettextCatalog.getString("Count", null, "Context Grid Columns")
			, enableHiding:true
			,sortingAlgorithm: Helpers.Sorting.Numeric
		}
		,{
			field: 'user_name'
			,displayName:gettextCatalog.getString("User", null, "Context Grid Columns")
			, enableHiding:true
		}
		];
	}

	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows()

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}

	function enableDisableEditButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

}]);

