'use strict';

angular.module('BarcoderyApp')
.controller('RequestDetailTabCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'uiGridConstants', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService, uiGridConstants, gettextCatalog )
{
    GlobalVariablesService.scope_request_details = $scope;
    $scope.working = false;
    $scope.gridOptions = {};

    load_user();
    init();



    // public methods

    $scope.load = function (request)
    {
        $scope.request = request;

        $scope.working = true;
        var promise = ApiService.RequestsAPI.get_request_items({request_id:request.request_id}).$promise;

        promise.then(function(result)
        {
            $scope.audit_items = result;
            $scope.gridOptions.data = result;
            // $scope.noItems = result.length == 0;

            $scope.working = false;
            GUI.Grid.SetGridHeight();
            GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
            $scope.isEditButtonEnabled = false;
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });

        return promise;
	}


    // other methods

    function init()
    {
        var customFieldNamesPromise = CacheService.GetCustomFieldNames();
		customFieldNamesPromise.then(function(result)
		{
			$scope.custom_field_names = result;
			init_grid();
		},
		function(err)
		{
			$scope.custom_field_names = [];
		});
	}

    function init_grid()
    {
        $scope.gridOptions = {

            showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: true,


            exporterMenuPdf: false,

            exporterCsvFilename: 'audit.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
			},

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    enableDisableEditButton();
				});

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    enableDisableEditButton();
				});
			}
		};

        $scope.gridOptions.columnDefs = [];
        set_grid_columns($scope.gridOptions);
	}

    function set_grid_columns(gridOptions)
    {
        set_grid_custom_field_columns(gridOptions);

        gridOptions.columnDefs.push({
            field: 'location_name'
            // ,displayName: 'Location'
            ,displayName:gettextCatalog.getString("Location", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.location_link_clicked(row,  COL_FIELD )"> {{ COL_FIELD }} </a></div>'
            // , aggregationType: uiGridConstants.aggregationTypes.sum
            , cellClass: grid_get_cell_class_based_on_cover_state
		});

        gridOptions.columnDefs.push({
            field: 'created_user'
            // ,displayName: 'Created by'
            ,displayName:gettextCatalog.getString("Created by", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            // , aggregationType: uiGridConstants.aggregationTypes.sum
            , cellClass: grid_get_cell_class_based_on_cover_state
		});

        gridOptions.columnDefs.push({
            field: 'created_date_time'
            // ,displayName: 'Created date'
            ,displayName:gettextCatalog.getString("Created date", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.format_date_time(COL_FIELD) }} </div>'
            , cellClass: grid_get_cell_class_based_on_cover_state
		});

        gridOptions.columnDefs.push({
            field: 'covered_date_time'
            // ,displayName: 'Covered date'
            ,displayName:gettextCatalog.getString("Covered date", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.format_date_time(COL_FIELD) }} </div>'
            , cellClass: grid_get_cell_class_based_on_cover_state
        });

      gridOptions.columnDefs.push({
            field: 'covered_user'
            // ,displayName: 'Covered user'
            ,displayName:gettextCatalog.getString("Covered user", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            // , aggregationType: uiGridConstants.aggregationTypes.sum
            , cellClass: grid_get_cell_class_based_on_cover_state
        });

        gridOptions.columnDefs.push({
             field: 'covered_by_in_out_entry_id'
            // , displayName: 'Covered type'
            ,displayName:gettextCatalog.getString("Covered type", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.get_request_item_covered_type(row) }} </div>'
            , cellClass: grid_get_cell_class_based_on_cover_state
        });

        gridOptions.columnDefs.push({
             field: 'is_active'
            // , displayName: 'State'
            ,displayName:gettextCatalog.getString("State", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.get_request_item_state(row) }} </div>'
            , cellClass: grid_get_cell_class_based_on_cover_state
        });

        gridOptions.columnDefs.push({
            field: 'note'
            // ,displayName: 'Note'
            ,displayName:gettextCatalog.getString("Note", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{ COL_FIELD }} </div>'
            , cellClass: grid_get_cell_class_based_on_cover_state
        });
	}

    function set_grid_custom_field_columns(gridOptions)
    {
        var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
        if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;

        var i = 0;
        for(var i = 0; i < custom_field_names.length;i++)
        {
    		var cfn = custom_field_names[i];

    		if (cfn)
    		{
    			switch (Number(cfn.type))
    			{
    				case 4: // date time
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
                            , cellClass: grid_get_cell_class_based_on_cover_state
    					});
    					break;
    				}
    				case 3: // bit
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
    						,filter:
    						{
    							type: uiGridConstants.filter.SELECT
    							, selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
    						}
                            , cellClass: grid_get_cell_class_based_on_cover_state

    					});
    					break;
    				}

    				default:
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
                            , cellClass: grid_get_cell_class_based_on_cover_state
    					});
    					break;
    				}
    			}
    		}
        }
	}

    function grid_get_cell_class_based_on_cover_state(grid, row, col, rowRenderIndex, colRenderIndex)
    {
       // if (is_covered(row))
       //  {
       //      return 'alert-cell';
       //  }
        return '';
    }




    // handlers

    $scope.refreshClicked = function()
    {
        refresh();
	}

    $scope.itemClicked = function(row)
    {
        var item_id = row.entity.item_id
        $scope.redirect('/item_detail/' + item_id);
	}

    $scope.location_link_clicked = function(row, cellContent)
    {
        GlobalVariablesService.ItemsLocationsFilter = cellContent;
        $scope.redirect('/items');
    }

    $scope.force_cover_state_button_clicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var row_ids = _.map(rows, function(row){ return row.entity.request_item_id; });

        $scope.working = true;
        var promise = ApiService.RequestsAPI.cover_request_items({ids:JSON.stringify(row_ids)}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            refresh();
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
	}

    $scope.uncover_button_clicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var row_ids = _.map(rows, function(row){ return row.entity.request_item_id; });

        $scope.working = true;
        var promise = ApiService.RequestsAPI.uncover_request_items({ids:JSON.stringify(row_ids)}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            refresh();
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
    }

    $scope.add_button_clicked = function()
    {
        var modalInstance = $modal.open({
            controller: 'AddRequestItemDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddRequestItemDialog.html',
            size: 'lg',
            resolve:
            {
               items_id: function ()
                {
                    return null;
                }
                ,
                request_id: function()
                {
                    return $scope.request.request_id;
                }

            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                refresh();
            }
        });
    }

    $scope.delete_button_clicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var row_ids = _.map(rows, function(row){ return row.entity.request_item_id; });

        $scope.working = true;
        var promise = ApiService.RequestsAPI.delete_request_items({ids:JSON.stringify(row_ids)}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            refresh();
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        });
    }



    // gui helper function

    $scope.format_date_time = function (dt)
    {
        return HelperService.getFormattedDate(dt);
	}

    $scope.get_request_item_state = function(row)
    {
        return is_covered(row) ? gettextCatalog.getString("Covered", null, "Context Request State") : gettextCatalog.getString("Active", null, "Context Request State");
    }

    $scope.get_request_item_covered_type = function(row)
    {
        if (is_covered(row))
        {
            var result = row.entity.covered_by_inout_entry_id == null ? gettextCatalog.getString("Forced", null, "Context Covered Type")
                                                                      : gettextCatalog.getString("By adding to location", null, "Context Covered Type");
            return result;
        }

        return "";
    }

    function is_covered(row)
    {
        return row.entity.covered_date_time == null ? false: true;
    }

    $scope.redirect = function(path, ev)
    {
        if (ev) ev.preventDefault();
        $location.path(path);
	}



    function refresh()
    {
        $scope.load($scope.request);
	}

    function load_user()
    {
        var promise = UserService.getUser();

        promise.then(function(result)
        {
            $scope.user = UserService.user;
		},
        function(err)
        {
		});
	}

    function enableDisableEditButton()
    {
        if (!$scope.gridApi)
        {
            return;
		}
        var rows = $scope.gridApi.selection.getSelectedGridRows();

        if (rows.length >= 1)
        {
            $scope.isEditButtonEnabled = true;
		}
        else
        {
            $scope.isEditButtonEnabled = false;
		}
	}

}]);
