'use strict';

angular.module('BarcoderyApp')
.controller('RequestsTabCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'uiGridConstants', 'gettextCatalog',
    function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService, uiGridConstants, gettextCatalog)
{
     GlobalVariablesService.scope_requests = $scope;
    $scope.set_type = function(type)
    {
        $scope.type = type;
    }


    // $scope.type = requests_type; // possible [all, my]

    $scope.isDeleteButtonDisabled = true;
    $scope.isEditButtonEnabled = false;
    $scope.deleteButtonTooltip = '';
    $scope.alerts = [];
    $scope.previous_is_show_active_checked = true;
    $scope.is_show_only_active_checked = true;
    $scope.selected_reqeust;

    $scope.HelperService = HelperService;

    loadUser();
    loadData();
    initGrid();

    $scope.redirect = function(path, ev)
    {
        if (ev) ev.preventDefault();
        $location.path(path);
    }

    $scope.addButtonClicked = function()
    {
        var modalInstance = $modal.open({
            controller: 'AddEditRequestDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddEditRequestDialog.html',
            size: 'sm',
            resolve: {
                item: function ()
                {
                    return null;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                CacheService.SetCategoriesDirty();
                loadData();
            }

            $scope.isEditButtonEnabled = false;
        });
    }

    $scope.editButtonClicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var item = rows[0].entity;

        var modalInstance = $modal.open({
            controller: 'AddEditRequestDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddEditRequestDialog.html',
            size: 'sm',
            resolve: {
                item: function ()
                {
                    return item;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                CacheService.SetCategoriesDirty();
                CacheService.SetItemsDirty();
                loadData();
            }

            $scope.isEditButtonEnabled = false;
        });
    }

    $scope.deleteButtonClicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var ids = [];

        for(var i in rows)
        {
            ids.push(rows[i].entity.request_id);
        }

        var promise = ApiService.RequestsAPI.delete_requests({ids:JSON.stringify(ids) , type:$scope.type}).$promise;

        promise.then(function(result)
        {
            loadData();
            $scope.isDeleteButtonDisabled = true;
        },
        function(err)
        {
            $scope.working = false;
        });
    }


    // $scope.solveButtonClicked = function()
    // {
    //     if ($scope.is_group_checked) return;

    //     var rows = $scope.gridApi.selection.getSelectedGridRows();
    //     var ids = [];

    //     for(var i in rows)
    //     {
    //         ids.push(rows[i].entity.item_request_id);
    //     }

    //     $scope.working = true;

    //     var promise = ApiService.RequestsAPI.solve_requests({type: $scope.type, ids:JSON.stringify(ids)}).$promise;

    //     promise.then(function(result)
    //     {
    //         loadData();
    //         $scope.isDeleteButtonDisabled = true;
    //     },
    //     function(err)
    //     {
    //         $scope.working = false;
    //     });
    // }

    $scope.closeAlert = function(index)
    {
        $scope.alerts.splice(index, 1);
    };

    $scope.itemClicked = function(row)
    {
        GlobalVariablesService.scope_requests_tabset.activate_request_details_tab();

        if (GlobalVariablesService.scope_request_details != null)
        {
            GlobalVariablesService.scope_request_details.load(row.entity);
        }
    }

    $scope.fixxx = function()
    {
        GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
    }

    $scope.refreshClicked = function()
    {
        loadData();
        $scope.isEditButtonEnabled = false;
    }

    $scope.get_formatted_state = function(is_active)
    {
        if (is_active == 0 || is_active == '0') return gettextCatalog.getString("Covered", null, "Context Request State");

        return gettextCatalog.getString("Active", null, "Context Request State");
    }

    $scope.show_only_active_clicked = function(checked)
    {
        $scope.previous_is_show_active_checked = checked;

        loadData();
        initGrid();
    }


    function loadData()
    {
        $scope.working = true;

        var promise = {};

        var only_active = $scope.is_show_only_active_checked ? 1 : 0;

        promise = ApiService.RequestsAPI.get({ only_active: only_active}).$promise;

        promise.then(function(result)
        {
            $scope.noItems = result.length == 0;
            $scope.requests = result;

            // var customFieldNamesPromise = CacheService.GetCustomFieldNames();
            // customFieldNamesPromise.then(function(result)
            // {
            //     $scope.custom_field_names = result;

            // },
            // function(err)
            // {
            //     $scope.custom_field_names = [];
            // });

            // return customFieldNamesPromise;

            var account_promise = ApiService.MyAccountAPI.getAccounts().$promise;

            account_promise.then(function(result)
            {
                $scope.account = result[0];
                HelperService.set_date_format($scope.account.date_format);
            },
            function(err)
            {

            });

            return account_promise;
        },
        function(err)
        {
            $scope.loadingData = false;
        })
        // .then(function(result)
        // {
        //     var account_promise = ApiService.MyAccountAPI.getAccounts().$promise;

        //     account_promise.then(function(result)
        //     {
        //         $scope.account = result[0];
        //         HelperService.set_date_format($scope.account.date_format);
        //     },
        //     function(err)
        //     {

        //     });

        //     return account_promise;

        // },
        // function(err)
        // {
        //     $scope.loadingData = false;
        // })
        .then(function(result)
        {
            // vsetko loadnute
            loadDataIntoGrid($scope.requests);

            $scope.working = false;
            GUI.Grid.SetGridHeight();
            GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

            $scope.isDeleteButtonDisabled = true;
        },
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
        })
        ;
    }

    function initGrid()
    {
        $scope.gridOptions = {

            enableSorting : true,
            enableRowSelection : true,
            enableSelectAll: true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,


            exporterMenuPdf: false,

            exporterCsvFilename: 'requests.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
            },

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    enableDisableEditButtons();
                });

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    enableDisableEditButtons();
                });
            }
        };
    }

    function loadDataIntoGrid(items)
    {
        $scope.gridOptions.data = items;

        $scope.gridOptions.columnDefs = [];

        // setGridCustomColumns($scope.gridOptions, items);

        $scope.gridOptions.columnDefs.push(
        {
            field: 'name'
            ,displayName:gettextCatalog.getString("Name", null, "Context Grid Columns")
            ,enableHiding:true
            ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD}} </a></div>'
            ,filter: {
                condition: uiGridConstants.filter.CONTAINS,
                placeholder: 'contains'
            }
        });

        $scope.gridOptions.columnDefs.push(
        {
            field: 'description'
            ,displayName:gettextCatalog.getString("Description", null, "Context Grid Columns")
            ,enableHiding:true
            ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD }} </a></div>'
        });

        $scope.gridOptions.columnDefs.push(
        {
            field: 'created_date_time'
            // ,displayName:'Created date'
            ,displayName:gettextCatalog.getString("Created date", null, "Context Grid Columns")
            ,enableHiding:true
            ,sortingAlgorithm: Helpers.Sorting.Date
            ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
        });

        $scope.gridOptions.columnDefs.push(
        {
            field: 'created_user'
            // ,displayName:'Created by user'
            ,displayName:gettextCatalog.getString("Created by user", null, "Context Grid Columns")
            ,enableHiding:true
            ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD }} </a></div>'
        });


        // $scope.gridOptions.columnDefs.push(
        // {
        //     field: 'covered_user'
        //     ,displayName:'Covered user'
        //     ,enableHiding:true
        //     ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD }} </a></div>'
        // });

        // $scope.gridOptions.columnDefs.push(
        // {
        //     field: 'note'
        //     ,displayName:'Note'
        //     ,enableHiding:true
        //     ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD }} </a></div>'
        // });

        // if ($scope.is_group_checked)
        // {
        //     $scope.gridOptions.columnDefs.push(
        //     {
        //         field: 'count'
        //         ,displayName:'Count'
        //         ,enableHiding:true
        //         ,sortingAlgorithm: Helpers.Sorting.Numeric
        //         ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ COL_FIELD }} </a></div>'
        //     });
        // }

        $scope.gridOptions.columnDefs.push(
        {
            field: 'is_active'
            // ,displayName:'State'

            ,displayName:gettextCatalog.getString("State", null, "Context Grid Columns")
            ,enableHiding:true
            ,cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{ grid.appScope.get_formatted_state(COL_FIELD) }}</a></div>'
        });

        $scope.loadingData = false;

        GUI.Grid.SetGridHeight();
        GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

        $scope.isDeleteButtonDisabled = true;
    }

    // function setGridCustomColumns(gridOptions, items)
    // {
    //     var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
    //     if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;

    //     var i = 0;
    //     for(var i = 0; i < custom_field_names.length;i++)
    //     {
    //         var cfn = custom_field_names[i];

    //         if (cfn)
    //         {
    //             switch (Number(cfn.type))
    //             {
    //                 case 4: // date time
    //                 {
    //                     gridOptions.columnDefs.push(
    //                     {
    //                         field: 'custom_field_id_'+cfn.custom_field_name_id
    //                         ,enableHiding:true
    //                         ,displayName: cfn.name
    //                         , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
    //                     });
    //                     break;
    //                 }
    //                 case 3: // bit
    //                 {
    //                     gridOptions.columnDefs.push(
    //                     {
    //                         field: 'custom_field_id_'+cfn.custom_field_name_id
    //                         ,enableHiding:true
    //                         ,displayName: cfn.name
    //                         , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
    //                         ,filter:
    //                         {
    //                             type: uiGridConstants.filter.SELECT
    //                             , selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
    //                         }

    //                     });
    //                     break;
    //                 }

    //                 default:
    //                 {
    //                     gridOptions.columnDefs.push(
    //                     {
    //                         field: 'custom_field_id_'+cfn.custom_field_name_id
    //                         ,enableHiding:true
    //                         ,displayName: cfn.name
    //                         , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
    //                     });
    //                     break;
    //                 }
    //             }

    //         }
    //     }
    // }

    function enableDisableEditButtons()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();

        if (rows.length >0)
        {
            $scope.isDeleteButtonDisabled = false;
        }
        else
        {
            $scope.isDeleteButtonDisabled = true;
        }

        if (rows.length == 1)
        {
            $scope.isEditButtonEnabled = true;
        }
        else
        {
            $scope.isEditButtonEnabled = false;
        }
    }

    function loadUser()
    {
        var promise = UserService.getUser();

        promise.then(function(result)
        {
            $scope.user = UserService.user;
        },
        function(err)
        {
        });
    }

}]);

