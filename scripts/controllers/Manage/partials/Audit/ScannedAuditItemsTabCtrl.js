'use strict';

angular.module('BarcoderyApp')
.controller('ScannedAuditItemsTabCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService)
{
    GlobalVariablesService.scope_scanned_audits_items_tabset = $scope;
}]);
