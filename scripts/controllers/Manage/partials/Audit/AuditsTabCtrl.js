'use strict';

angular.module('BarcoderyApp')
.controller('AuditsTabCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'uiGridGroupingConstants', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService,  uiGridGroupingConstants, gettextCatalog)
{
	$scope.isDeleteButtonDisabled = true;
	$scope.isEditButtonEnabled = false;
	$scope.deleteButtonTooltip = '';
	$scope.alerts = [];
	$scope.HelperService = HelperService;


	GlobalVariablesService.scope_audits_tabset = $scope;

	load_user();
	load_data();
	init_grid();

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.addButtonClicked = function(audit_type)
	{
		var modalInstance = $modal.open({
			controller: 'AddEditAuditDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditAuditDialog.html',
			size: 'lm',
			resolve:
			{
				audit: function ()
				{
					return null;
				},

				audit_type: function()
				{
					return audit_type;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				load_data();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		var audit = rows[0].entity;
		var audit_type = audit.location_name != null ? 'location' : 'category';

		var modalInstance = $modal.open({
			controller: 'AddEditAuditDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditAuditDialog.html',
			size: 'lm',
			resolve:
			{
				audit: function ()
				{
					return audit;
				},
				audit_type: function()
				{
					return audit_type;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				CacheService.SetItemsDirty();
				load_data();
			}
		});
	}

	$scope.deleteButtonClicked = function()
	{

		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var content = {title:'Delete', text:'Do you really want to delete selected audits ?'};

		var modalInstance = $modal.open(
		{
			controller: 'ConfirmDialogCtrl',
			templateUrl: 'views/ConfirmDialog.html',
			size: 'sm',
			resolve: {
				content: function ()
				{
					return content;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				var rows = $scope.gridApi.selection.getSelectedGridRows();
				var ids = [];

				for(var i in rows)
				{
					ids.push(rows[i].entity.audit_id);
				}

				ApiService.AuditAPI.delete({ids:JSON.stringify(ids)}).$promise
				.then(function(result)
				{
					load_data();
				},
				function(err)
				{
				});
			}
		});
	}

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};


	$scope.fixxx = function()
	{
		GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
	}

	$scope.refreshClicked = function()
	{
		$scope.refresh();
	}

	$scope.item_clicked = function(row)
	{
		GlobalVariablesService.scope_audits_tabset.activate_audit_items_tab();

		if (GlobalVariablesService.scope_audits_items_tabset != null)
		{
			GlobalVariablesService.scope_audits_items_tabset.load_audit_items(row.entity);
		}
	}

	$scope.item_location_clicked = function(row)
	{
		var name = row.entity.location_name;
		GlobalVariablesService.ItemsLocationsFilter = name;
		$scope.redirect('/items');
	}

	$scope.item_category_clicked = function(row)
	{
		var name = row.entity.category_name;
		GlobalVariablesService.ItemsCategoriesFilter = name;
		$scope.redirect('/items');
	}

	$scope.refresh = function()
	{
		load_data();
	}

	function load_user()
	{
		var user_promise = UserService.getUser();

		user_promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});

		var account_promise = ApiService.MyAccountAPI.getAccounts().$promise;

		account_promise.then(function(result)
		{
			$scope.account = result[0];
			HelperService.set_date_format($scope.account.date_format);
		},
		function(err)
		{

		});
	}

	function load_data()
	{
		$scope.working = true;
		var promise = ApiService.AuditAPI.get_audits().$promise;
		// var promise = ApiService.AuditAPI.get_audit_items({audit_id:0}).$promise;

		promise.then(function(result)
		{
			$scope.audits = result;
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;

			$scope.working = false;
			GUI.Grid.SetGridHeight();
			GUI.Grid.FixHalfLoadedGrid($scope.gridApi);


			// if ($scope.gridApi)
			// {
			// 	$scope.gridApi.grouping.clearGrouping();
			//     $scope.gridApi.grouping.groupColumn('audit_id');
			//     $scope.gridApi.grouping.aggregateColumn('audit_id', uiGridGroupingConstants.aggregation.COUNT);
			//     alert('gg');
			// }
		},
		function(err)
		{
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		});

		return promise;
	}

	function init_grid()
	{
		$scope.gridOptions = {

 			treeRowHeaderAlwaysVisible: false,
			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,

			exporterCsvFilename: 'audit.csv',

			// exporterPdfDefaultStyle: {fontSize: 9},
			// exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
			// exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
			// exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
			// exporterPdfFooter: function ( currentPage, pageCount )
			// {
			// return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			// },

			// exporterPdfCustomFormatter: function ( docDefinition )
			// {
			// docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
			// docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
			// return docDefinition;
			// },

			// exporterPdfOrientation: 'portrait',
			// exporterPdfPageSize: 'LETTER',
			// exporterPdfMaxGridWidth: 500,
			// exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

			exporterOlderExcelCompatibility:true,

			importerDataAddCallback: function ( grid, newObjects )
			{
				$scope.data = $scope.data.concat( newObjects );
			},

			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;

				GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
			}
		};

		$scope.gridOptions.columnDefs =
		[

		// {
		// 	field: 'audit_id'
		// 	,displayName: 'audit_id'
		// 	, enableHiding:true
		// 	, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_clicked(row)">{{COL_FIELD}} </a></div>'
		// 	, grouping: { groupPriority: 0 }
		// 	, treeAggregationType: uiGridGroupingConstants.aggregation.AVG
		// 	, customTreeAggregationFinalizerFn: function( aggregation )
		// 	{
	 //        	aggregation.rendered = aggregation.value;
	 //      	}
	 //      	,visible: false
		// },


		{
			field: 'name'

			// , displayName: 'Name'
			,displayName:gettextCatalog.getString("Name", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_clicked(row)">{{COL_FIELD}} </a></div>'
			// , grouping: { groupPriority: 1 }
			// , treeAggregationType: uiGridGroupingConstants.aggregation.COUNT
			// , customTreeAggregationFinalizerFn: function( aggregation )
			// {
	  //       	aggregation.rendered = aggregation.value;
	  //     	}
	  //     	,visible: true
		},
		{
			field: 'description'
			,displayName:gettextCatalog.getString("Description", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_clicked(row)">{{COL_FIELD}} </a></div>'
		},
		{
			field: 'created_date'
			// ,displayName: 'Created'
			,displayName:gettextCatalog.getString("Created", null, "Context Grid Columns")
			, enableHiding:true
			, sortingAlgorithm: Helpers.Sorting.Date
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_clicked(row)">{{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
		},
		{
			field: 'closed_date'
			// ,displayName: 'Closed'
			,displayName:gettextCatalog.getString("Closed", null, "Context Grid Columns")
			, enableHiding:true
			, sortingAlgorithm: Helpers.Sorting.Date
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_clicked(row)">{{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
		},

		{
			field: 'location_name'
			// ,displayName: 'Location'
			,displayName:gettextCatalog.getString("Location", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_location_clicked(row)">{{COL_FIELD}} </a></div>'
		},
		{
			field: 'category_name'
			// ,displayName: 'Category'
			,displayName:gettextCatalog.getString("Category", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.item_category_clicked(row)">{{COL_FIELD}} </a></div>'
		},


		];
	}

	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}

	function enableDisableEditButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}

}]);
