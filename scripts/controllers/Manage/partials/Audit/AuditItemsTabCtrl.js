'use strict';

angular.module('BarcoderyApp')
.controller('AuditItemsTabCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'uiGridConstants', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService, uiGridConstants, gettextCatalog )
{
    GlobalVariablesService.scope_audits_items_tabset = $scope;
    $scope.working = false;
    $scope.gridOptions = {};

    load_user();
    init();


    // public methods

    $scope.load_audit_items = function (audit)
    {
        $scope.audit = audit;

        $scope.working = true;
        var promise = ApiService.AuditAPI.get_audit_items({audit_id:$scope.audit.audit_id}).$promise;

        promise.then(function(result)
        {
            $scope.audit_items = result;
            $scope.gridOptions.data = result;
            // $scope.noItems = result.length == 0;

            $scope.working = false;
            GUI.Grid.SetGridHeight();
            GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
            $scope.isEditButtonEnabled = false;
		},
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
		});

        return promise;
	}


    // other methods

    function init()
    {
        var customFieldNamesPromise = CacheService.GetCustomFieldNames();
		customFieldNamesPromise.then(function(result)
		{
			$scope.custom_field_names = result;
			init_grid();
		},
		function(err)
		{
			$scope.custom_field_names = [];
		});
	}

    function init_grid()
    {
        $scope.gridOptions = {

            showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: true,


            exporterMenuPdf: false,

            exporterCsvFilename: 'audit.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
			},

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    enableDisableEditButton();
				});

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    enableDisableEditButton();
				});
			}
		};

        $scope.gridOptions.columnDefs = [];
        set_grid_columns($scope.gridOptions);
	}

    function set_grid_columns(gridOptions)
    {
        set_grid_custom_field_columns(gridOptions);

        gridOptions.columnDefs.push({
            field: 'required_count'
            // ,displayName: 'Required Count'
            ,displayName:gettextCatalog.getString("Required Count", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            , aggregationType: uiGridConstants.aggregationTypes.sum
		});

        gridOptions.columnDefs.push({
            field: 'count'
            // ,displayName: 'Count'
            ,displayName:gettextCatalog.getString("Count", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            , aggregationType: uiGridConstants.aggregationTypes.sum
		});

        gridOptions.columnDefs.push({
            field: 'missing_count'
            // ,displayName: 'Missing'
            ,displayName:gettextCatalog.getString("Missing", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            , aggregationType: uiGridConstants.aggregationTypes.sum
            , cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex)
			{
				if (grid.getCellValue(row,col) !="0" )
				{
					return 'alert-cell';
				}
				return '';
			}



		});

        gridOptions.columnDefs.push({
            field: 'extra_count'
            // ,displayName: 'Extra'
            ,displayName:gettextCatalog.getString("Extra", null, "Context Grid Columns")
            , enableHiding:true
            , sortingAlgorithm: Helpers.Sorting.Numeric
            , cellTemplate: '<div class="ui-grid-cell-contents" > {{COL_FIELD}} </div>'
            , aggregationType: uiGridConstants.aggregationTypes.sum
            , cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex)
			{
				if (grid.getCellValue(row,col) !="0" )
				{
					return 'alert-cell';
				}
				return '';
			}
		});
	}

    function set_grid_custom_field_columns(gridOptions)
    {
        var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
        if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;

        var i = 0;
        for(var i = 0; i < custom_field_names.length;i++)
        {
    		var cfn = custom_field_names[i];

    		if (cfn)
    		{
    			switch (Number(cfn.type))
    			{
    				case 4: // date time
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
    					});
    					break;
    				}
    				case 3: // bit
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
    						,filter:
    						{
    							type: uiGridConstants.filter.SELECT
    							, selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
    						}

    					});
    					break;
    				}

    				default:
    				{
    					gridOptions.columnDefs.push(
    					{
    						field: 'custom_field_id_'+cfn.custom_field_name_id
    						,enableHiding:true
    						,displayName: cfn.name
    						, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
    					});
    					break;
    				}
    			}
    		}
        }
	}

    function get_custom_fields_for_report()
    {
        var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
        if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;


        var result = [];
        var i = 0;
        for(var i = 0; i < custom_field_names.length;i++)
        {
    		var cfn = custom_field_names[i];
    		if (!cfn) continue;

            result.push({title: cfn.name, dataKey: 'custom_field_id_'+cfn.custom_field_name_id});
        }

        return result;
	}




    // handlers

    $scope.refreshClicked = function()
    {
        refresh();
	}

    $scope.itemClicked = function(row)
    {
        var item_id = row.entity.item_id
        $scope.redirect('/item_detail/' + item_id);
	}

    $scope.incrementButtonClicked = function ()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var row_ids = _.map(rows, function(row){ return row.entity.audit_item_id; });

        $scope.working = true;
        var promise = ApiService.AuditAPI.increment_audit_items({ids:JSON.stringify(row_ids)}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            refresh();
		},
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
		});
	}

    $scope.decrementButtonClicked = function ()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var row_ids = _.map(rows, function(row){ return row.entity.audit_item_id; });

        $scope.working = true;
        var promise = ApiService.AuditAPI.decrement_audit_items({ids:JSON.stringify(row_ids)}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            refresh();
		},
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
		});
	}

    $scope.closeButtonClicked = function()
    {
        $scope.working = true;
        var promise = ApiService.AuditAPI.close_audit({audit_id:$scope.audit.audit_id}).$promise;

        promise.then(function(result)
        {
            $scope.working = false;
            $scope.audit = null;
            GlobalVariablesService.scope_audits_tabset.activate_audits_tab();
            if (GlobalVariablesService.scope_audits_tabset != null)
            {
                GlobalVariablesService.scope_audits_tabset.refresh();
			}
		},
        function(err)
        {
            $scope.working = false;
            GUI.Grid.SetGridHeight();
		});
	}

    $scope.editCountButtonClicked = function()
    {
        var rows = $scope.gridApi.selection.getSelectedGridRows();
        var audit_items = _.map(rows, function(row){ return row.entity; });

        var modalInstance = $modal.open({
            controller: 'EditAuditItemsCountDialogCtrl',
            templateUrl: 'views/Manage/dialogs/EditAuditItemsCountDialog.html',
            size: 'sm',
            resolve:
            {
                audit_items: function ()
                {
                    return audit_items;
				}
			}
		});

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                refresh();
			}
		});
	}

    $scope.downloadReportButtonClicked = function()
    {
        generate_pdf_report();
	}

    $scope.addExtraItemButtonClicked = function()
    {
        var modalInstance = $modal.open({
            controller: 'AddExtraAuditItemDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddExtraAuditItemDialog.html',
            size: 'lg',
            resolve:
            {
               audit: function ()
                {
                    return $scope.audit;
                }
                ,
                audit_items: function ()
                {
                    return $scope.audit_items;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                refresh();
            }
        });
    }


    // gui helper function

    $scope.format_date_time = function (dt)
    {
        return HelperService.getFormattedDate(dt);
	}

    $scope.location_link_clicked = function()
    {
        var name = $scope.audit.location_name;
        GlobalVariablesService.ItemsLocationsFilter = name;
        $scope.redirect('/items');
	}

    $scope.category_link_clicked = function()
    {
        var name = $scope.audit.category_name;
        GlobalVariablesService.ItemsCategoriesFilter = name;
        $scope.redirect('/items');
	}

    $scope.redirect = function(path, ev)
    {
        if (ev) ev.preventDefault();
        $location.path(path);
	}


    function generate_pdf_report()
    {
		var doc = new jsPDF({unit: 'pt', lineHeight: 1.5, orientation: 'p'});

        doc.setFontSize(18);
        doc.text('Audit report - ' + $scope.audit.name, 40, 60);
        doc.setFontSize(11);
        doc.setTextColor(100);

        // create audit table

        // doc.setFontType("bold");
        // doc.text('Audit Name  :' , 40, 100);
        // doc.text('Description :' , 40, 120);
        // doc.text('Created     :' , 40, 120);
        // doc.text('Closed      :' , 40, 120);

        var cloned_audit = _.clone($scope.audit);
        cloned_audit.loc_created = $scope.format_date_time($scope.audit.created_date);
        cloned_audit.loc_closed  = $scope.format_date_time($scope.audit.closed_date);

        var columnDefs =
        [
		{title: "Audit Name", dataKey: "name"},
		// {title: "Description", dataKey: "description"},
		{title: "Created", dataKey: "loc_created"},
		{title: "Closed", dataKey: "loc_closed"},
        ];

        if ($scope.audit.type == 'location') columnDefs.push({title: "Location", dataKey: "location_name"});
        if ($scope.audit.type == 'category') columnDefs.push({title: "Category", dataKey: "category_name"});

        var data =
        [
		cloned_audit,
        ];

        doc.autoTable(columnDefs, data, { startY: 90  });


        // create audit items table

        doc.setFontSize(15);
        doc.text('Audit items', 40, doc.autoTableEndPosY() + 40);
        doc.setFontSize(11);


        // var text = doc.splitTextToSize('shuffleSentence(faker.lorem.words(55))' + '.', doc.internal.pageSize.width - 80, {});
        // doc.text(text, 40, 80);

        var columnDefs = get_custom_fields_for_report();
		columnDefs.push( {title: "Required Count", dataKey: "required_count"});
		columnDefs.push( {title: "Count", dataKey: "count"});
		columnDefs.push( {title: "Missing Count", dataKey: "missing_count"});
		columnDefs.push( {title: "Extra Count", dataKey: "extra_count"});

        var yPosition = doc.autoTableEndPosY() + 50;

        // yPosition = 250;

        doc.autoTable(columnDefs, $scope.gridOptions.data ,
		{
			startY: yPosition
		  ,  createdCell: function (cell, data)
            {
				if (data.column.dataKey === 'missing_count' || data.column.dataKey === 'extra_count' )
                {
					cell.styles.halign = 'right';
					if (cell.raw != 0 || cell.raw != '0')
                    {
						cell.styles.textColor = [255, 100, 100];
						cell.styles.fontStyle = 'bold';
					}

					// cell.text = '$' + cell.text;
				}
			}
		});

        var now = new Date();
        var generated_date_text = 'Generated: ' + now.toLocaleDateString()  + ' ' + now.toLocaleTimeString();
        doc.text(generated_date_text, 40, doc.internal.pageSize.height - 30);



        // set page numbers
        for (var i = 0; i < doc.internal.pages.length - 1; i++)
        {
            doc.setPage(i);
            doc.text(doc.internal.pageSize.width - 70, doc.internal.pageSize.height - 30 , 'Page ' + (i+1) );
		};

        doc.save('Audit_'+$scope.audit.name +'.pdf' , 'right' );
	}

    function refresh()
    {
        $scope.load_audit_items($scope.audit);
	}

    function load_user()
    {
        var promise = UserService.getUser();

        promise.then(function(result)
        {
            $scope.user = UserService.user;
		},
        function(err)
        {
		});
	}

    function enableDisableEditButton()
    {
        if (!$scope.gridApi)
        {
            return;
		}
        var rows = $scope.gridApi.selection.getSelectedGridRows();

        if (rows.length >= 1)
        {
            $scope.isEditButtonEnabled = true;
		}
        else
        {
            $scope.isEditButtonEnabled = false;
		}
	}

}]);
