'use strict';

angular.module('BarcoderyApp')
.controller('ItemLocationsCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', '$timeout', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, $timeout, gettextCatalog)
{
	$scope.alerts = [];
	$scope.working = true;
	$scope.isTabActive = true;

	GlobalVariablesService.Scope_ItemTab_Locations = $scope;

	initGrid();

	$scope.addTransferClicked = function()
	{
		var modalInstance = $modal.open({
				controller: 'AddTransferDialogCtrl',
				templateUrl: 'views/Manage/dialogs/AddTransferDialog.html',
				size: 'lg',
				resolve: {
					item_id: function () {
						return $scope.item_id;
					}
					,items: function ()
					{
						return null;
					}
				}
			});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
				//loadData();
			}
		});
	}

	$scope.addCheckInClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCheckInDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
				,items: function ()
					{
						return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
			}
		});
	}

	$scope.addCheckOutClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCheckOutDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
				,items: function ()
					{
						return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				GlobalVariablesService.Scope_ItemTab_InOutEntries.loadData();
			}
		});
	}


	$scope.init = function(item_id)
	{
		$scope.item_id = item_id;
		$scope.loadData();
	}

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};

	$scope.loadData = function()
	{
		$scope.working = true;
		var promise = ApiService.ItemsAPI.getItemLocations({item_id:$scope.item_id}).$promise;

		promise.then(function(result)
		{
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;
			$scope.working = false;
			GUI.Grid.SetGridHeight();

			setTimeout((function()
			{
				GUI.Grid.SetGridHeight();
			}), 100);


		},
		function(err)
		{
			GUI.Grid.SetGridHeight();
		});
	}

	$scope.itemClicked = function(row)
	{
		if ($scope.user.perm_inout_view == true || $scope.user.perm_inout_view ==1)
		{
			var id = row.entity.location_id;
			var name = row.entity.location_name;

			GlobalVariablesService.Scope_ItemTab_Details.activateInOutEntriesTab();
			GlobalVariablesService.Scope_ItemTab_InOutEntries.setLocationFilter(name);
		}

	}

	$scope.exportToCsv = function(rowType)
	{
		var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
		$scope.gridApi.exporter.csvExport( rowType, 'all' , myElement);
	}

	$scope.refreshClicked = function()
	{
		$scope.loadData();
	}

	function initGrid()
	{
		$scope.gridOptions = {

			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,

			exporterCsvFilename: 'item_locations.csv',
			exporterOlderExcelCompatibility:true,

			exporterPdfDefaultStyle: {fontSize: 9},
			exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
			exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
			exporterPdfHeader: { text: "Item locations", style: 'headerStyle' },
			exporterPdfFooter: function ( currentPage, pageCount ) {
				return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			},
			exporterPdfCustomFormatter: function ( docDefinition ) {
				docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
				docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
				return docDefinition;
			},
			exporterPdfOrientation: 'portrait',
			exporterPdfPageSize: 'LETTER',
			exporterPdfMaxGridWidth: 500,
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),


			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;

				// gridApi.selection.on.rowSelectionChanged($scope,function(row)
				// {
				// });

				// gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				// {
				// });
			}
		};

		$scope.gridOptions.columnDefs =
		[
		,{
			field: 'location_name'
			,displayName:gettextCatalog.getString("Location", null, "Context Grid Columns")
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
			, enableHiding:true
		}
		,{
			field: 'count'
			,displayName:'Count'
			, enableHiding:true
		}

		];
	}


}]);

