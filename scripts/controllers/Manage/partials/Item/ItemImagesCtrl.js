'use strict';

angular.module('BarcoderyApp')
.controller('ItemImagesCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', '$timeout',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, $timeout)
{
	$scope.alerts = [];
	$scope.isTabActive = true;
	
	GlobalVariablesService.Scope_ItemTab_Images = $scope;
	
	initGrid();
	
	$scope.init = function(item_id)
	{
		$scope.item_id = item_id;
		loadData();
	}
	
	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}
	
	$scope.addImageClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCheckInDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
			}
		});
		
		modalInstance.result.then(function (answer) 
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				loadData();
			}
		});
	}
	
	$scope.closeAlert = function(index) 
	{
		$scope.alerts.splice(index, 1);
	};
	

	function loadData()
	{
		$scope.working = true;
		var promise = ApiService.ItemsAPI.getItemInOutEntries({item_id:$scope.item_id}).$promise;
		
		promise.then(function(result) 
		{
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;			
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		}, 
		function(err) 
		{
			GUI.Grid.SetGridHeight();
		});	
	}
	
	function initGrid()
	{
		$scope.gridOptions = {
			
			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,
			
			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;
				
				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
				
				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});						
			}
		};		
		
		$scope.gridOptions.columnDefs = 
		[
		// { 
		// field: 'direction'
		// , enableHiding:true
		// // , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'  
		// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'  
		// }
		,{ 
			field: 'type'
			,displayName:'Type'
			, enableHiding:true 
		}	
		,{ 
			field: 'location_name'
			,displayName:'Location'
			, enableHiding:true 
		}
		,{ 
			field: 'count_text'
			,displayName:'Count'
			, enableHiding:true 
			// , cellTemplate: '<div class="ui-grid-cell-contents" >{{ grid.getCellValue(row, col)}} </div>'  
		}
		,{ 
			field: 'note'
			, enableHiding:true 
		}
		,{ 
			field: 'timestamp'
			,displayName:'Created'
			, enableHiding:true 
		}		
		,{ 
			field: 'user_name'
			,displayName:'User'
			, enableHiding:true 
		}		
		];		
	}	
	
	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		
		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}
	
	function enableDisableEditButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		
		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}	
}]);

