'use strict';

angular.module('BarcoderyApp')
.controller('ItemInOutEntriesCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', '$timeout', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, $timeout, gettextCatalog)
{
	$scope.HelperService = HelperService;

	$scope.isDeleteButtonDisabled = true;
	$scope.isEditButtonEnabled = false;
	$scope.deleteButtonTooltip = '';
	$scope.alerts = [];
	$scope.isTabActive = true;

	GlobalVariablesService.Scope_ItemTab_InOutEntries = $scope;

	initGrid();

	$scope.init = function(item_id)
	{
		$scope.item_id = item_id;
		$scope.loadData();
	}

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.addCheckInClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCheckInDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
				,items: function ()
					{
						return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();

				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				$scope.loadData();
			}
		});
	}

	$scope.addCheckOutClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCheckOutDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCheckInOutDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
				,items: function ()
					{
						return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				$scope.loadData();
			}
		});
	}

	$scope.addTransferClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddTransferDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddTransferDialog.html',
			size: 'lg',
			resolve: {
				item_id: function () {
					return $scope.item_id;
				}
				,items: function ()
					{
						return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				GlobalVariablesService.Scope_ItemTab_Details.loadData();
				GlobalVariablesService.Scope_ItemTab_Locations.loadData();
				$scope.loadData();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var item = rows[0].entity;

		var modalInstance = $modal.open({
			controller: 'EditCategoryDialogCtrl',
			templateUrl: 'views/Manage/EditCategoryDialog.html',
			size: 'lg',
			resolve: {
				item: function ()
				{
					return item;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				$scope.loadData();
			}
		});
	}

	$scope.deleteButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var ids = [];

		for(var i in rows)
		{
			ids.push(rows[i].entity.entry_id);
		}

		$scope.working = true;
		ApiService.ItemsAPI.deleteInOutEntries({ids:JSON.stringify(ids)}).$promise
		.then(function(result)
		{
			$scope.isDeleteButtonDisabled = true;
			CacheService.SetItemsDirty();
			GlobalVariablesService.Scope_ItemTab_Details.loadData();
			GlobalVariablesService.Scope_ItemTab_Locations.loadData();
			$scope.loadData();
		},
		function(err)
		{
			$scope.working = false;
		});
	}

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.itemClicked = function(row)
	{
		var name = row.entity.name;
		GlobalVariablesService.ItemsCategoriesFilter = name;
		$scope.redirect('/items');
	}

	$scope.setLocationFilter = function(text)
	{
		setTimeout((function()
		{
			var col = _.findWhere($scope.gridApi.grid.columns, {field: "location_name"});
			col.filters[0].term = text;

		}), 0);

	}

	$scope.exportToCsv = function(rowType)
	{
		var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
		$scope.gridApi.exporter.csvExport( rowType, 'all' , myElement);
	}

	$scope.refreshClicked = function()
	{
		$scope.loadData();
	}

	$scope.loadData = function()
	{
		$scope.working = true;
		var in_out_entries_promise = ApiService.ItemsAPI.getItemInOutEntries({item_id:$scope.item_id}).$promise;

		var account_promise = ApiService.MyAccountAPI.getAccounts().$promise;

        account_promise.then(function(result)
        {
            $scope.account = result[0];
            HelperService.set_date_format($scope.account.date_format);

            return in_out_entries_promise;
        },
        function(err)
        {

        })
        .then(function(result)
		{
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		},
		function(err)
		{
			GUI.Grid.SetGridHeight();
		});
	}

	$scope.getTranslatedType = function(type_text)
	{
		gettextCatalog.getString("IN", 	null, "Context In Out Entry Type");
		gettextCatalog.getString("OUT",  null, "Context In Out Entry Type");

		var translated = gettextCatalog.getString(type_text,  null, "Context In Out Entry Type");
		return translated;
	}

	function initGrid()
	{
		$scope.gridOptions = {

			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,

			exporterCsvFilename: 'in_out_entires.csv',
			exporterOlderExcelCompatibility:true,
			exporterPdfDefaultStyle: {fontSize: 9},
			exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
			exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
			exporterPdfHeader: { text: "In Out entries", style: 'headerStyle' },
			exporterPdfFooter: function ( currentPage, pageCount ) {
				return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			},
			exporterPdfCustomFormatter: function ( docDefinition ) {
				docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
				docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
				return docDefinition;
			},
			exporterPdfOrientation: 'portrait',
			exporterPdfPageSize: 'LETTER',
			exporterPdfMaxGridWidth: 500,
			exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),



			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
			}
		};

		$scope.gridOptions.columnDefs =
		[
		// {
		// field: 'direction'
		// , enableHiding:true
		// // , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'
		// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
		// }
		,{
			field: 'type'
			,displayName:gettextCatalog.getString("Type", null, "Context Grid Columns")
			,enableHiding:true
			,cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.getTranslatedType(COL_FIELD) }} </div>'
		}
		,{
			field: 'location_name'
			,displayName:gettextCatalog.getString("Location", null, "Context Grid Columns")
			,enableHiding:true
		}
		,{
			field: 'count_text'
			,displayName:gettextCatalog.getString("Count", null, "Context Grid Columns")
			,enableHiding:true
			,sortingAlgorithm: Helpers.Sorting.Numeric
			// , cellTemplate: '<div class="ui-grid-cell-contents" >{{ grid.getCellValue(row, col)}} </div>'
		}
		,{
			field: 'note'
			,displayName:gettextCatalog.getString("Note", null, "Context Grid Columns")
			, enableHiding:true
		}
		,{
			field: 'timestamp'
			// ,displayName:'Created'
			,displayName:gettextCatalog.getString("Created", null, "Context Grid Columns")
			,enableHiding:true
			,sortingAlgorithm: Helpers.Sorting.Date
			,cellTemplate: '<div class="ui-grid-cell-contents" > {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </div>'
		}
		,{
			field: 'user_name'
			// ,displayName:'User'
			,displayName:gettextCatalog.getString("User", null, "Context Grid Columns")
			,enableHiding:true
		}
		];
	}

	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}

	function enableDisableEditButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}
}]);

