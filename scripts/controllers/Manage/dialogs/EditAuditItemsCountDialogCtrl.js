'use strict';

angular.module('BarcoderyApp')
.controller('EditAuditItemsCountDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'audit_items', 'gettextCatalog',
									 function ($scope, $location,  ApiService, $modalInstance, audit_items, gettextCatalog)
{
	$scope.working = false;

	init_count();


	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
        var audit_item_ids = _.map(audit_items, function(audit_item){ return audit_item.audit_item_id; });

		$scope.working = true;

		ApiService.AuditAPI.set_audit_items_count({ids:JSON.stringify(audit_item_ids), count: $scope.count }).$promise
		.then(function(result)
		{
			$scope.working = false;
			$modalInstance.close(true);
		},
		function(err)
		{
			$scope.working = false;
			$scope.validationErrors = err.data.error_message;
		});
	};


	function init_count()
	{
		$scope.count = 0;

		var counts = _.map(audit_items, function(audit){ return audit.count; });
		if (counts.length > 0)
		{
			var count = counts[0];
			$scope.count = count;

			for (var i = 0; i < counts.length; i++)
			{
				if (counts[i] != count)
				{
					$scope.count = 0;
				}
			};
		}
	}

	function validate()
	{
		if ($scope.item.name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		return true;
	}

}]);
