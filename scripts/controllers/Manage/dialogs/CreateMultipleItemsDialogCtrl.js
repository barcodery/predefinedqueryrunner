'use strict';

angular.module('BarcoderyApp')
.controller('CreateMultipleItemsDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'gettextCatalog',
									 function ($scope, $location,  ApiService, $modalInstance,  gettextCatalog)
{
	$scope.working = false;
	$scope.validationErrors = '';

	$scope.count = 10;


	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		$scope.working = true;

		ApiService.ItemsAPI.createMultipleItems({count: $scope.count }).$promise
		.then(function(result)
		{
			$scope.working = false;
			$modalInstance.close(true);
		},
		function(err)
		{
			$scope.working = false;
			$scope.validationErrors = err.data.error_message;
		});
	};

	function validate()
	{
		if ($scope.item.name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		return true;
	}

}]);
