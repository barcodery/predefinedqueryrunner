'use strict';

angular.module('BarcoderyApp')
.controller('AddTagDialogCtrl', ['$scope', '$location',  'ApiService', '$modalInstance', 'gettextCatalog',
						function ($scope, $location,  ApiService, $modalInstance, gettextCatalog)
{
	$scope.inputFields = {Name:''};
	$scope.working = false;
	$scope.validationErrors = '';

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			ApiService.TagsAPI.add({name:$scope.inputFields.Name}).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
				},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function validate()
	{
		if ($scope.inputFields.Name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		return true;
	}
}]);
