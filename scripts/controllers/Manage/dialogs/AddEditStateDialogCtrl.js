'use strict';

angular.module('BarcoderyApp')
.controller('AddEditStateDialogCtrl', ['$scope', '$location',  'ApiService', '$modalInstance', 'gettextCatalog', 'item',
						function ($scope, $location,  ApiService, $modalInstance, gettextCatalog, item)
{
	$scope.is_editing = item != null;

	$scope.inputFields = {Name:''};
	$scope.working = false;
	$scope.validationErrors = '';
	$scope.selectedColor = "#FFFFFF";

	if (item != null)
	{
		$scope.inputFields.Name = item.name;
		$scope.selectedColor = item.color;
	}

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			var promise = null;

			if (item != null)
			{
				promise = ApiService.StatesAPI.edit({state_id: item.state_id, name:$scope.inputFields.Name, color:$scope.selectedColor}).$promise;
			}
			else
			{
				promise = ApiService.StatesAPI.add({name:$scope.inputFields.Name, color:$scope.selectedColor}).$promise;
			}

			promise.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
				},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function validate()
	{
		if ($scope.inputFields.Name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		return true;
	}
}]);
