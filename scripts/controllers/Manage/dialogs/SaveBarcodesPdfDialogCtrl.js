'use strict';

angular.module('BarcoderyApp')
.controller('SaveBarcodesPdfDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'item', 'items', 'CacheService', 'gettextCatalog',
                            function ($scope, $location,  ApiService, $modalInstance, item, items, CacheService, gettextCatalog)
{
    // $scope.working = false;
    var qr_code_text = "QR Code";
    init();

    $scope.selectionChanged = function()
    {
        var _item = get_one_item();

        for (var i = 0; i < $scope.cfn.length; i++) {
            $scope.cfn[i].ticked = false;
        }

        $scope.selectedCfn.ticked = true;

        $scope.barcode_value = get_custom_field_value(_item);

        if ($scope.selectedBarcodeType.name == qr_code_text)
        {
            var qrImageData = qr.toDataURL({value:$scope.barcode_value, size:8});

            $("#barcode_img").attr('src',qrImageData);

            $scope.is_barcode_invalid = false;
        }
        else
        {
            JsBarcode($("#barcode_img"), $scope.barcode_value, {format:get_selected_barcode_type() ,displayValue:true,fontSize:20},
             function(valid)
             {
                if(valid)
                {
                    $scope.is_barcode_invalid = false;
                }
                else
                {
                    $scope.is_barcode_invalid = true;
                }
             }
            );
        }
    }

    $scope.cancelClicked = function()
    {
        $modalInstance.dismiss('cancel');
    }

    $scope.saveClicked = function()
    {
        $scope.validationErrors = '';
        $scope.working = true;
        if (items == null)
        {
            generate_pdf_for_one_item();
        }
        else
        {
            generate_pdf_for_multiple_items();
        }

        $scope.working = false;
    };


    function generate_pdf_for_one_item()
    {
        var doc = new jsPDF();

        var a4Width = 210;
        var a4Height = 297;

        var margin = 10;
        var columns = $scope.selectedColumnsCount;

        var canvas = document.createElement('canvas');
        var barcodeHeight = 100;

        if ($scope.selectedBarcodeType.name == qr_code_text)
        {
            canvas = qr.canvas({value:$scope.barcode_value, size:10});
            barcodePdfWidth = Math.floor(a4Width/ columns);
            barcodePdfHeight = barcodePdfWidth; // barcodePdfWidth-8;
        }
        else
        {
            JsBarcode(canvas, $scope.barcode_value, {format:get_selected_barcode_type() ,displayValue:true,fontSize:20, height: barcodeHeight});

            var barcodeWidth = canvas.width;
            barcodeHeight = canvas.height;
            var barcodePdfWidth = Math.floor((a4Width - margin/2 )/ columns);
            var barcodePdfHeight =Math.floor( barcodeHeight/ (barcodeWidth / barcodePdfWidth )) ;
            // barcodePdfHeight = 25;
        }

        var barcodeImgDataBase64 = canvas.toDataURL();

        var rows = Math.floor(a4Height  / barcodePdfHeight);

        for (var col = 0; col < columns; col++)
        {
            for (var row = 0; row < rows; row++)
            {
                var x = col * (barcodePdfWidth);
                var y = row * (barcodePdfHeight);

                var width = barcodePdfWidth-margin/2    ;
                var height = barcodePdfHeight - margin/2;

                if (height <=0) height = barcodePdfHeight;
                if (width <=0) width = barcodePdfWidth;

                doc.addImage(barcodeImgDataBase64, x + margin/2, y + margin, width, height);

                if ($scope.selectedBarcodeType.name == qr_code_text)
                {
                    var _item = get_one_item();
                    var labelToPrint = get_barcode_label_to_be_printed_below_qr_code(_item);
                    doc.setFontSize(12);
                    doc.text(x + margin/2 ,  y + margin + height +1, labelToPrint);
                }
            }
        }

        doc.save('Barcode_'+$scope.selectedBarcodeType.name+'_' +$scope.barcode_value +'.pdf' );
    }

    function generate_pdf_for_multiple_items()
    {
        var doc = new jsPDF();

        var a4Width = 210;
        var a4Height = 297;

        var margin = 10;
        var columns = $scope.selectedColumnsCount;

        var canvas = document.createElement('canvas');

        // ku kazdemu itemu sa vygeneruje jeden ciarovy kod
        var col = 0;
        var row = 0;

        for (var i = 0; i < items.length; i++)
        {
            var _item = items[i];

            // generate image
            var barcode_value = get_custom_field_value(_item);

			if (!barcode_value) barcode_value = '';

            // if (!barcode_value)
            // {
            //     $scope.validationErrors = gettextCatalog.getString("Not all items have value for selected field. Canceled", null, "PDF Generation");
            //     return;
            // }

            var barcodeHeight = 100;

            if ($scope.selectedBarcodeType.name == qr_code_text)
            {
                canvas = qr.canvas({value:barcode_value, size:10});
                barcodePdfWidth = Math.floor(a4Width/ columns);
                barcodePdfHeight = barcodePdfWidth; // barcodePdfWidth-8;
            }
            else
            {
            	var is_valid = false;
                JsBarcode(canvas, barcode_value, {format:get_selected_barcode_type() ,displayValue:true,fontSize:20, height: barcodeHeight},
                 function(valid)
	             {
	             	is_valid = valid;
	                if(!valid)
	                {
	                    $scope.validationErrors = gettextCatalog.getString("Not valid barcode. Canceled.", null, "PDF Generation");
	                	return;
	                }
	             }
            	);

            	if (!is_valid) return;

         // JsBarcode($("#barcode_img"), $scope.barcode_value, {format:$scope.selectedBarcodeType.name ,displayValue:true,fontSize:20},
         //     function(valid)
         //     {
         //        if(valid)
         //        {
         //            $scope.is_barcode_invalid = false;
         //        }
         //        else
         //        {
         //            $scope.is_barcode_invalid = true;
         //        }
         //     }
         //    );

                var barcodeWidth = canvas.width;
                barcodeHeight = canvas.height;
                var barcodePdfWidth = Math.floor((a4Width - margin/2 )/ columns);
                var barcodePdfHeight =Math.floor( barcodeHeight/ (barcodeWidth / barcodePdfWidth )) ;
                // barcodePdfHeight = 25;
            }

            var barcodeImgDataBase64 = canvas.toDataURL();


            var width = barcodePdfWidth-margin/2;
            var height = barcodePdfHeight - margin/2;

            if (height <=0) height = barcodePdfHeight;
            if (width <=0) width = barcodePdfWidth;

            var x = col * (barcodePdfWidth);
            var y = row * (barcodePdfHeight);

            var pageHeight= doc.internal.pageSize.height;
            var add = $scope.selectedBarcodeType.name == qr_code_text ? 2 : 0;
            if (y >= pageHeight || y + margin +barcodePdfHeight + add >= pageHeight )
            {
                row = 0;
                col = 0;
                doc.addPage();

                x = col * (barcodePdfWidth);
                y = row * (barcodePdfHeight);
            }


            doc.addImage(barcodeImgDataBase64, x + margin/2, y + margin, width, height);

            if ($scope.selectedBarcodeType.name == qr_code_text)
            {
                var labelToPrint = get_barcode_label_to_be_printed_below_qr_code(_item);
                doc.setFontSize(12);
                doc.text(x + margin/2 ,  y + margin + height +1, labelToPrint);
            }

            col++;

            if (col >= columns)
            {
                col = 0;
                row++;
            }

            // var pageHeight= doc.internal.pageSize.height;
            // if (y >= pageHeight || y + margin +barcodePdfHeight + 20>= pageHeight )
            // {
            //  row = 0;
            //      col = 0;
            //      doc.addPage();
            // }
        }

        // doc.save('Barcode'+$scope.selectedBarcodeType.name+'_' +$scope.barcode_value +'.pdf' );
        // doc.save($scope.selectedBarcodeType.name+'.pdf' );
        doc.save('Barcodes.pdf' );
    }


    function init()
    {
        $scope.item = get_one_item();

        $scope.working = false;

        $scope.selectedColumnsCount = 3;

        $scope.barcode_types = [{name:qr_code_text}, {name:"CODE128"}, {name:"EAN"}, {name:"UPC-A"}, {name:"CODE39"}, {name:"ITF"}, {name:"ITF14"}, {name:"Pharmacode"}];
        $scope.selectedBarcodeType = $scope.barcode_types[0];

        var customFieldNamesPromise = CacheService.GetCustomFieldNames();
        customFieldNamesPromise.then(function(result)
        {
            $scope.cfn = result;
            $scope.selectedCfn = result[0];
            $scope.selectionChanged();
        },
        function(err)
        {
        });
    }

    // function get_custom_field_value()
    // {
    //  var id = $scope.selectedCfn.custom_field_name_id;
    //  var value = item["custom_field_id_"+id];
    //  return value;
    // }

    function get_custom_field_value(_item)
    {
        var id = $scope.selectedCfn.custom_field_name_id;
        var value = _item["custom_field_id_"+id];
        return value;
    }

    function get_one_item()
    {
        var _item = items == null ? item : items[0];
        return _item;
    }

    function get_selected_barcode_type()
    {
         var barcode_type = $scope.selectedBarcodeType.name;

        if (barcode_type == 'UPC-A')
        {
            barcode_type = 'UPC';
        }

        return barcode_type;
    }

    function get_barcode_label_to_be_printed_below_qr_code(_item)
    {
        if ($scope.selectedBarcodeType.name == qr_code_text)
        {
            // ak sa jedna o qr kod, tak hodnotu vezmeme tu co je vyznacena v label
             var selectedLabels = _.where($scope.cfn,
                {
                    ticked: true
                });

             var result = "";
             for (var i = 0; i < selectedLabels.length; i++)
             {
                var id = selectedLabels[i].custom_field_name_id;
                var value = _item["custom_field_id_"+id];

                result += (i == 0) ? value : ";" + value;
             }

             return result;
        }
        else
        {
            return get_custom_field_value(_item);
        }
    }


}]);
