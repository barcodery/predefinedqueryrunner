'use strict';

angular.module('BarcoderyApp')
.controller('AddEditItemDialogCtrl', ['$scope', '$http', 'Upload', '$location', 'ApiService', 'HelperService', 'CacheService', '$modal', '$routeParams', 'GlobalVariablesService', 'SettingsService', '$modalInstance','selected_items',
function($scope, $http, Upload, $location, ApiService, HelperService, CacheService, $modal, $routeParams, GlobalVariablesService, SettingsService, $modalInstance, selected_items)
{
	$scope.item = {};
    // $scope.item.title = '';
    // $scope.item.note = '';
    // $scope.item.description = '';
    $scope.categories = [];
    $scope.tags = [];
    $scope.validation_error = '';
    $scope.working = false;
    $scope.alerts = [];
    $scope.editing_item = false;
    $scope.edit_field_enabled = {}
    $scope.images_object_url_and_id = [];

    $scope.onlyNumbers = /^\d+$/;

    var images_count_to_add = 0;

    if (selected_items)
    {
    	$scope.editing_item_one = selected_items.length == 1;
        $scope.editing_item_multiple = selected_items.length > 1;
	}
    else
    {
        $scope.adding_item = true;
    }

    init();

    $scope.cancelClicked = function()
    {
        $modalInstance.dismiss('cancel');
    }

    $scope.redirect = function(path, ev)
    {
        // ev.preventDefault();
        $location.path(path);
	}

    $scope.saveItemButtonClicked = function()
    {
    	var isValid = validate();

    	if (!isValid)
    	{
    		return;
		}

    	$scope.working = true;

		var item_to_save = {};
        var values = {};

        if ($scope.adding_item || $scope.editing_item_one || ($scope.editing_item_multiple && $scope.edit_categories_enabled)) item_to_save.categories_ids = JSON.stringify(getSelectedCategoriesId());
        if ($scope.adding_item || $scope.editing_item_one || ($scope.editing_item_multiple && $scope.edit_tags_enabled))       item_to_save.tags_ids = JSON.stringify(getSelectedTagsId());
        if ($scope.adding_item || $scope.editing_item_one || ($scope.editing_item_multiple && $scope.edit_states_enabled))     item_to_save.states_ids = JSON.stringify(getSelectedStatesId());

		// {
		// 	//values: $scope.item,
		// 	categories_ids: JSON.stringify(getSelectedCategoriesId()),
		// 	tags_ids: JSON.stringify(getSelectedTagsId()),

		// };

		if (selected_items && selected_items.length > 0)
		{
            var item_ids = _.map(selected_items, function(item){ return item.item_id; });
			item_to_save.item_ids = JSON.stringify(item_ids);
		}

        var image_cfns =  $scope.get_custom_fields_by_type(8);
        var images_cfn_ids = _.map(image_cfns, function(x){ return x.custom_field_name_id; });

		for (var n in $scope.item)
		{
			var index = n.indexOf('custom_field_id_');
            var index_od_image_url = n.indexOf("_image_url");

			if (index > -1 && index_od_image_url == -1)
			{
                var cfn_id = n.substring('custom_field_id_'.length, n.length);

                var is_image_value = _.contains(images_cfn_ids, cfn_id);
                // if (is_image_value)
                // {
                //     continue;
                // }

                if ($scope.adding_item || $scope.editing_item_one || ($scope.editing_item_multiple && $scope.edit_field_enabled[cfn_id]))
                {
                    values[cfn_id] = $scope.item[n];
                    if(typeof $scope.item[n] === 'undefined')
                    {
                        values[cfn_id] = null;
                    }
                }
			}
		}

        item_to_save.values = JSON.stringify(values);


        var files_to_send = {};

        for (var i = 0; i < images_cfn_ids.length; i++)
        {
            var image_cfn_id = images_cfn_ids[i];

            var file_values = $scope.item['image_field_id_' + image_cfn_id];
            var files = file_values.flow.files;

            for (var j = 0; j < files.length; j++)
            {
                var flowFile = files[j]
                files_to_send['image_cfn_id_'+image_cfn_id+'_' + j] = flowFile.file;
            };
        };

        var url = ApiService.get_api_base_url() + "/add_edit_item_fd";

        Upload.upload(
        {
            url: url,
            data: {files: files_to_send, 'item_data': JSON.stringify(item_to_save)}
        })
        .then(function (resp)
        {
            $scope.working = false;
            CacheService.SetItemsDirty();
            $modalInstance.close(true);
        },
        function (err)
        {
            $scope.alerts.push(
            {
                type: 'danger',
                msg: err.data.error_message
            });
            $scope.working = false;
        },
        function (evt)
        {
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
	}

	$scope.createNewCategoryClicked = function()
	{
		var modalInstance = $modal.open(
		{
			controller: 'AddCategoryDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCategoryDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function(answer)
		{
			if (answer)
			{
				var selectedCategories = _.where($scope.categories,
				{
					ticked: true
				});

				CacheService.SetCategoriesDirty();
				var promise = getCategroies();

				promise.then(function(result)
				{
					var last_item = _.last($scope.categories);
					last_item.ticked = true;

					for (var i in selectedCategories)
					{
						var item = _.findWhere($scope.categories,
						{
							category_id: selectedCategories[i].category_id
						});
						item.ticked = true;
					}
				});
			}
		});
	}

	$scope.createNewTagClicked = function()
	{
		var modalInstance = $modal.open(
		{
			controller: 'AddTagDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddTagDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function(answer)
		{
			if (answer)
			{
				var selectedTags = _.where($scope.tags,
				{
					ticked: true
				});

				CacheService.SetTagsDirty();
				var promise = getTags(true);

				promise.then(function(result)
				{
					var last_item = _.last($scope.tags);
					last_item.ticked = true;

					for (var i in selectedTags)
					{
						var item = _.findWhere($scope.tags,
						{
							tag_id: selectedTags[i].tag_id
						});
						item.ticked = true;
					}
				});
			}
		});
	}

    $scope.createNewStateClicked = function()
    {
        var modalInstance = $modal.open(
        {
            controller: 'AddEditStateDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddEditStateDialog.html',
            size: 'sm',
            resolve:
            {
                item: function ()
                {
                    return null;
                }
            }
        });

        modalInstance.result.then(function(answer)
        {
            if (answer)
            {
                var selectedStates = _.where($scope.states,
                {
                    ticked: true
                });

                CacheService.SetStatesDirty();
                var promise = getStates(true);

                promise.then(function(result)
                {
                    var last_item = _.first($scope.states);
                    last_item.ticked = true;

                    for (var i in selectedStates)
                    {
                        var item = _.findWhere($scope.states,
                        {
                            tag_id: selectedStates[i].state_id
                        });
                        item.ticked = true;
                    }
                });
            }
        });
    }

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.get_custom_fields_by_type = function(type)
	{
		var result = [];

		if ($scope.custom_filed_names)
		{
			for (var i = 0; i < $scope.custom_filed_names.length; i++)
			{
				if ($scope.custom_filed_names[i].type == type)
				{
					result.push($scope.custom_filed_names[i]);
				}
			}
		}

		return result;
	}

    $scope.get_custom_fields_by_types = function(types_array)
    {
        var result = [];

        if ($scope.custom_filed_names)
        {
            for (var i = 0; i < $scope.custom_filed_names.length; i++)
            {
                if (_.contains(types_array, $scope.custom_filed_names[i].type))
                {
                    result.push($scope.custom_filed_names[i]);
                }
            }
        }

        return result;
    }

	$scope.on_controller_init = function()
	{
		GUI.InitNumberOnlyInputs();
	}

    $scope.remove_existing_image = function(cfn, image_id)
    {
        var cfn_id = cfn.custom_field_name_id;

        var field_url = "custom_field_id_" + cfn_id + "_image_url";
        var field_id  = "custom_field_id_" + cfn_id;

        var urls = $scope.item[field_url];
        var ids  = $scope.item[field_id];

        var splitted_urls = [];
        var splitted_ids  = [];

        if (urls) splitted_urls = urls.split(",");
        if (ids) splitted_ids   = ids.split(",");

        for (var i = 0; i < splitted_ids.length; i++)
        {
            var id  = splitted_ids[i];
            var url = splitted_urls[i];

            if (id == image_id)
            {
                splitted_ids = _.without(splitted_ids, image_id);
                splitted_urls = _.without(splitted_urls, url);
                break;
            }
        };

        $scope.item[field_id] = splitted_ids.join(',');
        $scope.item[field_url] = splitted_urls.join(',');

        load_existing_images_into_var(cfn);
    }

    $scope.flowImageAddedEvent = function($file, $event, $flow, totalFilesCount)
    {
        event.preventDefault();

        if (!($file.getExtension() == "png" || $file.getExtension() == "gif" || $file.getExtension() == "jpg" || $file.getExtension() == "jpeg"))
        {
        return false;
        }

        if (totalFilesCount + images_count_to_add >= $scope.AppSettings.AllowedImagesPerImageField )
        {
            return false;
        }

        images_count_to_add ++;

        return true;
    }

    $scope.flowImagesAddedEvent = function($file, $event, $flow, totalFilesCount)
    {
        images_count_to_add = 0;
        return true;
    }

	// private

	function getSelectedCategoriesId()
	{
		var result = [];

		for (var i in $scope.categories)
		{
			if ($scope.categories[i].ticked)
			{
				result.push($scope.categories[i].category_id);
			}
		}

		return result;
	}

	function getSelectedTagsId()
	{
		var result = [];

		for (var i in $scope.tags)
		{
			if ($scope.tags[i].ticked)
			{
				result.push($scope.tags[i].tag_id);
			}
		}

		return result;
	}

    function getSelectedStatesId()
    {
        var result = [];

        for (var i in $scope.states)
        {
            if ($scope.states[i].ticked)
            {
                result.push($scope.states[i].state_id);
            }
        }

        return result;
    }

    function loadCustomFieldNames()
    {
        // ApiService.CustomFieldNamesAPI.loadCustomFieldNames().$promise
        var promise = CacheService.GetCustomFieldNames();
        promise.then(function(result)
        {
        	$scope.custom_filed_names = result;
        	init_item_default_valuse();
		},
        function(err)
        {
        	$scope.custom_filed_names = [];
		});
	}

    function getCategroies()
    {
    	var promise = CacheService.GetCategories();
    	promise.then(function(result)
    	{
    		$scope.categories = result;

    		_.each($scope.categories, function(item)
    		{
    			item.ticked = false;
			});
		},
    	function(err) {});

    	return promise;
	}

    function getTags()
    {
    	var promise = CacheService.GetTags();
    	promise.then(function(result)
    	{
    		$scope.tags = result;

    		_.each($scope.tags, function(item)
    		{
    			item.ticked = false;
			});
		},
    	function(err) {});

    	return promise;
	}

    function getStates()
    {
        var promise = CacheService.GetStates();
        promise.then(function(result)
        {
            $scope.states = result;

            _.each($scope.states, function(item)
            {
                item.ticked = false;
            });
        },
        function(err) {});

        return promise;
    }

    function init()
    {
    	loadCustomFieldNames();
    	var account_promise = load_account();

    	account_promise.then(function(result)
    	{
    		var promisCat = getCategroies();
    		var promiseTag = getTags();
            var promiseStates = getStates();

    		if (selected_items)
    		{
    			promisCat.then(function(result)
    			{
    				return promiseTag;
				})
                .then(function(result)
                {
                    return promiseStates;
                })
    			.then(function(result)
    			{
    				loadExistingItem();
				});
			}
		});

        getAppSettings();
	}

    function init_item_default_valuse()
    {
    	$scope.item = {};

    	for (var i = 0; i < $scope.custom_filed_names.length; i++)
    	{
    		var cfn = $scope.custom_filed_names[i];
    		var value = '';

            if (cfn.type == 3) // bit value
            {
            	value = 0;
			}

            $scope.item['custom_field_id_' + cfn.custom_field_name_id] = value;
		};
	}

    function loadExistingItem()
    {
        if (!selected_items)
        {
            return;
        }

		// $scope.item = selected_items[0];


        // get intersected categories
        var array = [];
        _.each(selected_items, function (item) { if (item.categories_ids) array.push(item.categories_ids.split(',')); else array.push(0);  } );
        var categories_ids_to_load = _.intersection.apply(_, array);

        // load intersected categories
		for (var i in $scope.categories)
		{
			for (var j in categories_ids_to_load)
			{
				if ($scope.categories[i].category_id == categories_ids_to_load[j])
				{
					$scope.categories[i].ticked = true;
				}
			}
		}

        // load intersected tags
        var array = [];
        _.each(selected_items, function (item) {  if (item.tags_ids) array.push(item.tags_ids.split(',')); else array.push(0); } );
        var tags_ids_to_load = _.intersection.apply(_, array);

		// load tags from item
		for (var i in $scope.tags)
		{
			for (var j in tags_ids_to_load)
			{
				if ($scope.tags[i].tag_id == tags_ids_to_load[j])
				{
					$scope.tags[i].ticked = true;
				}
			}
		}

        // load states
        var array = [];
        _.each(selected_items, function (item) {  if (item.states_ids) array.push(item.states_ids.split(',')); else array.push(0); } );
        var states_ids_to_load = _.intersection.apply(_, array);

        for (var i in $scope.states)
        {
            for (var j in states_ids_to_load)
            {
                if ($scope.states[i].state_id == states_ids_to_load[j])
                {
                    $scope.states[i].ticked = true;
                }
            }
        }

		// format date values to date
		var date_cfn = $scope.get_custom_fields_by_type(4);
		for (var i = 0; i < date_cfn.length; i++)
		{
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + date_cfn[i].custom_field_name_id]] ); } );
            var intersected_dates = _.intersection.apply(_, array);

            if (!intersected_dates || intersected_dates.length <= 0) continue;

            if (intersected_dates[0] != null)
            {
                var t = intersected_dates[0].split(/[- :]/);
    			// var t = $scope.item['custom_field_id_' + date_cfn[i].custom_field_name_id].split(/[- :]/);

    			$scope.item['custom_field_id_' + date_cfn[i].custom_field_name_id] = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
            }
		}

        var bit_cfn = $scope.get_custom_fields_by_type(3);
        for (var i = 0; i < bit_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + bit_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            if (intersected_values[0] == "1")
            {
                $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 1;
            }
            else
            {
                $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 0;
            }

            // if ($scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] == "1")
            // {
            //     $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 1;
            // }
            // else
            // {
            //     $scope.item['custom_field_id_' + bit_cfn[i].custom_field_name_id] = 0;
            // }
        }


         // text, number/ barcode values
        var other_cfn = $scope.get_custom_fields_by_types(["1","2","5","6","7","9"]);
        for (var i = 0; i < other_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + other_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + other_cfn[i].custom_field_name_id] = intersected_values[0];
        }


        var images_cfn = $scope.get_custom_fields_by_type(8);
        for (var i = 0; i < images_cfn.length; i++)
        {
            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + images_cfn[i].custom_field_name_id +"_image_url" ] ] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + images_cfn[i].custom_field_name_id +"_image_url"] = intersected_values[0];

            var array = [];
            _.each(selected_items, function (item) { array.push([ item['custom_field_id_' + images_cfn[i].custom_field_name_id]] ); } );
            var intersected_values = _.intersection.apply(_, array);
            if (!intersected_values || intersected_values.length <= 0) continue;

            $scope.item['custom_field_id_' + images_cfn[i].custom_field_name_id] = intersected_values[0];

            load_existing_images_into_var(images_cfn[i]);
        }
	}

    function load_existing_images_into_var(cfn)
    {
        var cfn_id = cfn.custom_field_name_id;
        var field_url = "custom_field_id_" + cfn_id + "_image_url";
        var field_id = "custom_field_id_" + cfn_id;

        var urls = $scope.item[field_url];
        var ids = $scope.item[field_id];

        var splitted_urls = [];
        var splitted_ids = [];

        if (urls) splitted_urls = urls.split(",");
        if (ids) splitted_ids = ids.split(",");

        var result = [];
        for (var i = 0; i < splitted_urls.length; i++)
        {
            var obj = {url : splitted_urls[i], id : splitted_ids[i]};
            result.push(obj);
        };

        $scope.images_object_url_and_id[cfn_id] = result;
    }

	function load_account()
	{
		var promise = ApiService.MyAccountAPI.getAccounts()
		.$promise;

		promise.then(function(result)
		{
			$scope.account = result[0];
			$scope.calendar_format = $scope.account.date_format;
		},
		function(err) {}
		);

		return promise;
	}

	function validate()
	{
        // $scope.alerts = [];
        // var valid = false;

        // for (var i =0; i<  GlobalVariablesService.CustomFieldNamesCount; i++)
        // {
        // 	if ($scope.item['custom_field_'+(i+1)] && $scope.item['custom_field_'+(i+1)].length > 0)
        // 	{
        // 		valid = true;
        // 		break;
        // 	}
        // }

        // if (!valid)
        // {
        // 	$scope.alerts.push({ type: 'danger', msg: 'Please fill at least 1 text field.'});
        // 	return false;
        // }


        $scope.validation_error = '';
        return true;
	}

    function getAppSettings()
    {
        var appSettingsPromise = SettingsService.get_app_settings();

        appSettingsPromise.then(function(result)
        {
            $scope.AppSettings = result;
        },
        function(err)
        {
        });
    }



    // calendar

    $scope.calendar_open = function($event, scope_item_name)
    {
    	$event.preventDefault();
    	$event.stopPropagation();

    	$scope[scope_item_name] = true;
	};

    $scope.calendar_dateOptions = {
    	formatYear: 'yy',
    	startingDay: 1
	};

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 2);
    $scope.calendar_events =
    [
    {
    	date: tomorrow,
    	status: 'full'
	},
    {
    	date: afterTomorrow,
    	status: 'partially'
	}];

    $scope.calendar_getDayClass = function(date, mode)
    {
    	if (mode === 'day')
    	{
    		var dayToCheck = new Date(date)
    		.setHours(0, 0, 0, 0);

    		for (var i = 0; i < $scope.events.length; i++)
    		{
    			var currentDay = new Date($scope.events[i].date)
    			.setHours(0, 0, 0, 0);

    			if (dayToCheck === currentDay)
    			{
    				return $scope.events[i].status;
				}
			}
		}

    	return '';
	};


    // time

    $scope.time_hstep = 1;
    $scope.time_mstep = 15;

    $scope.time_changed = function()
    {
    	console.log('Time changed to: ' + $scope.mytime);
	};

}
]);