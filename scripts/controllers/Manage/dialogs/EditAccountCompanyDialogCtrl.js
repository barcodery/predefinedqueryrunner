'use strict';

angular.module('BarcoderyApp')
.controller('EditAccountCompanyDialogCtrl', ['$scope', '$location',  'ApiService', '$modalInstance', 'gettextCatalog',
						function ($scope, $location,  ApiService, $modalInstance, gettextCatalog)
{

	$scope.address = {name:'', street:'', zip:'', city:'', state:''};
	$scope.working = false;
	$scope.validationErrors = '';

	load();

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			if (typeof($scope.address.name) == "undefined") $scope.address.name = '';
			if (typeof($scope.address.street) == "undefined") $scope.address.street = '';
			if (typeof($scope.address.zip) == "undefined") $scope.address.zip = '';
			if (typeof($scope.address.city) == "undefined") $scope.address.city = '';
			if (typeof($scope.address.state) == "undefined") $scope.address.state = '';

			var promise =  ApiService.AccountsAPI.set_account_address($scope.address).$promise;

			promise.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
				},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function load()
	{
		var account_address = ApiService.AccountsAPI.get_account_address().$promise;

        account_address.then(function(address)
        {
        	if (address)
        	{
				$scope.address = address;
        	}
        },
        function(err)
        {

        });
	}

	function validate()
	{
		// if ($scope.inputFields.Name == '')
		// {
		// 	$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
		// 	return false;
		// }

		return true;
	}
}]);
