'use strict';

angular.module('BarcoderyApp')
.controller('AddExtraAuditItemDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'audit_items', 'CacheService', 'gettextCatalog', 'audit',
									 function ($scope, $location,  ApiService, $modalInstance, audit_items, CacheService, gettextCatalog, audit)
{
	$scope.working = false;
	$scope.count = 1;
	$scope.gridOptions = {};


	init();

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			var rows = $scope.gridApi.selection.getSelectedGridRows();
			var item_ids = _.map(rows, function(row){return row.entity.item_id});

			ApiService.AuditAPI.add_extra_audit_items({audit_id: audit.audit_id, item_ids:JSON.stringify(item_ids), count:$scope.count}).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};


	function init()
	{
		$scope.working = true;

		init_grid();
		var items_promise = load_items();
		var cfn_promise = load_custom_field_names();

		items_promise.then( function( )
        {
            return cfn_promise ;
        })
        .then( function( )
        {
        	$scope.gridOptions.data = $scope.items;
        	setGridCustomColumns($scope.gridOptions, $scope.items);

        	$scope.working = false;
        }
        );
	}

    function init_grid()
    {
        $scope.gridOptions = {

            // showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: false,


            exporterMenuPdf: false,
            // exporterMenuCsv: false,

            exporterCsvFilename: 'audit.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
			},

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                // GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    // enableDisableEditButton();
				});

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    // enableDisableEditButton();
				});
			}
		};


        // set_grid_columns($scope.gridOptions);
	}

	function setGridCustomColumns(gridOptions, items)
	{
		gridOptions.columnDefs = [];
		var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
		if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;

		var i = 0;
		for(var i = 0; i < custom_field_names.length;i++)
		{
			var cfn = custom_field_names[i];

			if (cfn)
			{
				switch (Number(cfn.type))
				{
					case 4: // date time
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
						});
						break;
					}
					case 3: // bit
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
							,filter:
							{
								type: uiGridConstants.filter.SELECT
								, selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
							}

						});
						break;
					}

					default:
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
						});
						break;
					}
				}
			}
		}
	}

	function load_items()
	{
        var promise = ApiService.ItemsAPI.get_items_simple().$promise;

        promise.then(function(result)
        {
        	for (var i = 0; i < audit_items.length; i++)
        	{
        		result = _.without(result, _.findWhere(result, {item_id: audit_items[i].item_id}));
        	};

        	$scope.items = result;
		},
        function(err)
        {
		});

        return promise;
	}

	function load_custom_field_names()
	{
        var promise = CacheService.GetCustomFieldNames();
		promise.then(function(result)
		{
			$scope.custom_field_names = result;
		},
		function(err)
		{
			$scope.custom_field_names = [];
		});

		return promise;
	}

	function validate()
	{
		if (! Helpers.Validation.IsNumber($scope.count))
		{
			$scope.validationErrors = gettextCatalog.getString('Please set valid count.');
			return false;
		}

        var rows = $scope.gridApi.selection.getSelectedGridRows();
        if (rows.length == 0)
        {
			$scope.validationErrors = gettextCatalog.getString('Please select item.');
			return false;
        }

		return true;
	}

}]);
