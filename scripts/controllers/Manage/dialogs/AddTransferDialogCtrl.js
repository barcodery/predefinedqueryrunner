'use strict';

angular.module('BarcoderyApp')
.controller('AddTransferDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'CacheService', '$modal', 'item_id', 'UserService', 'items', 'gettextCatalog',
function ($scope, $location,  ApiService, $modalInstance, CacheService, $modal, item_id, UserService, items, gettextCatalog)
{
	loadUser();

	$scope.validationErrors = [];
	$scope.successMessages = [];
	$scope.selectedLocation = undefined;
	$scope.type="check_in";

	$scope.working = false;

	$scope.multiple_moving = items && items.length > 0 ? true : false;

	if ($scope.multiple_moving && items.length == 1 )
	{
		$scope.multiple_moving = false;
		item_id = items[0].item_id;
	}

	loadLocations();
	loadLocationsFrom();



	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;
			var location_from_id = $scope.selectedLocationFrom.location_id;
			var location_to_id = $scope.selectedLocationTo.location_id;
			var direction = 1;
			var note = $scope.note ? $scope.note : "";
			var count = $scope.count;

			if ($scope.multiple_moving)
			{
				var returned_count = 0;
				var returned_valid_count = 0;

				$scope.validationErrors = [];
				$scope.successMessages = [];

				for (var i = 0; i < items.length; i++)
				{

					var promise = send_api_add_transfer_out_multiple(items[i], location_from_id, location_to_id, note, count);
					// ApiService.ItemsAPI.addTransfer({item_id:items[i].item_id, location_from_id:location_from_id, location_to_id:location_to_id, note:note, count:count }).$promise
					promise.then(function(result)
					{
						returned_count++;
						returned_valid_count++;
						if (returned_count == items.length) $scope.working = false;

						if (returned_valid_count == items.length)
						{
							$modalInstance.close(true);
						}


						// var item_name = get_first_cfn_value(items[returned_count-1]);
						// $scope.successMessages.push(item_name +' moved.');

						CacheService.SetItemsDirty();
					},
					function(err)
					{
						returned_count++;
						if (returned_count == items.length) $scope.working = false;

						// var item_name = get_first_cfn_value(items[returned_count-1]);
						// $scope.validationErrors.push(item_name +': '+ err.data.error_message);
					});
				};

			}
			else
			{

				ApiService.ItemsAPI.addTransfer({item_id:item_id, location_from_id:location_from_id, location_to_id:location_to_id, note:note, count:count }).$promise
				.then(function(result)
				{
					$scope.working = false;
					$modalInstance.close(true);
					CacheService.SetItemsDirty();
				},
				function(err)
				{
					$scope.validationErrors = [];
					$scope.working = false;
					$scope.validationErrors.push({msg: err.data.error_message});
				});
			}
		}
	};

	$scope.createLocationFromClicked = function()
	{
		var func = function()
		{
			$scope.selectedLocationFrom = _.first($scope.locations);
		}

		showCreateLocationDialog(func);
	}

	$scope.createLocationToClicked = function()
	{
		var func = function()
		{
			$scope.selectedLocationTo = _.first($scope.locations);
		}

		showCreateLocationDialog(func);
	}

	function send_api_add_transfer_out_multiple(item, location_from_id, location_to_id, note, count)
	{
		var promise = ApiService.ItemsAPI.addTransfer({item_id:item.item_id, location_from_id:location_from_id, location_to_id:location_to_id, note:note, count:count }).$promise

		promise.then(function(result)
		{
			var item_name = get_first_cfn_value(item);
			$scope.successMessages.push({msg: item_name +' moved.'});
		},
		function(err)
		{
			var item_name = get_first_cfn_value(item);
			$scope.validationErrors.push({msg: item_name +': '+ err.data.error_message});
		});

		return promise;
	}

	function showCreateLocationDialog(runWhenSuccFunc)
	{
		var modalInstance = $modal.open({
			controller: 'AddLocationDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddLocationDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetLocationsDirty();
				var promise = loadLocations();

				promise.then(function(result)
				{
					runWhenSuccFunc();
					//$scope.selectedLocation = _.last($scope.locations);
				});
			}
		});
	}

	function loadLocations()
	{
		var promise = CacheService.GetLocations();

		promise.then(function(result)
		{
			$scope.locations = result;
		},
		function(err)
		{
		});

		return promise;
	}

	function loadLocationsFrom()
	{
		if ($scope.multiple_moving)
		{
			var promise = CacheService.GetLocations();

			promise.then(function(result)
			{
				$scope.locations_from = result;
			},
			function(err)
			{
			});
			return;
		}

		$scope.working = true;
		var promise = ApiService.ItemsAPI.getItemLocations({item_id:item_id}).$promise;

		promise.then(function(result)
		{
			for (var i=0;i<result.length;i++)
			{
				result[i].name = result[i].location_name;
			}

			$scope.locations_from = result;

			$scope.working = false;
		},
		function(err)
		{
			$scope.working = false;
		});
	}

	function validate()
	{
		$scope.validationErrors = [];
		if ($scope.selectedLocationFrom == undefined ||  $scope.selectedLocationFrom == null)
		{
			$scope.validationErrors.push({msg: gettextCatalog.getString('Please choose location from.')});
			return false;
		}

		if ($scope.selectedLocationTo == undefined ||  $scope.selectedLocationTo == null)
		{
			$scope.validationErrors.push({msg: gettextCatalog.getString('Please choose location to.')});
			return false;
		}

		if (!Helpers.Validation.IsNumber($scope.count))
		{
			$scope.validationErrors.push({msg: gettextCatalog.getString('Count must be number.')});
			return false;
		}

		if ($scope.count <= 0)
		{
			$scope.validationErrors.push({msg: gettextCatalog.getString('Count must be bigger than 0.')});
			return false;
		}


		return true;
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

	function get_first_cfn_value(item)
	{
		for(var field_name in item)
		{
			if (field_name.indexOf("custom_field_id") > -1)
			{
				return item[field_name];
			}
		}

		return false;
	}

}]);
