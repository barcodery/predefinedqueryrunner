'use strict';

angular.module('BarcoderyApp')
.controller('ImagesGalleryDialogCtrl', ['$scope',  '$modalInstance', 'imagesUrls', 'selectedIndex',
							function ($scope,  $modalInstance, imagesUrls, selectedIndex)
{
	var index = selectedIndex;

	$scope.nextImage = function()
	{
		if (index >= imagesUrls.length)
		{
			index = 0;
		}

		$scope.image_url = imagesUrls[index];

		index++;
	}

	$scope.nextImage();
}]);
