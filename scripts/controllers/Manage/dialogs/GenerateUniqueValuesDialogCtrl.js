'use strict';

angular.module('BarcoderyApp')
.controller('GenerateUniqueValuesDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'items_id', 'CacheService', 'gettextCatalog',
                            function ($scope, $location,  ApiService, $modalInstance, items_id, CacheService, gettextCatalog)
{

    var cr_code_text = "QR Code";
    init();

    $scope.selectionChanged = function()
    {

    }

    $scope.cancelClicked = function()
    {
        $modalInstance.dismiss('cancel');
    }

    $scope.saveClicked = function()
    {
        if (validate())
        {
            $scope.working = true;
            ApiService.ItemsAPI.generate_unique_values({ids:JSON.stringify(items_id), cfn_id:$scope.selectedCfn.custom_field_name_id}).$promise
            .then(function(result)
            {
                $scope.working = false;
                $modalInstance.close(true);
            },
            function(err)
            {
                $scope.working = false;
                $scope.validationErrors = err.data.error_message;
            });
        }
    }


    function init()
    {
        $scope.working = false;

        var customFieldNamesPromise = CacheService.GetCustomFieldNames();
        customFieldNamesPromise.then(function(result)
        {
            $scope.cfn = [];

            for (var i = 0; i < result.length; i++) {
                if (result[i].type == 1 || result[i].type == 5 || result[i].type == 6) // text, barcode, nfc
                {
                    $scope.cfn.push(result[i]);
                }
            };

            $scope.selectedCfn = $scope.cfn[0];
        },
        function(err)
        {
        });
    }

    function validate()
    {
        if (!$scope.selectedCfn)
        {
            $scope.validationErrors = gettextCatalog.getString('Please choose field.');
            return false;
        }

        $scope.validationErrors = '';

        return true;
    }

}]);
