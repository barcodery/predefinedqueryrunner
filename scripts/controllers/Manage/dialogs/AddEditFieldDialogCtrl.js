'use strict';

angular.module('BarcoderyApp')
.controller('AddEditFieldDialogCtrl', ['$scope', '$location', 'ApiService', 'CacheService', '$modalInstance', 'gettextCatalog', 'item',
function ($scope, $location,  ApiService, CacheService, $modalInstance, gettextCatalog, item)
{
	$scope.working = false;
	$scope.validationErrors = '';
	$scope.item = item;

	load_custom_field_types();


	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			if ($scope.item.is_unique) $scope.item.is_unique = "1";
			if ($scope.item.is_identifier) $scope.item.is_identifier = "1";


			$scope.item.type = $scope.selected_type.value;

			ApiService.CustomFieldNamesAPI.add($scope.item).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function load_custom_field_types()
	{
		var promise_available_custom_field_ids = CacheService.GetAvailableCustomFieldTypes(); //ApiService.CustomFieldNamesAPI.getAvailableCustomFieldIds().$promise;

		promise_available_custom_field_ids.then(function(custom_field_ids)
		{
			$scope.types = [];

			for (var i = 0; i < custom_field_ids.length; i++)
			{
				var id = custom_field_ids[i];

				if (id == 1) $scope.types.push({value:1 ,name:"Text"});
				if (id == 2) $scope.types.push({value:2 ,name:"Number"});
				if (id == 3) $scope.types.push({value:3 ,name:"Bit"});
				if (id == 4) $scope.types.push({value:4 ,name:"Date Time"});
				if (id == 5) $scope.types.push({value:5 ,name:"Barcode/QR Code"});
				if (id == 6) $scope.types.push({value:6 ,name:"NFC"});
				if (id == 7) $scope.types.push({value:7 ,name:"GPS"});
				if (id == 8) $scope.types.push({value:8 ,name:"Images"});
				if (id == 9) $scope.types.push({value:9 ,name:"Item minimum alert count"});
			};

			init_item();
		},
		function(err)
		{
		});
	}

	function init_item()
	{
		if (item)
		{
			item = _.clone(item);
			$scope.item = item;
			$scope.selected_type = _.find($scope.types, function(it) {return it.value == $scope.item.type});
		}
		else
		{
			$scope.item = {is_unique : false, is_identifier: false};
			$scope.selected_type = $scope.types[0];
		}
	}

	function validate()
	{
		if (!$scope.item.name || $scope.item.name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		if ($scope.selected_type.value == 9 && (!$scope.item.alert_email || $scope.item.alert_email == ''))
		{

		}

		return true;
	}
}]);
