'use strict';

angular.module('BarcoderyApp')
.controller('EditCategoryDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'item', 'gettextCatalog',
							function ($scope, $location,  ApiService, $modalInstance, item, gettextCatalog)
{
	$scope.item = item;
	$scope.name = item.name;
	$scope.working = false;
	$scope.validationErrors = '';

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			ApiService.CategoriesAPI.edit({category_id: $scope.item.category_id,name:$scope.name}).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function validate()
	{
		if (!$scope.name || $scope.name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}

		return true;
	}
}]);
