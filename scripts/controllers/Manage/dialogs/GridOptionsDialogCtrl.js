'use strict';

angular.module('BarcoderyApp')
.controller('GridOptionsDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'gettextCatalog', 'items_view_scope',
function ($scope, $location,  ApiService, $modalInstance, gettextCatalog, items_view_scope)
{
	$scope.items_view_scope = items_view_scope;
	$scope.original_row_height = items_view_scope.gridOptions.rowHeight;

	$scope.cancelClicked = function()
	{
		items_view_scope.gridOptions.rowHeight = $scope.original_row_height;
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		items_view_scope.saveGridState()

		$modalInstance.close(true);
	}

}]);
