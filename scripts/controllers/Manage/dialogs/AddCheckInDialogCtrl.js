'use strict';

angular.module('BarcoderyApp')
.controller('AddCheckInDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'CacheService', '$modal', 'item_id', 'UserService', 'gettextCatalog', 'items',
							function ($scope, $location,  ApiService, $modalInstance, CacheService, $modal, item_id, UserService, gettextCatalog, items)
{
	loadUser();

	$scope.selectedLocation = undefined;
	$scope.type="check_in";

	$scope.working = false;
	$scope.validationErrors = [];

	$scope.multiple_moving = items && items.length > 0 ? true : false;

	if ($scope.multiple_moving && items.length == 1 )
	{
		$scope.multiple_moving = false;
		item_id = items[0].item_id;
	}

	loadLocations();

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;
			var location_id = $scope.selectedLocation.location_id;
			var direction = 1;
			var note = $scope.note ? $scope.note : "";
			var count = $scope.count;

			if ($scope.multiple_moving)
			{
				var returned_count = 0;
				var returned_valid_count = 0;

				$scope.validationErrors = [];
				$scope.successMessages = [];

				for (var i = 0; i < items.length; i++)
				{
					var promise = send_api_add_check_in_out_multiple(items[i], location_id, direction, note, count);

					//ApiService.ItemsAPI.addInOutEntry({item_id:items[i].item_id, location_id:location_id, direction:direction, note:note, count:count }).$promise
					promise.then(function(result)
					{
						returned_count++;
						returned_valid_count++;

						if (returned_count == items.length) $scope.working = false;

						if (returned_valid_count == items.length)
						{
							$modalInstance.close(true);
						}

						// var item_name = get_first_cfn_value(items[returned_count-1]);
						// $scope.successMessages.push(item_name +' moved.');

						// $scope.working = false;
						// $modalInstance.close(true);
						CacheService.SetItemsDirty();
					},
					function(err)
					{
						returned_count++;
						if (returned_count == items.length) $scope.working = false;

						// var item_name = get_first_cfn_value(items[returned_count-1]);
						// $scope.validationErrors.push(item_name +': '+ err.data.error_message);
					});
				}
			}
			else
			{
				// $scope.working = true;
				// var location_id = $scope.selectedLocation.location_id;
				// var direction = 1;
				// var note = $scope.note ? $scope.note : "";
				// var count = $scope.count;

				ApiService.ItemsAPI.addInOutEntry({item_id:item_id, location_id:location_id, direction:direction, note:note, count:count }).$promise
				.then(function(result)
				{
					$scope.working = false;
					$modalInstance.close(true);
					CacheService.SetItemsDirty();
				},
				function(err)
				{
					$scope.working = false;
					$scope.validationErrors.push({msg:err.data.error_message});
				});
			}
		}
	};

	$scope.createLocationClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddLocationDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddLocationDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetLocationsDirty();
				var promise = loadLocations();

				promise.then(function(result)
				{
					$scope.selectedLocation = _.last($scope.locations);
				});
			}
		});
	}


	function send_api_add_check_in_out_multiple(item,location_id, direction, note, count)
	{
		var promise = ApiService.ItemsAPI.addInOutEntry({item_id:item.item_id, location_id:location_id, direction:direction, note:note, count:count }).$promise;

		promise.then(function(result)
		{
			var item_name = get_first_cfn_value(item);
			$scope.successMessages.push({msg:item_name +' ok.'});
		},
		function(err)
		{
			var item_name = get_first_cfn_value(item);
			$scope.validationErrors.push({msg:item_name +': '+ err.data.error_message});
		});

		return promise;
	}

	function loadLocations()
	{
		var promise = CacheService.GetLocations();

		promise.then(function(result)
		{
			$scope.locations = result;
		},
		function(err)
		{
		});

		return promise;
	}

	function validate()
	{
		$scope.validationErrors =  [];

		if ($scope.selectedLocation == undefined ||  $scope.selectedLocation == null)
		{
			$scope.validationErrors.push({msg:gettextCatalog.getString('Please choose location.')});
			return false;
		}

		if (! Helpers.Validation.IsNumber($scope.count))
		{
			$scope.validationErrors.push({msg:gettextCatalog.getString('Count must be number.')});
			return false;
		}

		if ($scope.count <= 0)
		{
			$scope.validationErrors.push({msg:gettextCatalog.getString('Count must be bigger than 0.')});
			return false;
		}


		return true;
	}


	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

	function get_first_cfn_value(item)
	{
		for(var field_name in item)
		{
			if (field_name.indexOf("custom_field_id") > -1)
			{
				return item[field_name];
			}
		}

		return false;
	}

}]);
