'use strict';

angular.module('BarcoderyApp')
.controller('AddRequestItemDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'CacheService', '$modal', 'items_id', 'request_id', 'UserService', 'gettextCatalog',
function ($scope, $location,  ApiService, $modalInstance, CacheService, $modal, items_id, request_id, UserService, gettextCatalog)
{
	$scope.gridOptions = {};
	$scope.selectedLocation = undefined;
	$scope.count=1;
	$scope.working = false;
	$scope.validationErrors = '';
	$scope.show_items_grid = items_id == null;
    $scope.show_choose_request = request_id == null;

	init();


	// handlers

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;
			var location_id = $scope.selectedLocation.location_id;
			var direction = 1;
			var note = $scope.note ? $scope.note : "";
			var count = $scope.count;

			if ($scope.show_items_grid)
			{
				var rows = $scope.gridApi.selection.getSelectedGridRows();
				items_id = _.map(rows, function(row){return row.entity.item_id});
			}

            if ($scope.show_choose_request)
            {
                request_id = $scope.selectedRequest.request_id;
            }

			ApiService.RequestsAPI.add_request_items({items_ids:JSON.stringify(items_id), location_id:location_id,  note:note, count:count, request_id:request_id  }).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
				CacheService.SetItemsDirty();
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	$scope.createLocationClicked = function()
	{
		var func = function()
		{
			$scope.selectedLocation = _.last($scope.locations);
		}

		showCreateLocationDialog(func);
	}

    $scope.create_request_button_clicked = function()
    {
        var modalInstance = $modal.open({
            controller: 'AddEditRequestDialogCtrl',
            templateUrl: 'views/Manage/dialogs/AddEditRequestDialog.html',
            size: 'sm',
            resolve: {
                item: function ()
                {
                    return null;
                }
            }
        });

        modalInstance.result.then(function (answer)
        {
            if (answer)
            {
                var promise = load_requests();

                promise.then(function(result)
                {
                    $scope.selectedRequest = _.first($scope.requests);
                },
                function(err)
                {
                });
            }

            $scope.isEditButtonEnabled = false;
        });
    }


	// methods

	function init()
	{
		$scope.working = true;

		init_grid();
		load_items();

		var items_promise = null;
        var cfn_promise = null;

		if (items_id == null)
    	{
            var cfn_promise = load_custom_field_names();
			items_promise = load_items();
    	}

        if ($scope.show_choose_request)
        {
            load_requests();
        }


		var user_promise = loadUser();
		var locations_promise = loadLocations();

		user_promise.then(function(result)
		{
			return user_promise;
		},
		function(err)
		{
		})
		.then(function(result)
		{
			$scope.working = false;
			return locations_promise;
		},
		function(err)
		{
		})
		.then( function()
        {
            if (items_id == null)
            {
                return cfn_promise ;
            }
            else
            {
                return locations_promise;
            }

        })
        .then(function()
        {
        	if (items_id == null)
        	{
        		return items_promise;
        	}
        	else
        	{
				return locations_promise;
        	}
        },
		function(err)
		{
		})
        .then(function()
        {
        	$scope.working = false;
    	},
		function(err)
		{
		})
		;
	}

    function init_grid()
    {
        $scope.gridOptions =
        {
            // showColumnFooter: true,
            enableSorting : true,
            enableRowSelection : true,
            enableColumnResizing : true,
            enableFiltering : true,
            enableGridMenu: true,
            enableSelectAll: false,

            exporterMenuPdf: false,
            // exporterMenuCsv: false,

            exporterCsvFilename: 'items.csv',

            // exporterPdfDefaultStyle: {fontSize: 9},
            // exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
            // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
            // exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
            // exporterPdfFooter: function ( currentPage, pageCount )
            // {
            // return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
            // },

            // exporterPdfCustomFormatter: function ( docDefinition )
            // {
            // docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            // docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            // return docDefinition;
            // },

            // exporterPdfOrientation: 'portrait',
            // exporterPdfPageSize: 'LETTER',
            // exporterPdfMaxGridWidth: 500,
            // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

            exporterOlderExcelCompatibility:true,

            importerDataAddCallback: function ( grid, newObjects )
            {
                $scope.data = $scope.data.concat( newObjects );
			},

            onRegisterApi : function(gridApi)
            {
                $scope.gridApi = gridApi;

                GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

                gridApi.selection.on.rowSelectionChanged($scope,function(row)
                {
                    // enableDisableEditButton();
				});

                gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
                {
                    // enableDisableEditButton();
				});
			}
		};

        // set_grid_columns($scope.gridOptions);
	}

	function showCreateLocationDialog(runWhenSuccFunc)
	{
		var modalInstance = $modal.open({
			controller: 'AddLocationDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddLocationDialog.html',
			size: 'sm',
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetLocationsDirty();
				var promise = loadLocations();

				promise.then(function(result)
				{
					runWhenSuccFunc();
					//$scope.selectedLocation = _.last($scope.locations);
				});
			}
		});
	}


	// load methods

	function loadLocations()
	{
		var promise = CacheService.GetLocations();

		promise.then(function(result)
		{
			$scope.locations = result;
		},
		function(err)
		{
		});

		return promise;
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});

		return promise;
	}

	function load_items()
	{
        var promise = ApiService.ItemsAPI.get_items_simple().$promise;

        promise.then(function(result)
        {
        	// for (var i = 0; i < audit_items.length; i++)
        	// {
        	// 	result = _.without(result, _.findWhere(result, {item_id: audit_items[i].item_id}));
        	// };

        	$scope.items = result;

    		$scope.gridOptions.data = $scope.items;

		},
        function(err)
        {
		});

        return promise;
	}

	function load_custom_field_names()
	{
        var promise = CacheService.GetCustomFieldNames();
		promise.then(function(result)
		{
			$scope.custom_field_names = result;
			setGridCustomColumns($scope.gridOptions, $scope.items);
		},
		function(err)
		{
			$scope.custom_field_names = [];
		});

		return promise;
	}

    function load_requests()
    {
        var only_active = 1;
        var promise = ApiService.RequestsAPI.get({ only_active: only_active}).$promise;

        promise.then(function(result)
        {
            $scope.requests = result;
        },
        function(err)
        {
            $scope.loadingData = false;
        });

        return promise;
    }


	// helper

	function setGridCustomColumns(gridOptions, items)
	{
		gridOptions.columnDefs = [];
		var custom_field_names = _.where($scope.custom_field_names, {is_identifier: true});
		if (custom_field_names.length == 0 ) custom_field_names.push($scope.custom_field_names[0]) ;

		var i = 0;
		for(var i = 0; i < custom_field_names.length;i++)
		{
			var cfn = custom_field_names[i];

			if (cfn)
			{
				switch (Number(cfn.type))
				{
					case 4: // date time
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> {{ grid.appScope.HelperService.getFormattedDate(COL_FIELD) }} </a></div>'
						});
						break;
					}
					case 3: // bit
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)"> <span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span> </a></div>'
							,filter:
							{
								type: uiGridConstants.filter.SELECT
								, selectOptions: [ { value: 1, label: 'True' }, { value: 0, label: 'False' }]
							}

						});
						break;
					}

					default:
					{
						gridOptions.columnDefs.push(
						{
							field: 'custom_field_id_'+cfn.custom_field_name_id
							,enableHiding:true
							,displayName: cfn.name
							, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'
						});
						break;
					}
				}
			}
		}
	}

	function validate()
	{
		$scope.validationErrors =  '';

		if ($scope.show_items_grid)
		{
			var rows = $scope.gridApi.selection.getSelectedGridRows();
			if (rows.length == 0)
			{
				$scope.validationErrors = gettextCatalog.getString('Please select items');
				return false;
			}
		}

        if ($scope.show_choose_request)
        {
            if ($scope.selectedRequest == null)
            {
                $scope.validationErrors = gettextCatalog.getString('Please select request');
                return false;
            }
        }

		if ($scope.selectedLocation == undefined )
		{
			$scope.validationErrors = gettextCatalog.getString('Please choose location.');
			return false;
		}

		if (!Helpers.Validation.IsNumber($scope.count))
		{
			$scope.validationErrors = gettextCatalog.getString('Count must be number.');
			return false;
		}

		if ($scope.count <= 0)
		{
			$scope.validationErrors = gettextCatalog.getString('Count must be bigger than 0.');
			return false;
		}

		$scope.validationErrors =  '';

		return true;
	}
}]);
