'use strict';

angular.module('BarcoderyApp')
.controller('AddEditRequestDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'CacheService', '$modal',  'UserService',  'gettextCatalog', 'item',
function ($scope, $location,  ApiService, $modalInstance, CacheService, $modal, UserService, gettextCatalog, item)
{
	$scope.working = false;
	$scope.validationErrors = '';

	if (item != null)
	{
		$scope.request = {name : item.name, description : item.description};
	}
	else
	{
		$scope.request = {name : '', description : ''};
	}


	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			var obj_to_send = {name:$scope.request.name, description:$scope.request.description };

			if (item != null)
			{
				obj_to_send.request_id = item.request_id;
			}

			var promise = ApiService.RequestsAPI.add_edit_request(obj_to_send).$promise;
			promise.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	$scope.createLocationClicked = function()
	{
		var func = function()
		{
			$scope.selectedLocation = _.last($scope.locations);
		}

		showCreateLocationDialog(func);
	}

	function validate()
	{
		if ($scope.request.name == '' )
		{
			$scope.validationErrors =  gettextCatalog.getString('Please type name.');
			return false;
		}

		$scope.validationErrors =  '';

		return true;
	}
}]);
