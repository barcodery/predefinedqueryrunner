'use strict';

angular.module('BarcoderyApp')
.controller('AddEditAuditDialogCtrl', ['$scope', '$location', 'ApiService', '$modalInstance', 'CacheService', '$modal',  'UserService', 'gettextCatalog', 'audit', 'audit_type',
function ($scope, $location,  ApiService, $modalInstance, CacheService, $modal,  UserService, gettextCatalog, audit, audit_type)
{

	init();

	load_all();

	function init()
	{
		$scope.audit = audit;

		$scope.editing = audit ? true : false;
		$scope.adding = !$scope.editing;

		$scope.is_location_audit = audit_type == "location";
		$scope.is_category_audit = !$scope.is_location_audit;

		$scope.selected_location = undefined;
		$scope.selected_category = undefined;

		$scope.working = false;
		$scope.validationErrors = '';

		$scope.name = '';

		if ($scope.editing)
		{
			load_editing_audit();
		}
	}

	$scope.cancelClicked = function()
	{
		$modalInstance.dismiss('cancel');
	}

	$scope.saveClicked = function()
	{
		if (validate())
		{
			$scope.working = true;

			var post_obj = {};
			if ($scope.adding && $scope.is_location_audit) post_obj.location_id = $scope.selected_location.location_id;
			if ($scope.adding && $scope.is_category_audit) post_obj.category_id = $scope.selected_category.category_id;
			if ($scope.editing) post_obj.audit_id = audit.audit_id;

			post_obj.name = $scope.name;
			post_obj.description = $scope.description ? $scope.description : '' ;

			ApiService.AuditAPI.create(post_obj).$promise
			.then(function(result)
			{
				$scope.working = false;
				$modalInstance.close(true);
			},
			function(err)
			{
				$scope.working = false;
				$scope.validationErrors = err.data.error_message;
			});
		}
	};

	function load_all()
	{
		$scope.working = true;
		var user_promise = load_user();

		user_promise.then(function(result)
		{
			return user_promise;
		},
		function(err)
		{
		})
		.then(function(result)
		{
			var locations_promise = load_locations();
			return locations_promise;
		},
		function(err)
		{
		})
		.then(function(result)
		{
			var categories_promise = load_categories();
			$scope.working = false;
			return categories_promise;
		},
		function(err)
		{
		})
		;

		return user_promise;
	}

	function load_locations()
	{
		var promise = CacheService.GetLocations();

		promise.then(function(result)
		{
			$scope.locations = result;
		},
		function(err)
		{
		});

		return promise;
	}

	function load_categories()
	{
		var promise = CacheService.GetCategories();

		promise.then(function(result)
		{
			$scope.categories = result;
		},
		function(err)
		{
		});

		return promise;
	}

	function load_user()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});

		return promise;
	}

	function validate()
	{
		if ($scope.adding && $scope.is_location_audit && $scope.selected_location == undefined )
		{
			$scope.validationErrors = gettextCatalog.getString('Please choose location.');
			return false;
		}

		if ($scope.adding && $scope.is_category_audit && $scope.selected_category == undefined )
		{
			$scope.validationErrors = gettextCatalog.getString('Please choose category.');
			return false;
		}

		if ($scope.name == '')
		{
			$scope.validationErrors = gettextCatalog.getString('Name can not be empty.');
			return false;
		}



		$scope.validationErrors =  '';

		return true;
	}

	function load_editing_audit()
	{
		$scope.name = audit.name;
		$scope.description = audit.description;
	}

}]);
