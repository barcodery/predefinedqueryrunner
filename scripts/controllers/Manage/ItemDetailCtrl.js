'use strict';

angular.module('BarcoderyApp')
.controller('ItemDetailCtrl', ['$scope', '$location', 'ApiService', 'HelperService', 'CacheService', 'uiGridConstants', 'GlobalVariablesService','$timeout', 'UserService','$modal', '$routeParams',
function ($scope, $location, ApiService, HelperService, CacheService, uiGridConstants, GlobalVariablesService, $timeout, UserService, $modal, $routeParams)
{
	var item_id = $routeParams.item_id;
	$scope.item_id = item_id;
	$scope.images_object_url_and_id = [];

	GlobalVariablesService.Scope_ItemTab_Details = $scope;

	$scope.redirect = function(path, ev)
	{
		// ev.preventDefault();
		$location.path(path);
	}

	$scope.inOutTabClicked = function()
	{
		GlobalVariablesService.Scope_ItemTab_InOutEntries.isTabActive = true;
		GlobalVariablesService.Scope_ItemTab_Locations.isTabActive = false;
		GUI.Grid.SetGridHeight();
		// GUI.Grid.SetGridHeight();
	}

	$scope.locationsTabClicked = function()
	{
		GlobalVariablesService.Scope_ItemTab_InOutEntries.isTabActive = false;
		GlobalVariablesService.Scope_ItemTab_Locations.isTabActive = true;
		GUI.Grid.SetGridHeight();
		// GUI.Grid.SetGridHeight();
	}

	$scope.detailsTabClicked = function()
	{
		// refreshResize();
	}

	$scope.loadData = function()
	{
		loadUser();
		var load_account_promise = load_account();

		$scope.itemLoaded = false;
		$scope.cfnLoaded = false;
		$scope.bnLoaded = false;

		$scope.working = true;

		var promise = ApiService.ItemsAPI.getItem({item_id:item_id}).$promise;
		promise.then(function(result)
		{
			$scope.item = result;

			var find = ',';
			var re = new RegExp(find, 'g');

			if ($scope.item.categories != null) $scope.item.categories = $scope.item.categories.replace(re, ", ");
			if ($scope.item.tags != null) $scope.item.tags = $scope.item.tags.replace(re, ", ");

			if ($scope.item.state_colors)
			{
				$scope.state_colors = $scope.item.state_colors.split(",");
			}

			$scope.itemLoaded = true;
		},
		function(err)
		{
		});

		promise.then(function(result)
		{
			var customFieldNamesPromise = CacheService.GetCustomFieldNames();
			customFieldNamesPromise.then(function(result)
			{
				$scope.cfn = result;
				$scope.cfnLoaded = true;
				$scope.bnLoaded = true;

				load_account_promise.then(function(result)
				{
					loaded_all();
				});
			},
			function(err)
			{
			});
		});

		// var barcodeNamesPromise = CacheService.GetBarcodeNames();
		// barcodeNamesPromise.then(function(result)
		// {
		// $scope.bn = result;
		// $scope.bnLoaded = true;
		// },
		// function(err)
		// {
		// });

		waitTillLoaded();
	}

	$scope.activateInOutEntriesTab = function()
	{
		$scope.isInOutEntriesTabActive = true;

		$scope.inOutTabClicked();
	}

	$scope.editItemButtonClicked = function()
	{
		// $scope.redirect('/edit_item/' + $scope.item_id );

		var modalInstance = $modal.open({
			controller: 'AddEditItemDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditItemDialog.html',
			size: '90-percent',
			resolve: {
			selected_items: function ()
			{
				return [ $scope.item ];
			}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				$scope.loadData();
			}
		});
	}

	$scope.refreshClicked = function()
	{
		$scope.loadData();
	}

	$scope.get_scope_variable = function (name)
	{
		var result = $scope[name];
		return result;
	}

	$scope.showSaveBarcodesPDFDialog = function()
	{
		var modalInstance = $modal.open({
			controller: 'SaveBarcodesPdfDialogCtrl',
			templateUrl: 'views/Manage/dialogs/SaveBarcodesPdfDialog.html',
			size: 'lg',
			resolve: {
				item: function ()
				{
					return $scope.item;
				},

				items: function ()
				{
					return null;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				loadData();
			}
		});
	}

	$scope.get_custom_fields_by_types = function(types)
	{
		var result = [];

		if ($scope.cfn)
		{
			for (var i = 0; i < $scope.cfn.length; i++)
			{
				if (_.contains(types, $scope.cfn[i].type) )
				{
					result.push($scope.cfn[i]);
				}
			}
		}

		return result;
	}

	$scope.generate_google_map_url = function(latlng)
	{

		var url = 'http://www.google.com/maps';

		latlng = latlng.replace(/\s/g,'');  	//remove spaces
		var semicolonIndex = latlng.indexOf(";");
		if (semicolonIndex > -1 )
		{
			var latitude  = latlng.substring(0, semicolonIndex);
			var longitide = latlng.substring(semicolonIndex + 1, latlng.length);

			url = 'http://www.google.com/maps/place/'+latitude+','+longitide+'/@'+latitude+','+longitide+',12z';
		}

		return url;
	}

	$scope.imageClicked = function(images_object_url_and_id, selected_image_obj_url)
	{
		var index = 0;
		var urls = [];
		for (var i = 0; i < images_object_url_and_id.length; i++)
		{
			var image_obj = images_object_url_and_id[i];

			if (image_obj == selected_image_obj_url)
			{
				index = i;
			}

			urls.push(image_obj.url)
		};

		var modalInstance = $modal.open({
			controller: 'ImagesGalleryDialogCtrl',
			templateUrl: 'views/Manage/dialogs/ImagesGalleryDialog.html',
			size: '75-percent',
			resolve:
			{
				imagesUrls: function ()
				{
					return urls;
				}
				, selectedIndex: function()
				{
					return index;
				}
			}
		});

		// modalInstance.result.then(function (answer)
		// {
		// 	if (answer)
		// 	{
		// 		$scope.loadData();
		// 	}
		// });

	}

	function waitTillLoaded()
	{
		$timeout((function()
		{
			if ($scope.bnLoaded && $scope.cfnLoaded && $scope.itemLoaded)
			{
				$scope.working = false;
				return;
			}
			else
			{
				waitTillLoaded();
			}

		}), 300);
	}

	function setGridTabsToUnactive()
	{
		GlobalVariablesService.Scope_ItemTab_InOutEntries.isTabActive = false;
		GlobalVariablesService.Scope_ItemTab_Locations.isTabActive = false;
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

	function loaded_all()
	{
		set_item_date_format();

		load_images_to_variables();

		$scope.page_header = get_item_identifier_value($scope.item);
	}

	function load_images_to_variables()
	{
		var images_fields = $scope.get_custom_fields_by_types(['8']);

		for (var i = 0; i < images_fields.length; i++)
		{
			var cfn = images_fields[i];

			load_existing_images_into_scope(cfn);
		};
	}

    function load_existing_images_into_scope(cfn)
    {
        var cfn_id = cfn.custom_field_name_id;
        var field_url = "custom_field_id_" + cfn_id + "_image_url";
        var field_id = "custom_field_id_" + cfn_id;

        var urls = $scope.item[field_url];
        var ids = $scope.item[field_id];

        var splitted_urls = [];
        var splitted_ids = [];

        if (urls) splitted_urls = urls.split(",");
        if (ids) splitted_ids = ids.split(",");

        var result = [];
        for (var i = 0; i < splitted_urls.length; i++)
        {
            var obj = {url : splitted_urls[i], id : splitted_ids[i]};
            result.push(obj);
        };

        $scope.images_object_url_and_id[cfn_id] = result;
    }

	function set_item_date_format()
	{
		var date_fields = $scope.get_custom_fields_by_types(["4"]);

		for (var i = 0; i < date_fields.length; i++)
		{
			var t = $scope.item['custom_field_id_' + date_fields[i].custom_field_name_id].split(/[- :]/);
			var date = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));

			$scope.item['formatted_date_field_id_' + date_fields[i].custom_field_name_id] = date.format($scope.account.date_format +" hh:mm"); ;
		}
	}

	function load_account()
	{
		var promise = ApiService.MyAccountAPI.getAccounts()
		.$promise;

		promise.then(function(result)
		{
			$scope.account = result[0];
			$scope.calendar_format = $scope.account.date_format;
		},
		function(err) {}
		);

		return promise;
	}

	function get_item_identifier_value(item)
	{
		var custom_field_names = _.where($scope.cfn, {is_identifier: true});
		if (custom_field_names.length == 0 ) custom_field_names.push($scope.cfn[0]) ;

		var i = 0;
		for(var i = 0; i < custom_field_names.length;i++)
		{
			var current_custom_field_name = custom_field_names[i];

			if (current_custom_field_name)
			{
				var value = item['custom_field_id_'+current_custom_field_name.custom_field_name_id];
				return value;
			}
		}

		return "";
	}

	// startup

	$scope.loadData();
}]);

