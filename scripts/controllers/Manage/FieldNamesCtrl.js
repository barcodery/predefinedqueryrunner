'use strict';

angular.module('BarcoderyApp')
.controller('FieldNamesCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService, gettextCatalog)
{
	$scope.isDeleteButtonDisabled = true;
	$scope.isEditButtonEnabled = false;
	$scope.deleteButtonTooltip = '';
	$scope.alerts = [];

	$scope.HelperService = HelperService;


	loadUser();
	loadData();
	initGrid();

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.addButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddEditFieldDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditFieldDialog.html',
			size: 'sm',
			resolve: {
					item: function () {
					return null;
					}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				// CacheService.SetCategoriesDirty();
				loadData();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var item = rows[0].entity;

		var modalInstance = $modal.open({
			controller: 'AddEditFieldDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddEditFieldDialog.html',
			size: 'sm',
			resolve: {
				item: function ()
				{
					return item;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				// CacheService.SetCategoriesDirty();
				// CacheService.SetItemsDirty();
				loadData();
			}
		});
	}

	$scope.deleteButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var content = {title:'Delete', text:'Do you really want to delete selected fileds and all values associated with them ?'};

		var modalInstance = $modal.open({
			controller: 'ConfirmDialogCtrl',
			templateUrl: 'views/ConfirmDialog.html',
			size: 'sm',
			resolve: {
				content: function ()
				{
					return content;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				var rows = $scope.gridApi.selection.getSelectedGridRows();
				var ids = [];

				for(var i in rows)
				{
					ids.push(rows[i].entity.custom_field_name_id);
				}

				ApiService.CustomFieldNamesAPI.delete({ids:JSON.stringify(ids)}).$promise
				.then(function(result)
				{
					CacheService.SetItemsDirty();
					loadData();
				},
				function(err)
				{
				});
			}
		});
	}

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.refreshClicked = function()
	{
		loadData();
	}

	$scope.getTypeText = function(type)
	{
		if (type==1) return "Text";
		if (type==2) return "Number";
		if (type==3) return "Bit";
		if (type==4) return "Date Time";
		if (type==5) return "Barcode/QR Code";
		if (type==6) return "NFC";
		if (type==7) return "GPS";
		if (type==8) return "Images";
		if (type==9) return "Item minimum alert count";

		return "unknown"
	}

	function loadData()
	{
		$scope.working = true;
		var promise = CacheService.GetCustomFieldNames();

		promise.then(function(result)
		{
			$scope.custom_field_names = result;

			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;

			$scope.working = false;
			GUI.Grid.SetGridHeight();
			GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

			enableDisableEditButton();
			enableDisableDeleteButton();

			if (result.some(function(element, index, array) {return element.type==9;} ))

			add_alert_email_column();
		},
		function(err)
		{
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		});
	}

	function initGrid()
	{
		$scope.gridOptions = {

			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,

			exporterCsvFilename: 'fields.csv',

			exporterOlderExcelCompatibility:true,

			importerDataAddCallback: function ( grid, newObjects )
			{
				$scope.data = $scope.data.concat( newObjects );
			},

			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;

				GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
			}
		};

		$scope.gridOptions.columnDefs =
		[
		{
			field: 'name'
			,displayName:gettextCatalog.getString("Name", null, "Context Grid Columns")
			, enableHiding:true
		}
		,{
			field: 'is_identifier'
			,displayName:gettextCatalog.getString("Is identifier", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span>  </div>'
		}
		,{
			field: 'is_unique'
			,displayName:gettextCatalog.getString("Is unique", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents" ><span ng-class="grid.appScope.HelperService.getBoolFaSymbolClass(COL_FIELD) "> </span>  </div>'
		}
		,{
			field: 'type'
			// ,displayName:'Type'
			,displayName:gettextCatalog.getString("Type", null, "Context Grid Columns")
			, enableHiding:true
			, cellTemplate: '<div class="ui-grid-cell-contents"> {{ grid.appScope.getTypeText(COL_FIELD) }} </div>'
		}


		];
	}

	function add_alert_email_column()
	{
		var alreadyContain = $scope.gridOptions.columnDefs.some(function(element, index, array)
		{
			return element.field == 'alert_email';

		});

		if (alreadyContain) return;


		$scope.gridOptions.columnDefs.push (
		{
			field: 'alert_email'
			// ,displayName:'Type'
			,displayName:gettextCatalog.getString("Alert email", null, "Context Grid Columns")
			, enableHiding:true
		});
	}

	function enableDisableDeleteButton()
	{
		if (!$scope.gridApi)
		{
			$scope.isDeleteButtonDisabled = false;
			return;
		}

		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}

	function enableDisableEditButton()
	{
		if (!$scope.gridApi)
		{
			$scope.isEditButtonEnabled = false;
			return;
		}

		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

	var custom_field_names_count = GlobalVariablesService.CustomFieldNamesCount;

	$scope.savingCustomFieldNames = false;

}]);