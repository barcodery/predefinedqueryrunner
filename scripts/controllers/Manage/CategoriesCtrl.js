'use strict';

angular.module('BarcoderyApp')
.controller('CategoriesCtrl', ['$scope', '$location',  'ApiService', 'HelperService', 'CacheService', '$modal', 'GlobalVariablesService', 'UserService', 'uiGridConstants', 'gettextCatalog',
function ($scope, $location,ApiService, HelperService, CacheService, $modal, GlobalVariablesService, UserService,uiGridConstants, gettextCatalog)
{
	$scope.isDeleteButtonDisabled = true;
	$scope.isEditButtonEnabled = false;
	$scope.deleteButtonTooltip = '';
	$scope.alerts = [];

	loadUser();
	loadData();
	initGrid();

	$scope.redirect = function(path, ev)
	{
		if (ev) ev.preventDefault();
		$location.path(path);
	}

	$scope.addButtonClicked = function()
	{
		var modalInstance = $modal.open({
			controller: 'AddCategoryDialogCtrl',
			templateUrl: 'views/Manage/dialogs/AddCategoryDialog.html',
			size: 'sm',
			// resolve: {
			// items: function () {
			// return $scope.items;
			// }
			// }
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				loadData();
			}
		});
	}

	$scope.editButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var item = rows[0].entity;

		var modalInstance = $modal.open({
			controller: 'EditCategoryDialogCtrl',
			templateUrl: 'views/Manage/dialogs/EditCategoryDialog.html',
			size: 'sm',
			resolve: {
				item: function ()
				{
					return item;
				}
			}
		});

		modalInstance.result.then(function (answer)
		{
			if (answer)
			{
				CacheService.SetCategoriesDirty();
				CacheService.SetItemsDirty();
				loadData();
			}
		});
	}

	$scope.deleteButtonClicked = function()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();
		var ids = [];

		for(var i in rows)
		{
			ids.push(rows[i].entity.category_id);
			if (rows[i].entity.count > 0)
			{
				$scope.alerts.push({ type: 'danger', msg: gettextCatalog.getString("Categories", null, "Only empty categories can be deleted.") });
				return;
			}
		}

		$scope.working = true;
		ApiService.CategoriesAPI.delete({ids:JSON.stringify(ids)}).$promise
		.then(function(result)
		{

			CacheService.SetCategoriesDirty();
			loadData();
			$scope.isDeleteButtonDisabled = true;
		},
		function(err)
		{
			$scope.working = false;
		});
	}

	$scope.closeAlert = function(index)
	{
		$scope.alerts.splice(index, 1);
	};

	$scope.itemClicked = function(row)
	{
		var name = row.entity.name;
		GlobalVariablesService.ItemsCategoriesFilter = name;
		$scope.redirect('/items');
	}

	$scope.exportToCsv = function(rowType)
	{
		var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
		$scope.gridApi.exporter.csvExport( rowType, 'all' , myElement);
	}

	$scope.initFileChooser = function()
	{
		var fileChooser = $('.import-file-chooser');

		if ( fileChooser.length !== 1 )
		{
			console.log('Found > 1 or < 1 file choosers within the menu item, error, cannot continue');
		}
		else
		{
			fileChooser[0].addEventListener('change', importFileSelected, false);  // TODO: why the false on the end?  Google
		}
	}

	$scope.fixxx = function()
	{
		GUI.Grid.FixHalfLoadedGrid($scope.gridApi);
	}

	$scope.refreshClicked = function()
	{
		loadData();
	}

	function loadData()
	{
		$scope.working = true;
		var promise = CacheService.GetCategories();

		promise.then(function(result)
		{
			$scope.gridOptions.data = result;
			$scope.noItems = result.length == 0;

			$scope.working = false;
			GUI.Grid.SetGridHeight();
			GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

			$scope.isDeleteButtonDisabled = true;
			$scope.isEditButtonEnabled = false;
		},
		function(err)
		{
			$scope.working = false;
			GUI.Grid.SetGridHeight();
		});
	}

	function initGrid()
	{
		$scope.gridOptions = {

			showColumnFooter: true,
			enableSorting : true,
			enableRowSelection : true,
			enableColumnResizing : true,
			enableFiltering : true,
			enableGridMenu: true,
			enableSelectAll: true,

			exporterMenuPdf: false,

			exporterCsvFilename: 'categories.csv',

			// exporterPdfDefaultStyle: {fontSize: 9},
			// exporterPdfTableStyle: {margin: [15, 15, 15, 15]},
			// exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: false, color: 'blue'},
			// exporterPdfHeader: { text: "Categories", style: {fontSize: 20, bold: true, italics: true, color: 'red', margin: [15, 15, 15, 15]} },
			// exporterPdfFooter: function ( currentPage, pageCount )
			// {
			// return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
			// },

			// exporterPdfCustomFormatter: function ( docDefinition )
			// {
			// docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
			// docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
			// return docDefinition;
			// },

			// exporterPdfOrientation: 'portrait',
			// exporterPdfPageSize: 'LETTER',
			// exporterPdfMaxGridWidth: 500,
			// exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),

			exporterOlderExcelCompatibility:true,

			importerDataAddCallback: function ( grid, newObjects )
			{
				$scope.data = $scope.data.concat( newObjects );
			},

			onRegisterApi : function(gridApi)
			{
				$scope.gridApi = gridApi;

				GUI.Grid.FixHalfLoadedGrid($scope.gridApi);

				gridApi.selection.on.rowSelectionChanged($scope,function(row)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});

				gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows)
				{
					enableDisableDeleteButton();
					enableDisableEditButton();
				});
			}
		};

		$scope.gridOptions.columnDefs =
		[
		{
			field: 'name'
			,displayName:gettextCatalog.getString("Name", null, "Context Grid Columns")
			, enableHiding:true
			// , cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="$parent.$parent.$parent.$parent.$parent.$parent.itemClicked(row)">{{COL_FIELD}} </a></div>'
			, cellTemplate: '<div class="ui-grid-cell-contents" ><a style="cursor:pointer" ng-click="grid.appScope.itemClicked(row)">{{COL_FIELD}} </a></div>'

		}
		,{
			field: 'count'
			,displayName:gettextCatalog.getString("Count", null, "Context Grid Columns")
			, enableHiding:true
			,sortingAlgorithm: Helpers.Sorting.Numeric
			, aggregationType: uiGridConstants.aggregationTypes.sum
		}
		,{
			field: 'user_name'
			,displayName:gettextCatalog.getString("User", null, "Context Grid Columns")
			, enableHiding:true
		}
		];
	}

	function enableDisableDeleteButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length >0)
		{
			$scope.isDeleteButtonDisabled = false;
		}
		else
		{
			$scope.isDeleteButtonDisabled = true;
		}
	}

	function enableDisableEditButton()
	{
		var rows = $scope.gridApi.selection.getSelectedGridRows();

		if (rows.length == 1)
		{
			$scope.isEditButtonEnabled = true;
		}
		else
		{
			$scope.isEditButtonEnabled = false;
		}
	}

	function importFileSelected(event)
	{
		var target = event.srcElement || event.target;

		if (target && target.files && target.files.length === 1)
		{
			var fileObject = target.files[0];

			if (fileObject.size > 1000000)		//	1MB
			{
				$scope.alerts.push({ type: 'danger', msg: 'File is too large.'});
				return;
			}

			$scope.working = true;
			// $scope.gridApi.importer.importFile( fileObject );
			target.form.reset();

			var read = new FileReader();

			read.readAsBinaryString(fileObject);

			read.onloadend = function()
			{
				var categories_json = HelperService.CsvToJson(read.result);

				ApiService.CategoriesAPI.import({items_json:categories_json}).$promise
				.then(function(result)
				{
					$scope.alerts.push({ type: 'success', msg: 'Successfully imported.'});
					loadData();
				},
				function(err)
				{
					$scope.alerts.push({ type: 'danger', msg: err.data.error_message});
					$scope.working = false;
					GUI.Grid.SetGridHeight();
				});

				console.log();
			}
		}
	}

	function loadUser()
	{
		var promise = UserService.getUser();

		promise.then(function(result)
		{
			$scope.user = UserService.user;
		},
		function(err)
		{
		});
	}

}]);
