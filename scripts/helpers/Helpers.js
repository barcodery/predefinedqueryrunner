function Helpers()
{
	this.Validation = new ValidationHelperScript();
    this.Sorting = new SortingAlgs();
    this.Date = new DateHelper();
}


function SortingAlgs()
{
    this.Numeric = function(a,b)
    {
        if (a) a = parseFloat(a.replace(/ /g,''));
        if (b) b = parseFloat(b.replace(/ /g,''));

        if (a == b) return 0;
        if (a < b) return -1;
        return 1;
    }

    this.Date = function(a,b)
    {
        a = Helpers.Date.Parse(a);
        b = Helpers.Date.Parse(b);

        if (a == b) return 0;
        if (a < b) return -1;
        return 1;
    }
}

function DateHelper()
{
    this.Parse = function(date_string_from_server)
    {
        if (!date_string_from_server)
        {
            return null;
        }
        var t = date_string_from_server.split(/[- :]/);
        var date = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));

        return date;
    }

    this.twoDigits = function(d)
    {
        if(0 <= d && d < 10) return "0" + d.toString();
        if(-10 < d && d < 0) return "-0" + (-1*d).toString();
        return d.toString();
    }

    this.DateToMysqlDate = function(d)
    {
        return d.getUTCFullYear() + "-" + this.twoDigits(1 + d.getUTCMonth()) + "-" + this.twoDigits(d.getUTCDate()) + " " + this.twoDigits(d.getUTCHours()) + ":" + this.twoDigits(d.getUTCMinutes()) + ":" + this.twoDigits(d.getUTCSeconds());
    }

}


function ValidationHelperScript()
{
	this.IsEmail = function(email)
	{
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

    this.IsNumber = function(data)
    {
        if (data === parseInt(data, 10))
        {
            return true;
        }
        else
        {
            return false
        }
    }
}



