function GUI()
{
	this.Sidebar = new Sidebar();
	this.Grid = new Grid();

	window.addEventListener('resize', function(event)
	{
		GUI.Grid.SetGridHeight();
	});

	this.InitNumberOnlyInputs = function()
	{
		$('.numbers-only-input-class').jStepper();
	}


	function Sidebar()
	{

	}


	function Grid()
	{
		this.lastGridApi = null;

		this.SetGridHeight = function()
		{
			setTimeout((function()
			{
				var min_height = 200;
				var window_height = window.innerHeight;

				var header = $(".content-header").outerHeight();
				var top_bar = $(".navbar-static-top").outerHeight();

				var tab_header = 0;
				if ($(".nav-tabs").size() > 0)
				{
					tab_header = $(".nav-tabs").first().outerHeight();
				}

				var button_bar = 0;
                var header_info = 0;
                var parameters = 0;

                if ($(".parameters").size() > 0)
                {
                    parameters = $(".parameters").first().outerHeight();
                }

				if ($(".button-menu-bar").size() > 0)
				{
					button_bar = $(".button-menu-bar").first().outerHeight();
				}

                if ($(".header-info").size() > 0)
                {
                    header_info = $(".header-info").first().outerHeight();
                    header_info+= 25;
                }

                var demo_banner_height = $("#demo-banner").outerHeight();
				var padding_margin_other = 0;
				var height = window_height - header - top_bar - button_bar - padding_margin_other - tab_header - header_info - parameters;

                height = height - demo_banner_height;

				if (height < min_height) height = min_height;

				// $(".ui-grid").height(height);

                $("#items_grid").height(height - 10);

                $("#grid1").height(height);
                $("#grid2").height(height);
                $("#grid_report").height(height-135);

				//$(window).resize();
                //GUI.Grid.FixHalfLoadedGrid(null);
                if (GUI.Grid.lastGridApi)
                {
                    // GUI.Grid.lastGridApi.core.notifyDataChange('all');
                    GUI.Grid.lastGridApi.core.handleWindowResize();
                }

			}), 0);
		};

		this.FixHalfLoadedGrid = function(gridApi)
		{
			if (gridApi != null)
            {
                GUI.Grid.lastGridApi = gridApi;
            }

			setTimeout((function()
			{
				if (GUI.Grid.lastGridApi)
				{
                    GUI.Grid.lastGridApi.core.notifyDataChange('all');
					GUI.Grid.lastGridApi.core.handleWindowResize();
                    // GUI.Grid.lsApi.core.handleWindowResize()
				}
			}), 0);
		}



	}

}


Date.prototype.format = function(format) //author: meizz
{
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length==1 ? o[k] :
        ("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}