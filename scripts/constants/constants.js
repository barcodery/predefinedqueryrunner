'use strict';

angular.module('BarcoderyApp')
.constant('API_BASE_URL', 'server/index.php/api/web/v1')
.constant('INSTALLATION_TYPE', {IS_BARCODERY:true, IS_SAM:false})
.constant('APP_NAME', 'Barcodery')


